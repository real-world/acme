package com.dh.acme.controller;


import com.dh.acme.domain.Employee;
import com.dh.acme.service.EmployeeService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.ui.Model;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;


public class EmployeeControllerTest
{

    @Mock
    EmployeeService employeeService;

    @Mock
    private Model model;

    @InjectMocks
    EmployeeController employeeController;

    private List<Employee> employeeList;

    @Before
    public void setUp()
    {
        MockitoAnnotations.initMocks(this);
        employeeList = new ArrayList<>();
        employeeList.add(new Employee());
        when(employeeService.findAll()).thenReturn(employeeList);
    }

    @Test
    public void testGetAllEmployees() throws Exception
    {
        ArgumentCaptor<List<Employee>> argumentCaptor = ArgumentCaptor.forClass((Class<List<Employee>>) employeeList.getClass());
        List<Employee> emp = employeeService.findAll();
        verify(employeeService, times(1)).findAll();
        assertEquals(employeeList.size(), emp.size());


    }

    @Test
    public void testGetEmployeeById() throws Exception
    {
        Employee x = employeeService.findById((long) 0);
        verify(employeeService, times(1)).findById((long) 0);
    }

    @Test
    public void testAddEmploye() throws Exception
    {
        Employee nEmp = new Employee();
        Employee empAdded = employeeService.save(nEmp);
        verify(employeeService, times(1)).save(nEmp);
        assertEquals(empAdded, null);
    }

    @Test
    public void testUpdateEmploye() throws Exception
    {
        Employee nEmp = new Employee();
        Employee empUpdated = employeeService.save(nEmp);
        verify(employeeService, times(1)).save(nEmp);
        assertEquals(empUpdated, null);
    }

    @Test
    public void testDeleteEmploye() throws Exception
    {
        employeeService.deleteById((long) 0);
        verify(employeeService, times(1)).deleteById((long) 0);
    }
}
