 INSERT INTO category (id, code, name, CREATED_ON, VERSION ) VALUES (30, '10', 'ND','2018-04-10 14:46:19.282', 0);

INSERT INTO type_contract (id, created_on, updated_on, version, description, responsable, type_contract) VALUES ('17', '2018-05-08 00:00:00', '2018-09-25 00:00:00', '0', 'Contrato para proyectos y construcciones mayores a  escala ', 'Persona particular', 'Proyecto y Construccion');
INSERT INTO type_contract (id, created_on, updated_on, version, description, responsable, type_contract) VALUES ('18', '2018-03-08 00:00:00', '2018-09-25 00:00:00', '0', 'Contrato para construcciones escala', 'Persona particular', 'Construccion a Presio fijo o Alzado');
INSERT INTO type_contract (id, created_on, updated_on, version, description, responsable, type_contract) VALUES ('19', '2018-03-08 00:00:00', '2018-09-25 00:00:00', '0', 'Contrato para construcciones menores Presio unitario', 'Persona particular', 'Presio unitario');

-- INSERT INTO acme.role (id, created_on, version, code, description) VALUES ('11', '2018-06-03 20:41:15', '0', 'SRC02', 'SUPERVISOR ENERGY ELECTRIC');
-- INSERT INTO acme.role (id, created_on, version, code, description) VALUES ('12', '2018-06-03 11:41:15', '0', 'SR0011', 'OPERATOR PALA MECANIC');
-- INSERT INTO acme.role (id, created_on, version, code, description) VALUES ('13', '2018-06-03 12:41:15', '0', 'OPM050', 'OPERATOR COMPACTOR MACHINE');
--
--
-- INSERT INTO acme.position (id, created_on, version, name, role_id) VALUES ('11', '2018-06-03 20:41:15', '0', 'PENDIENTE', '10');
-- INSERT INTO acme.position (id, created_on, version, name, role_id) VALUES ('13', '2018-06-03 18:20:15', '0', 'PAUSA', '11');
-- INSERT INTO acme.position (id, created_on, version, name, role_id) VALUES ('14', '2018-06-03 18:20:15', '0', 'OPERATIVO MACHINE', '13');