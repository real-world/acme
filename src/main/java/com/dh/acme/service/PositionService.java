//--------Marvin Dickson Mendia Calizaya-------
package com.dh.acme.service;

import com.dh.acme.domain.Position;

import java.util.List;

public interface PositionService extends GenericService<Position>
{
    List<Position> getAllPositionNotAssigned();

    List<Position> procedureFindAll() throws ClassNotFoundException;

    Position procedureSaveInsert(Position position) throws ClassNotFoundException;

    Position procedureSaveUpdate(Position position) throws ClassNotFoundException;

    void procedureDelete(Long id) throws ClassNotFoundException;
}