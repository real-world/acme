package com.dh.acme.service;


import com.dh.acme.domain.Project;
import com.dh.acme.domain.ProjectArea;
import com.dh.acme.repository.ProjectAreaRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Service
public class ProjectAreaServiceImpl extends GenericServiceImpl<ProjectArea> implements ProjectAreaService{


    private ProjectAreaRepository repository;

    public ProjectAreaServiceImpl(ProjectAreaRepository projectAreaRepository) {
        this.repository = projectAreaRepository;
    }

    @Override
    protected CrudRepository<ProjectArea, Long> getRepository()
    {
        return repository;
    }

    @Value("${acme.datasource.jdbcUrl}")
    private String url;

    @Value("${acme.datasource.username}")
    private String userName;


    @Value("${acme.datasource.password}")
    private String password;

//    @Override
//    public List<ProjectArea> procedureFindAll() throws ClassNotFoundException
//    {
//        List<ProjectArea> results = new ArrayList<>();
//        Connection conn = null;
//        PreparedStatement ps = null;
//        ResultSet rs = null;
//
//        try {
//            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
//            conn = DriverManager.getConnection(url, userName, password);
//
//            String SPsql = "EXEC SP_GetAllProject_Area ";
//
//            ps = conn.prepareStatement(SPsql);
//            ps.setEscapeProcessing(true);
//            ps.setQueryTimeout(60000);
//            rs = ps.executeQuery();
//            while (rs.next()) {
//                ProjectArea projectArea = new ProjectArea();
//                projectArea.setId(rs.getLong("Id"));
//
//                projectArea.setProject(rs.getLong("project_id"));
//                projectArea.setArea(rs.getLong("area_id"));
//                projectArea.setEstado(rs.getString("estado"));
//                results.add(projectArea);
//            }
//        }
//        catch (SQLException error){
//            System.out.println(error.getMessage());
//        }
//        finally {
//            if (rs != null) {
//                try {
//                    rs.close();
//                } catch (SQLException e) { System.out.println(e.toString());}
//            }
//            if (ps != null) {
//                try {
//                    ps.close();
//                } catch (SQLException e) { System.out.println(e.toString());}
//            }
//            if (conn != null) {
//                try {
//                    conn.close();
//                } catch (SQLException e) { System.out.println(e.toString());}
//            }
//        }
//        return results;
//    }

    public ProjectArea procedureFindProjectAreaById(long id) throws ClassNotFoundException
    {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        ProjectArea projectArea = new ProjectArea();
        try
        {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            conn = DriverManager.getConnection(url, userName, password);
            String SPsql = "EXEC SP_GetProjectAreaById ?";

            ps = conn.prepareStatement(SPsql);
            ps.setEscapeProcessing(true);
            ps.setQueryTimeout(60000);
            ps.setLong(1, id);
            rs = ps.executeQuery();
            SetInfoForProjectArea(rs, projectArea);
        } catch (SQLException error)
        {
            System.out.println(error.getMessage());
        } finally
        {
            if (rs != null)
            {
                try
                {
                    rs.close();
                } catch (SQLException e)
                {
                    System.out.println(e.toString());
                }
            }
            if (ps != null)
            {
                try
                {
                    ps.close();
                } catch (SQLException e)
                {
                    System.out.println(e.toString());
                }
            }
            if (conn != null)
            {
                try
                {
                    conn.close();
                } catch (SQLException e)
                {
                    System.out.println(e.toString());
                }
            }
        }
        return projectArea;
    }

    private void SetInfoForProjectArea(ResultSet rs, ProjectArea projectArea) throws SQLException
    {
        while (rs.next())
        {
            projectArea.setId(rs.getLong("Id"));
            projectArea.setEstado(rs.getString("estado"));
        }
    }



    public ProjectArea procedureSaveInsert(ProjectArea projectArea) throws ClassNotFoundException
    {
        int userId=100;
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        ProjectArea area1 = new ProjectArea();

        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            conn = DriverManager.getConnection(url, userName, password);

            String SPsql = "EXEC SP_InsertProject_Area ?,?,?";

            ps = conn.prepareStatement(SPsql);
            ps.setEscapeProcessing(true);
            ps.setQueryTimeout(60000);
            ps.setLong(1, projectArea.getProject().getId());
            ps.setLong(2, projectArea.getArea().getId());
            ps.setString(3, projectArea.getEstado());
            rs = ps.executeQuery();
        }
        catch (SQLException error){
            System.out.println(error.getMessage());
        }
        finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) { /* ignored */}
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) { /* ignored */}
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) { /* ignored */}
            }
        }
        return area1;
    }
}