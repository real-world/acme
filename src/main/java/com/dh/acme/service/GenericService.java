//--------Marvin Dickson Mendia Calizaya-------
package com.dh.acme.service;

import java.util.List;

public interface GenericService<T>
{
        List<T> findAll();

        T findById(Long id);

        T save(T model);

        void deleteById(Long id);

        //List<T> procedureFindAll()throws ClassNotFoundException;
}
