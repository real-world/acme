package com.dh.acme.service;

import com.dh.acme.domain.Employee;
import com.dh.acme.domain.Eventuality;

import java.util.List;

public interface EventualityService  extends GenericService<Eventuality>{

    List<Eventuality> findByEmployee(Employee employee);

    List<Eventuality> procedureFindAll() throws ClassNotFoundException;

    Eventuality procedureSaveInsert(Eventuality eventuality) throws ClassNotFoundException;

    Eventuality procedureSaveUpdate(Eventuality eventuality) throws ClassNotFoundException;

    void procedureDelete(long id)throws ClassNotFoundException;
}
