//--------Marvin Dickson Mendia Calizaya-------
package com.dh.acme.service;

import com.dh.acme.domain.Training;

import java.util.List;

public interface TrainingService extends GenericService<Training>
{
    List<Training> procedureFindAll() throws ClassNotFoundException;

    Training procedureSaveInsert(Training training) throws ClassNotFoundException;

    Training procedureSaveUpdate(Training training) throws ClassNotFoundException;

    void procedureDelete(Long id) throws ClassNotFoundException;
}