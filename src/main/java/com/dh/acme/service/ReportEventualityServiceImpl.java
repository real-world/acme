package com.dh.acme.service;

import com.dh.acme.domain.ReportEventuality;
import com.dh.acme.repository.ReportEventualityRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.repository.CrudRepository;

import java.util.ArrayList;
import java.util.List;
import java.sql.*;

public class ReportEventualityServiceImpl extends GenericServiceImpl<ReportEventuality> implements ReportEventualityService{

    private ReportEventualityRepository repository;

    public ReportEventualityServiceImpl(ReportEventualityRepository repository) {
        this.repository = repository;
    }

    @Override
    protected CrudRepository<ReportEventuality, Long> getRepository()
    {
        return repository;
    }

    // Consultas a base de datos con stored procedures
    @Value("${acme.datasource.jdbcUrl}")
    private String url;

    @Value("${acme.datasource.username}")
    private String userName;


    @Value("${acme.datasource.password}")
    private String password;

    @Override
    public List<ReportEventuality> procedureFindAll() throws ClassNotFoundException
    {
        List<ReportEventuality> results = new ArrayList<>();
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            conn = DriverManager.getConnection(url, userName, password);
            //conn = DriverManager.getConnection(url);

            String SPsql = "EXEC SP_ReportEventuality ";

            ps = conn.prepareStatement(SPsql);
            ps.setEscapeProcessing(true);
            ps.setQueryTimeout(60000);
            rs = ps.executeQuery();
            while (rs.next()) {
                ReportEventuality evenReport = new ReportEventuality();
                evenReport.setEmployeeName(rs.getString("EmployeeName"));
                //report.getTypeEvent(rs.getString("TypeEvent"));
                results.add(evenReport);
            }
        }
        catch (SQLException error){
            System.out.println(error.getMessage());
        }
        finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) { System.out.println(e.toString());}
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) { System.out.println(e.toString());}
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) { System.out.println(e.toString());}
            }
        }
        return results;
    }

}
