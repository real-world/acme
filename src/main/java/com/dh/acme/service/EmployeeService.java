package com.dh.acme.service;

import com.dh.acme.domain.Employee;

import java.io.InputStream;
import java.util.List;

public interface EmployeeService extends GenericService<Employee>
{
    void saveImage(Long id, InputStream inputStream);

    List<Employee> getEmployeeLike(String search);

    List<Employee> procedureFindAllEmployees() throws ClassNotFoundException;

    Employee procedureFindEmployeeById(long Id) throws ClassNotFoundException;

    Employee procedureSaveInsertEmployee(Employee employee) throws ClassNotFoundException;

    Employee procedureSaveUpdateEmployee(Employee employee) throws ClassNotFoundException;

    void procedureDeleteEmployee(long id) throws ClassNotFoundException;
}