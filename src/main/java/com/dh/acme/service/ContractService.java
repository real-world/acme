package com.dh.acme.service;

import com.dh.acme.domain.Contract;

/**
 * Created by Marcelo on 21/05/2018.
 */
public interface ContractService extends GenericService<Contract>{
}
