
package com.dh.acme.service;

import com.dh.acme.domain.DwReportEmployeeTypeEvent;

import java.util.List;

public interface DwGraphReportEmployeeTypeEventService extends GenericService<DwReportEmployeeTypeEvent> {
    List<DwReportEmployeeTypeEvent> procedureEmployeeTypeEvent(String employee, Long project, String typeEvent, String dateIni, String dateEnd) throws ClassNotFoundException;
}