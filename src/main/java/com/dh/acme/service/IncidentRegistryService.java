package com.dh.acme.service;

import com.dh.acme.domain.IncidentRegistry;

public interface IncidentRegistryService extends GenericService<IncidentRegistry>{
}
