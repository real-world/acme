//--------Marvin Dickson Mendia Calizaya-------
package com.dh.acme.service;

import com.dh.acme.domain.Area;
import com.dh.acme.repository.AreaRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Service
public class AreaServiceImpl extends GenericServiceImpl<Area> implements AreaService
{
    private AreaRepository repository;

    public AreaServiceImpl(AreaRepository areaRepository) {
        this.repository = areaRepository;
    }

    @Override
    protected CrudRepository<Area, Long> getRepository()
    {
        return repository;
    }

    // Consultas a base de datos con stored procedures
    @Value("${acme.datasource.jdbcUrl}")
    private String url;

    @Value("${acme.datasource.username}")
    private String userName;


    @Value("${acme.datasource.password}")
    private String password;

    @Override
    public List<Area> procedureFindAll() throws ClassNotFoundException
    {
        List<Area> results = new ArrayList<>();
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            conn = DriverManager.getConnection(url, userName, password);
            //conn = DriverManager.getConnection(url);

            String SPsql = "EXEC SP_GetAllArea ";

            ps = conn.prepareStatement(SPsql);
            ps.setEscapeProcessing(true);
            ps.setQueryTimeout(60000);
            rs = ps.executeQuery();
            while (rs.next()) {
                Area area = new Area();

                area.setId(rs.getLong("id"));
                area.setCode(rs.getString("code"));
                area.setName(rs.getString("name"));
                results.add(area);
            }
        }
        catch (SQLException error){
            System.out.println(error.getMessage());
        }
        finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) { System.out.println(e.toString());}
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) { System.out.println(e.toString());}
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) { System.out.println(e.toString());}
            }
        }


        return results;
    }


    public Area procedureSaveInsert(Area area) throws ClassNotFoundException
    {
        int userId=100;
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Area area1 = new Area();

        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            conn = DriverManager.getConnection(url, userName, password);

            String SPsql = "EXEC SP_InsertArea ?,?,?";

            ps = conn.prepareStatement(SPsql);
            ps.setEscapeProcessing(true);
            ps.setQueryTimeout(60000);
            ps.setString(1, area.getCode());
            ps.setString(2, area.getName());
            ps.setInt(3, userId);
            rs = ps.executeQuery();
        }
        catch (SQLException error){
            System.out.println(error.getMessage());
        }
        finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) { /* ignored */}
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) { /* ignored */}
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) { /* ignored */}
            }
        }
        return area1;
    }

    public Area procedureSaveUpdate(Area area) throws ClassNotFoundException
    {
        int userId=100;
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Area area2 = new Area();

        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            conn = DriverManager.getConnection(url, userName, password);

            String SPsql = "EXEC SP_UpdateArea ?,?,?,?";

            ps = conn.prepareStatement(SPsql);
            ps.setEscapeProcessing(true);
            ps.setQueryTimeout(60000);
            ps.setLong(1, area.getId());
            ps.setString(2, area.getCode());
            ps.setString(3, area.getName());
            ps.setInt(4, userId);
            rs = ps.executeQuery();
            while (rs.next()) {
                area2.setId(rs.getLong("Id"));
                area2.setCode(rs.getString("code"));
                area2.setName(rs.getString("name"));
                area2.setModifiedBy(rs.getInt("ModifiedBy"));
            }
        }
        catch (SQLException error){
            System.out.println(error.getMessage());
        }
        finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) { /* ignored */}
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) { /* ignored */}
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) { /* ignored */}
            }
        }
        return area2;
    }

    public void procedureDelete(long id) throws ClassNotFoundException
    {
        int userId = 100;
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try
        {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            conn = DriverManager.getConnection(url, userName, password);

            String SPsql = "EXEC SP_DeleteArea ?";

            ps = conn.prepareStatement(SPsql);
            ps.setEscapeProcessing(true);
            ps.setQueryTimeout(60000);
            ps.setLong(1, id);
            rs = ps.executeQuery();
        } catch (SQLException error)
        {
            System.out.println(error.getMessage());
        } finally
        {
            if (rs != null)
            {
                try
                {
                    rs.close();
                } catch (SQLException e)
                { /* ignored */}
            }
            if (ps != null)
            {
                try
                {
                    ps.close();
                } catch (SQLException e)
                { /* ignored */}
            }
            if (conn != null)
            {
                try
                {
                    conn.close();
                } catch (SQLException e)
                { /* ignored */}
            }
        }
    }
}