package com.dh.acme.service;

import com.dh.acme.domain.SafetyRule;

public interface SafetyRuleService extends GenericService<SafetyRule>{
}
