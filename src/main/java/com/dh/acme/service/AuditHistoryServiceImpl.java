package com.dh.acme.service;

import com.dh.acme.domain.AuditHistory;
import com.dh.acme.repository.AuditHistoryRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


@Service
public class AuditHistoryServiceImpl extends GenericServiceImpl<AuditHistory> implements AuditHistoryService {
    private AuditHistoryRepository repository;

    public AuditHistoryServiceImpl(AuditHistoryRepository ahrepository) {
        this.repository = ahrepository;
    }

    @Override
    protected CrudRepository<AuditHistory, Long> getRepository() {
        return repository;
    }

    // Consultas a base de datos con stored procedures
    @Value("${acme.datasource.jdbcUrl}")
    private String url;

    @Value("${acme.datasource.username}")
    private String userName;


    @Value("${acme.datasource.password}")
    private String password;

    @Override
    public List<AuditHistory> procedureFindAll() throws ClassNotFoundException {
        List<AuditHistory> results = new ArrayList<>();
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            conn = DriverManager.getConnection(url, userName, password);
            //conn = DriverManager.getConnection(url);

            String SPsql = "EXEC SP_GetAuditHistory";

            ps = conn.prepareStatement(SPsql);
            ps.setEscapeProcessing(true);
            ps.setQueryTimeout(60000);
//            ps.setDate(1, Date.valueOf("2018-06-12"));
//            ps.setDate(2, Date.valueOf("2018-06-13"));

            rs = ps.executeQuery();

            System.out.println("llamada hecha a sp audit history");
            while (rs.next()) {
                AuditHistory report = new AuditHistory();


                report.setTableName(rs.getString("TableName"));
                report.setColumnName(rs.getString("ColumnName"));
                report.setDate(rs.getDate("Date"));
                report.setOldValue(rs.getString("OldValue"));
                report.setNewValue(rs.getString("NewValue"));
                report.setModifiedBy(rs.getString("ModifiedBy"));
                report.setTypeOperation(rs.getString("TypeOperation"));
                results.add(report);
            }
        } catch (SQLException error) {
            System.out.println(error.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    System.out.println(e.toString());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    System.out.println(e.toString());
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    System.out.println(e.toString());
                }
            }
        }


        return results;
    }
}