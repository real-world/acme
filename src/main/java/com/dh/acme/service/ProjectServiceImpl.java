package com.dh.acme.service;

import com.dh.acme.domain.Project;
import com.dh.acme.domain.ProjectArea;
import com.dh.acme.repository.ProjectRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

@Service
public class ProjectServiceImpl extends GenericServiceImpl<Project> implements ProjectService {
    private static Logger logger = LoggerFactory.getLogger(ProjectServiceImpl.class);
    private ProjectRepository projectRepository;

    public ProjectServiceImpl(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    protected CrudRepository<Project, Long> getRepository() {
        logger.info("returning project repository");
        return projectRepository;
    }

    // Consultas a base de datos con stored procedures
    @Value("${acme.datasource.jdbcUrl}")
    private String url;
    @Value("${acme.datasource.username}")
    private String userName;
    @Value("${acme.datasource.password}")
    private String password;

    @Override
    public List<Project> procedureFindAll() throws ClassNotFoundException
    {
        List<Project> results = new ArrayList<>();
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            conn = DriverManager.getConnection(url, userName, password);

            String SPsql = "EXEC SP_GetAllProjects ";

            ps = conn.prepareStatement(SPsql);
            ps.setEscapeProcessing(true);
            ps.setQueryTimeout(60000);
            rs = ps.executeQuery();
            while (rs.next()) {
                Project project = new Project();

                project.setId(rs.getLong("Id"));
                project.setName(rs.getString("name"));
                project.setDescription(rs.getString("description"));
                project.setDate_start(rs.getDate("date_start"));
                project.setDate_end(rs.getDate("date_end"));
                results.add(project);
            }
        }
        catch (SQLException error){
            System.out.println(error.getMessage());
        }
        finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) { System.out.println(e.toString());}
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) { System.out.println(e.toString());}
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) { System.out.println(e.toString());}
            }
        }
        return results;
    }

    @Override
    public Project procedureSaveInsert(Project project) throws ClassNotFoundException
    {
        int userId=100;
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Project project1 = new Project();

        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            conn = DriverManager.getConnection(url, userName, password);

            String SPsql = "EXEC SP_InsertProject ?,?,?,?";

            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

            String startDate = dateFormat.format(project.getDate_start());
            String endDate = dateFormat.format(project.getDate_end());


            ps = conn.prepareStatement(SPsql);
            ps.setEscapeProcessing(true);
            ps.setQueryTimeout(60000);
            ps.setString(1, project.getName());
            ps.setString(2, project.getDescription());
            ps.setDate(3, Date.valueOf(startDate));
            System.out.print(startDate);
            ps.setDate(4, Date.valueOf(endDate));
            System.out.print(endDate);
            //ps.setString(2, project.getDate_end());
            //ps.setString(2, project.getDate_end());
            //ps.setInt(5, userId);
            rs = ps.executeQuery();
        }
        catch (SQLException error){
            System.out.println(error.getMessage());
        }
        finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) { /* ignored */}
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) { /* ignored */}
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) { /* ignored */}
            }
        }
        return project1;
    }

    public Project procedureSaveUpdate(Project project) throws ClassNotFoundException
    {
        int userId=100;
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Project project2 = new Project();

        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            conn = DriverManager.getConnection(url, userName, password);

            String SPsql = "EXEC SP_UpdateProject ?,?,?,?,?";

            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

            String startDate = dateFormat.format(project2.getDate_start());
            String endDate = dateFormat.format(project2.getDate_end());


            ps = conn.prepareStatement(SPsql);
            ps.setEscapeProcessing(true);
            ps.setQueryTimeout(60000);
            ps.setLong(1, project2.getId());
            ps.setString(2, project2.getName());
            ps.setString(3, project2.getDescription());
            ps.setDate(4, Date.valueOf(startDate));
            ps.setDate(5, Date.valueOf(endDate));

            //ps.setInt(4, userId);
            rs = ps.executeQuery();
            while (rs.next()) {
                project2.setId(rs.getLong("id"));
                project2.setName(rs.getString("name"));
                project2.setDescription(rs.getString("description"));
                project2.setDate_start(rs.getDate("date_start"));
                project2.setDate_end(rs.getDate("date_end"));
            }
        }
        catch (SQLException error){
            System.out.println(error.getMessage());
        }
        finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) { /* ignored */}
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) { /* ignored */}
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) { /* ignored */}
            }
        }
        return project2;
    }

    public void procedureDelete(long id) throws ClassNotFoundException
    {
        int userId=100;
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            conn = DriverManager.getConnection(url, userName, password);

            String SPsql = "EXEC SP_DeleteProject ?";

            ps = conn.prepareStatement(SPsql);
            ps.setEscapeProcessing(true);
            ps.setQueryTimeout(60000);
            ps.setLong(1, id);
            rs = ps.executeQuery(); }
        catch (SQLException error){
            System.out.println(error.getMessage());
        }
        finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) { /* ignored */}
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) { /* ignored */}
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) { /* ignored */}
            }
        }
    }
}
