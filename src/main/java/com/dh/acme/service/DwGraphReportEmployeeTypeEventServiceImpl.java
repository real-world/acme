package com.dh.acme.service;

import com.dh.acme.domain.DwReportEmployeeTypeEvent;
import com.dh.acme.repository.DwReportEmpleadoTypeEventRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Service
public class DwGraphReportEmployeeTypeEventServiceImpl extends GenericServiceImpl<DwReportEmployeeTypeEvent> implements DwGraphReportEmployeeTypeEventService {
    private DwReportEmpleadoTypeEventRepository repository;

    public DwGraphReportEmployeeTypeEventServiceImpl(DwReportEmpleadoTypeEventRepository dwrepository) {
        this.repository = dwrepository;
    }

    @Override
    protected CrudRepository<DwReportEmployeeTypeEvent, Long> getRepository() {
        return repository;
    }

    // Consultas a base de datos con stored procedures
    @Value("${dwacme.datasource.jdbcUrl}")
    private String url;

    @Value("${acme.datasource.username}")
    private String userName;


    @Value("${acme.datasource.password}")
    private String password;

    @Override
    public List<DwReportEmployeeTypeEvent> procedureEmployeeTypeEvent(String employee, Long project, String typeEvent, String dateIni, String dateEnd) throws ClassNotFoundException {
        List<DwReportEmployeeTypeEvent> results = new ArrayList<>();
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            conn = DriverManager.getConnection(url, userName, password);

//           String SPsql = "EXEC SP_ReportEmployeeTypeEventGraphic ?,?,?";
            String SPsql = "SELECT  [EmployeeName]= dimE.[Name]\n" +
                    ",count(case when feven.typeEvent = 'Accidente' then feven.typeEvent end) as accident \n" +
                    ",count(case when feven.typeEvent = 'Incidente' then feven.typeEvent end) as incident \n" +
                    "FROM [dbo].[FactEventuality] feven\n" +
                    "INNER JOIN [dbo].[DimEmployee] dimE ON (feven.IdEmployee = dimE.IdEmployee)\n" +
                    "INNER JOIN [dbo].[DimProject] dimP ON (feven.IdProject = dimP.IdProject)\n ";

            SPsql = SPsql + conditionWhere(employee, project, typeEvent, dateIni, dateEnd);
            SPsql = SPsql + "  GROUP BY dimE.[Name]";

            ps = conn.prepareStatement(SPsql);
            ps.setEscapeProcessing(true);
            ps.setQueryTimeout(60000);
            if (!stringVoidOrNULL(dateIni) && !stringVoidOrNULL(dateEnd)) {
                ps.setDate(1, Date.valueOf(dateIni));
                ps.setDate(2, Date.valueOf(dateEnd));
            }

//            if (!employee.trim().isEmpty()){
//                ps.setString(1, employee);
//            }
//            if (!typeEvent.trim().isEmpty()){
//                ps.setString(2, typeEvent);
//            }
//            if (project<=0){
//                ps.setLong(3, project);
//            }
//            ps.setString(1, employee);
//            ps.setString(2, typeEvent);
//            ps.setLong(3, project);
//            localhost:8080/reportGraph/Mig/ /0/2018-01-13/2018-12-13

            rs = ps.executeQuery();

            while (rs.next()) {
                DwReportEmployeeTypeEvent report = new DwReportEmployeeTypeEvent();

                report.setEmployeeName(rs.getString("EmployeeName"));
                report.setNumberAccident(rs.getLong("accident"));
                report.setNumberIncident(rs.getLong("incident"));
                results.add(report);
            }
        } catch (SQLException error) {
            System.out.println(error.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    System.out.println(e.toString());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    System.out.println(e.toString());
                }
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    System.out.println(e.toString());
                }
            }
        }


        return results;
    }

    private boolean stringVoidOrNULL(String data) {
        return null == data || data.trim().isEmpty() || data.trim().toLowerCase().equals("null");
    }

    private String conditionWhere(String employee, Long project, String typeEvent, String dateIni, String dateEnd) {
        String conditions = "";
        if (!stringVoidOrNULL(employee) || !stringVoidOrNULL(typeEvent) ||
                (!stringVoidOrNULL(dateIni) && !stringVoidOrNULL(dateEnd)) || project > 0)
            conditions = conditions + " WHERE 1 = 1 \n";
        if (!stringVoidOrNULL(employee)) {
            conditions = conditions + " AND dimE.name LIKE '" + employee + "%' \n";
        }
        if (!stringVoidOrNULL(typeEvent)) {
            conditions = conditions + " AND feven.typeEvent LIKE '" + typeEvent + "' \n";
        }
        if (project > 0) {
            conditions = conditions + " AND feven.IdProject = " + project + "\n";
        }
        if (!stringVoidOrNULL(dateIni) && !stringVoidOrNULL(dateEnd)) {
            conditions = conditions + " AND feven.dateEvent BETWEEN ? AND ? \n";
        }

        return conditions;
    }

}
