package com.dh.acme.service;

import com.dh.acme.domain.Contract;
import com.dh.acme.repository.ContractRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;
/**
 * Created by Marcelo on 21/05/2018.
 */
@Service
public class ContractServiceImpl extends GenericServiceImpl<Contract> implements ContractService {
    private static Logger logger = LoggerFactory.getLogger(ContractServiceImpl.class);
    private ContractRepository contractRepository;

    public ContractServiceImpl(ContractRepository contractRepository)
    {
        this.contractRepository = contractRepository;
    }

    @Override
    protected CrudRepository<Contract, Long> getRepository()
    {
        return contractRepository;
    }



}
