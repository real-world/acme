
package com.dh.acme.service;

import com.dh.acme.domain.DwReport;


import java.sql.Date;
import java.util.List;

public interface DwReportService extends GenericService<DwReport>
{
    List<DwReport> procedureFindAll(String startDate, String endDate) throws ClassNotFoundException;

}