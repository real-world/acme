package com.dh.acme.service;

import com.dh.acme.domain.TypeContract;
import com.dh.acme.repository.TypeContractRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;


/**
 * Created by Marcelo on 21/05/2018.
 */
@Service
public class TypeContractServiceImpl extends GenericServiceImpl<TypeContract> implements TypeContractService {
    private static Logger logger = LoggerFactory.getLogger(TypeContractServiceImpl.class);
    private TypeContractRepository typeContractRepository;

    public TypeContractServiceImpl(TypeContractRepository typeContractRepository) {
        this.typeContractRepository = typeContractRepository;
    }

    @Override
    protected CrudRepository<TypeContract, Long> getRepository() {
        return typeContractRepository;
    }

}
