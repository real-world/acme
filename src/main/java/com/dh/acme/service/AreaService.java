//--------Marvin Dickson Mendia Calizaya-------
package com.dh.acme.service;
import com.dh.acme.domain.Area;

import java.util.List;

public interface AreaService extends GenericService<Area>
{
    List<Area> procedureFindAll()throws ClassNotFoundException;

    Area procedureSaveInsert(Area area)throws ClassNotFoundException;

    Area procedureSaveUpdate(Area area)throws ClassNotFoundException;

    void procedureDelete(long id) throws ClassNotFoundException;
}