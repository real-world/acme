package com.dh.acme.service;

import com.dh.acme.domain.SubCategory;

public interface SubCategoryService extends GenericService<SubCategory> {

}
