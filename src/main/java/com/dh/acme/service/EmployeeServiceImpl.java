
package com.dh.acme.service;

import com.dh.acme.domain.Employee;
import com.dh.acme.repository.EmployeeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Service
public class EmployeeServiceImpl extends GenericServiceImpl<Employee> implements EmployeeService {
    private static Logger logger = LoggerFactory.getLogger(EmployeeServiceImpl.class);
    private EmployeeRepository employeeRepository;

    private final int SIZE = 5;

    public EmployeeServiceImpl(EmployeeRepository EmployeeRepository) {
        this.employeeRepository = EmployeeRepository;
    }

    @Override
    protected CrudRepository<Employee, Long> getRepository() {
        return employeeRepository;
    }

    @Override
    public void saveImage(Long id, InputStream file) {
        Employee employeePersisted = findById(id);
        try
        {
            Byte[] bytes = ImageUtils.inputStreamToByteArray(file);
            employeePersisted.setImage(bytes);
            getRepository().save(employeePersisted);
        } catch (IOException e)
        {
            logger.error("Error reading file", e);
            e.printStackTrace();
        }
    }

    @Override
    public List<Employee> getEmployeeLike(String search) {
        Pageable pageable = new PageRequest(0, SIZE);
        return employeeRepository.getByFirstNameAndLastName(search.toLowerCase(),
                pageable);
    }

    @Value("${acme.datasource.jdbcUrl}")
    private String url;

    @Value("${acme.datasource.username}")
    private String userName;


    @Value("${acme.datasource.password}")
    private String password;

    @Override
    public List<Employee> procedureFindAllEmployees() throws ClassNotFoundException
    {
        List<Employee> results = new ArrayList<>();
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try
        {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            conn = DriverManager.getConnection(url, userName, password);
            String SPsql = "EXEC GetAllEmployees ";

            ps = conn.prepareStatement(SPsql);
            ps.setEscapeProcessing(true);
            ps.setQueryTimeout(60000);
            rs = ps.executeQuery();
            while (rs.next())
            {
                Employee employee = new Employee();
                employee.setId(rs.getLong("Id"));
                employee.setDni(rs.getString("Dni"));
                employee.setFirstName(rs.getString("First_Name"));
                employee.setLastName(rs.getString("Last_Name"));
                employee.setAddress(rs.getString("Address"));
                employee.setPhone(rs.getString("Phone"));
                employee.setEmail(rs.getString("Email"));
                employee.setJobDescription(rs.getString("Job_Description"));
                employee.setJobPosition(rs.getString("Job_Position"));
                results.add(employee);
            }
        } catch (SQLException error)
        {
            System.out.println(error.getMessage());
        } finally
        {
            if (rs != null)
            {
                try
                {
                    rs.close();
                } catch (SQLException e)
                {
                    System.out.println(e.toString());
                }
            }
            if (ps != null)
            {
                try
                {
                    ps.close();
                } catch (SQLException e)
                {
                    System.out.println(e.toString());
                }
            }
            if (conn != null)
            {
                try
                {
                    conn.close();
                } catch (SQLException e)
                {
                    System.out.println(e.toString());
                }
            }
        }
        return results;
    }

    @Override
    public Employee procedureFindEmployeeById(long id) throws ClassNotFoundException
    {
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Employee employee = new Employee();
        try
        {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            conn = DriverManager.getConnection(url, userName, password);
            String SPsql = "EXEC GetEmployeeById ?";

            ps = conn.prepareStatement(SPsql);
            ps.setEscapeProcessing(true);
            ps.setQueryTimeout(60000);
            ps.setLong(1, id);
            rs = ps.executeQuery();
            SetInfoForEmployee(rs, employee);
        } catch (SQLException error)
        {
            System.out.println(error.getMessage());
        } finally
        {
            if (rs != null)
            {
                try
                {
                    rs.close();
                } catch (SQLException e)
                {
                    System.out.println(e.toString());
                }
            }
            if (ps != null)
            {
                try
                {
                    ps.close();
                } catch (SQLException e)
                {
                    System.out.println(e.toString());
                }
            }
            if (conn != null)
            {
                try
                {
                    conn.close();
                } catch (SQLException e)
                {
                    System.out.println(e.toString());
                }
            }
        }
        return employee;
    }

    private void SetInfoForEmployee(ResultSet rs, Employee employee) throws SQLException
    {
        while (rs.next())
        {
            employee.setId(rs.getLong("Id"));
            employee.setDni(rs.getString("Dni"));
            employee.setFirstName(rs.getString("First_Name"));
            employee.setLastName(rs.getString("Last_Name"));
            employee.setAddress(rs.getString("Address"));
            employee.setPhone(rs.getString("Phone"));
            employee.setEmail(rs.getString("Email"));
            employee.setJobDescription(rs.getString("Job_Description"));
            employee.setJobPosition(rs.getString("Job_Position"));
        }
    }

    public Employee procedureSaveInsertEmployee(Employee employee) throws ClassNotFoundException
    {
        int userId = 100;
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Employee employeeAdded = new Employee();

        try
        {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            conn = DriverManager.getConnection(url, userName, password);
            String SPsql = "EXEC AddEmployee ?,?,?,?,?,?,?,?";
            System.out.println("==========Employee=============");
            System.out.println(employee.getJobDescription());
            System.out.println(employee.getJobPosition());
            System.out.println("==========FIN Employee=============");
            ps = conn.prepareStatement(SPsql);
            ps.setEscapeProcessing(true);
            ps.setQueryTimeout(60000);
            ps.setString(1, employee.getDni());
            ps.setString(2, employee.getFirstName());
            ps.setString(3, employee.getLastName());
            ps.setString(4, employee.getAddress());
            ps.setString(5, employee.getPhone());
            ps.setString(6, employee.getEmail());
            ps.setString(7, employee.getJobDescription());
            ps.setString(8, employee.getJobPosition());

            rs = ps.executeQuery();
        } catch (SQLException error)
        {
            System.out.println(error.getMessage());
        } finally
        {
            if (rs != null)
            {
                try
                {
                    rs.close();
                } catch (SQLException e)
                { /* ignored */}
            }
            if (ps != null)
            {
                try
                {
                    ps.close();
                } catch (SQLException e)
                { /* ignored */}
            }
            if (conn != null)
            {
                try
                {
                    conn.close();
                } catch (SQLException e)
                { /* ignored */}
            }
        }
        return employeeAdded;
    }

    public Employee procedureSaveUpdateEmployee(Employee employee) throws ClassNotFoundException
    {
        int userId = 100;
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Employee employeeUpdated = new Employee();

        try
        {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            conn = DriverManager.getConnection(url, userName, password);
            String SPsql = "EXEC UpdateEmployee ?,?,?,?,?,?,?,?,?";

            ps = conn.prepareStatement(SPsql);
            ps.setEscapeProcessing(true);
            ps.setQueryTimeout(60000);
            ps.setLong(1, employee.getId());
            ps.setString(2, employee.getDni());
            ps.setString(3, employee.getFirstName());
            ps.setString(4, employee.getLastName());
            ps.setString(5, employee.getAddress());
            ps.setString(6, employee.getPhone());
            ps.setString(7, employee.getEmail());
            ps.setString(8, employee.getJobDescription());
            ps.setString(9, employee.getJobPosition());
            rs = ps.executeQuery();
            SetInfoForEmployee(rs, employeeUpdated);
        } catch (SQLException error)
        {
            System.out.println(error.getMessage());
        } finally
        {
            if (rs != null)
            {
                try
                {
                    rs.close();
                } catch (SQLException e)
                { /* ignored */}
            }
            if (ps != null)
            {
                try
                {
                    ps.close();
                } catch (SQLException e)
                { /* ignored */}
            }
            if (conn != null)
            {
                try
                {
                    conn.close();
                } catch (SQLException e)
                { /* ignored */}
            }
        }
        return employeeUpdated;
    }

    public void procedureDeleteEmployee(long id) throws ClassNotFoundException
    {
        int userId = 100;
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try
        {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            conn = DriverManager.getConnection(url, userName, password);
            String SPsql = "EXEC DeleteEmployee ?";

            ps = conn.prepareStatement(SPsql);
            ps.setEscapeProcessing(true);
            ps.setQueryTimeout(60000);
            ps.setLong(1, id);
            rs = ps.executeQuery();
        } catch (SQLException error)
        {
            System.out.println(error.getMessage());
        } finally
        {
            if (rs != null)
            {
                try
                {
                    rs.close();
                } catch (SQLException e)
                { /* ignored */}
            }
            if (ps != null)
            {
                try
                {
                    ps.close();
                } catch (SQLException e)
                { /* ignored */}
            }
            if (conn != null)
            {
                try
                {
                    conn.close();
                } catch (SQLException e)
                { /* ignored */}
            }
        }
    }
}