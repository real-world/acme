package com.dh.acme.service;


import com.dh.acme.domain.ReportEventualityByEmployee;
import java.util.List;

public interface ReportEventualityByEmployeeService extends GenericService<ReportEventualityByEmployee>
{
    List<ReportEventualityByEmployee> procedureFindAll(Integer employeeId) throws ClassNotFoundException;

}