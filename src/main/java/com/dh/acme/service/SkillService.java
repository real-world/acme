//--------Marvin Dickson Mendia Calizaya-------
package com.dh.acme.service;

import com.dh.acme.domain.Skill;

import java.util.List;

public interface SkillService extends GenericService<Skill>
{
    List<Skill> find(String code);
}
