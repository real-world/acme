//--------Marvin Dickson Mendia Calizaya-------
package com.dh.acme.service;

import com.dh.acme.domain.EmployeeTraining;
import com.dh.acme.repository.EmployeeTrainingRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public class EmployeeTrainingServiceImpl extends GenericServiceImpl<EmployeeTraining> implements EmployeeTrainingService
{
    private EmployeeTrainingRepository repository;

    public EmployeeTrainingServiceImpl(EmployeeTrainingRepository repository)
    {
        this.repository = repository;
    }
    @Override
    protected CrudRepository<EmployeeTraining, Long> getRepository()
    {
        return repository;
    }
}