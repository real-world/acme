package com.dh.acme.service;

import com.dh.acme.domain.Employee;
import com.dh.acme.domain.Eventuality;
import com.dh.acme.repository.EventualityRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Service
public class EventualityServiceImpl extends  GenericServiceImpl<Eventuality> implements EventualityService{
    private EventualityRepository repository;

    public EventualityServiceImpl(EventualityRepository repository) {
        this.repository = repository;
    }

    @Override
    protected CrudRepository<Eventuality, Long> getRepository(){
        return repository;
    }

    // Consultas a base de datos con stored procedures
    @Value("${acme.datasource.jdbcUrl}")
    private String url;

    @Value("${acme.datasource.username}")
    private String userName;


    @Value("${acme.datasource.password}")
    private String password;


    @Override
    public List<Eventuality> findByEmployee(Employee employee) {
        List<Eventuality> temp;
        //return temp = repository.findByEmployeeId(employee.getId().get());

        //return StringUtils.isEmpty(employee) ? findAll() : repository.findByEmployeeId(employee.getId());
        return temp = repository.findAllById(1);
    }

    @Override
    public List<Eventuality> procedureFindAll() throws ClassNotFoundException
    {
        List<Eventuality> results = new ArrayList<>();
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try
        {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            conn = DriverManager.getConnection(url, userName, password);

            String SPsql = "EXEC SP_GetEventuality ";

            ps = conn.prepareStatement(SPsql);
            ps.setEscapeProcessing(true);
            ps.setQueryTimeout(60000);
            rs = ps.executeQuery();
            while (rs.next())
            {
                Eventuality eventuality = new Eventuality();

                eventuality.setId(rs.getLong("id"));
                eventuality.setDateEvent(rs.getDate("dateEvent"));
                eventuality.setDescription(rs.getString("description"));
                eventuality.setTypeEvent(rs.getString("typeEvent"));
                eventuality.setInjuryType(rs.getString("injuryType"));
                eventuality.setInjuryPart(rs.getString("injuryPart"));
                eventuality.setEmployee(rs.getString("employee"));
                results.add(eventuality);
            }
        } catch (SQLException error)
        {
            System.out.println(error.getMessage());
        } finally
        {
            if (rs != null)
            {
                try
                {
                    rs.close();
                } catch (SQLException e)
                {
                    System.out.println(e.toString());
                }
            }
            if (ps != null)
            {
                try
                {
                    ps.close();
                } catch (SQLException e)
                {
                    System.out.println(e.toString());
                }
            }
            if (conn != null)
            {
                try
                {
                    conn.close();
                } catch (SQLException e)
                {
                    System.out.println(e.toString());
                }
            }
        }
        return results;
    }
    private static java.sql.Date convertUtilToSql(java.util.Date uDate) {

        java.sql.Date sDate = new java.sql.Date(uDate.getTime());

        return sDate;
    }

    public Eventuality procedureSaveInsert(Eventuality eventuality) throws ClassNotFoundException
    {
        int userId = 100;
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Eventuality eventuality1 = new Eventuality();

        try
        {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            conn = DriverManager.getConnection(url, userName, password);

            String SPsql = "EXEC SP_InsertEventuality ?,?,?,?,?,?,?";

            ps = conn.prepareStatement(SPsql);
            ps.setEscapeProcessing(true);
            ps.setQueryTimeout(60000);
            System.out.println("******************************************");
            //java.util.Date utilDate = new java.util.Date(eventuality.getDateEvent().getTime());
            //java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());
            System.out.println(Date.valueOf("2018-06-30"));
            System.out.println(eventuality.getDescription());
            System.out.println(eventuality.getTypeEvent());
            System.out.println(eventuality.getInjuryType());
            System.out.println(eventuality.getInjuryPart());
            System.out.println(eventuality.getEmployee());
            System.out.println("******************************************");

            ps.setDate(1, Date.valueOf("2018-06-30"));
            ps.setString(2, eventuality.getDescription());
            ps.setInt(3, Integer.parseInt(eventuality.getTypeEvent()));
            ps.setInt(4, Integer.parseInt(eventuality.getInjuryType()));
            ps.setInt(5, Integer.parseInt(eventuality.getInjuryPart()));
            ps.setInt(6, Integer.parseInt(eventuality.getEmployee()));
            System.out.println("******************************************"
            +Integer.parseInt(eventuality.getEmployee()));
            ps.setInt(7, 1);

            rs = ps.executeQuery();
           // while (rs.next())
            //{
                //eventuality1.setDescription(rs.getString("description"));
               /* .setCode(rs.getString("code"));
                area1.setName(rs.getString("name"));
                area1.setModifiedBy(rs.getInt("ModifiedBy"));*/
                //results.add(area);
           // }
        } catch (SQLException error)
        {
            System.out.println(error.getMessage());
        } finally
        {
            if (rs != null)
            {
                try
                {
                    rs.close();
                } catch (SQLException e)
                { /* ignored */}
            }
            if (ps != null)
            {
                try
                {
                    ps.close();
                } catch (SQLException e)
                { /* ignored */}
            }
            if (conn != null)
            {
                try
                {
                    conn.close();
                } catch (SQLException e)
                { /* ignored */}
            }
        }
        return eventuality1;
    }

    public Eventuality procedureSaveUpdate(Eventuality eventuality) throws ClassNotFoundException
    {
        int userId = 100;
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Eventuality eventuality2 = new Eventuality();

        try
        {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            conn = DriverManager.getConnection(url, userName, password);

            String SPsql = "EXEC SP_UpdateEventuality ?,?,?,?,?,?,?,?";

            ps = conn.prepareStatement(SPsql);
            ps.setEscapeProcessing(true);
            ps.setQueryTimeout(60000);
            System.out.println("+++++++++++++++++++++++++++++++++++++++");
            System.out.println(eventuality.getId());
            ps.setInt(1, Math.toIntExact(eventuality.getId()));

            ps.setDate(2 ,Date.valueOf("2018-06-30"));
            ps.setString(3, eventuality.getDescription());
            System.out.println("+++++++++++++++++++++++++++++++++++++++");

            ps.setInt(4, 1);
            ps.setInt(5, 1);
            ps.setInt(6, 1);
            ps.setInt(7, 7);
            ps.setInt(8, 1);
            rs = ps.executeQuery();
            while (rs.next())
            {
                //eventuality.setId(rs.getLong("Id"));
                //area2.setCode(rs.getString("code"));
                //area2.setName(rs.getString("name"));
                ///area2.setModifiedBy(rs.getInt("ModifiedBy"));
            }
        } catch (SQLException error)
        {
            System.out.println(error.getMessage());
        } finally
        {
            if (rs != null)
            {
                try
                {
                    rs.close();
                } catch (SQLException e)
                { /* ignored */}
            }
            if (rs != null)
            {
                try
                {
                    rs.close();
                } catch (SQLException e)
                { /* ignored */}
            }
            if (ps != null)
            {
                try
                {
                    ps.close();
                } catch (SQLException e)
                { /* ignored */}
            }
            if (conn != null)
            {
                try
                {
                    conn.close();
                } catch (SQLException e)
                { /* ignored */}
            }
        }
        return eventuality2;
    }


    public void procedureDelete(long id) throws ClassNotFoundException
    {
        int userId=100;
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            conn = DriverManager.getConnection(url, userName, password);

            String SPsql = "EXEC SP_DeleteEventuality ?";

            ps = conn.prepareStatement(SPsql);
            ps.setEscapeProcessing(true);
            ps.setQueryTimeout(60000);
            ps.setLong(1, id);
            rs = ps.executeQuery(); }
        catch (SQLException error){
            System.out.println(error.getMessage());
        }
        finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) { /* ignored */}
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) { /* ignored */}
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) { /* ignored */}
            }
        }
    }


}