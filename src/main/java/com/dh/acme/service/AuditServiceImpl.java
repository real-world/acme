package com.dh.acme.service;

import com.dh.acme.domain.Audit;
import com.dh.acme.repository.AuditRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AuditServiceImpl extends GenericServiceImpl<Audit> implements AuditService{
    private AuditRepository repository;

    public AuditServiceImpl(AuditRepository repository) {
        this.repository = repository;
    }

    protected CrudRepository<Audit, Long> getRepository(){
        return repository;
    }

//    public List<AuditService> getAuditServicebyProjectId()
//    {
//        return auditService;
//    }
}
