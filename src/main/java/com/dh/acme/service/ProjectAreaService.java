package com.dh.acme.service;
import com.dh.acme.domain.ProjectArea;

import java.util.List;

public interface ProjectAreaService extends GenericService<ProjectArea> {
    //List<ProjectArea> procedureFindAll()throws ClassNotFoundException;
    ProjectArea procedureFindProjectAreaById(long Id) throws ClassNotFoundException;
    ProjectArea procedureSaveInsert(ProjectArea projectArea)throws ClassNotFoundException;

}