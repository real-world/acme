//--------Marvin Dickson Mendia Calizaya-------
package com.dh.acme.service;

import com.dh.acme.domain.Role;

import java.util.List;

public interface RoleService extends GenericService<Role>
{
    //List<Role> find(String code);
    List<Role> procedureFindAll() throws ClassNotFoundException;

    Role procedureSaveInsert(Role role) throws ClassNotFoundException;

    Role procedureSaveUpdate(Role role) throws ClassNotFoundException;

    void procedureDelete(Long id) throws ClassNotFoundException;
}
