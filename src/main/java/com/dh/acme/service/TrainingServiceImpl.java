//--------Marvin Dickson Mendia Calizaya-------
package com.dh.acme.service;

import com.dh.acme.domain.Training;
import com.dh.acme.repository.TrainingRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Service
public class TrainingServiceImpl extends GenericServiceImpl<Training> implements TrainingService
{
    private TrainingRepository repository;

    public TrainingServiceImpl(TrainingRepository repository)
    {
        this.repository = repository;
    }

    @Override
    protected CrudRepository<Training, Long> getRepository()
    {
        return repository;
    }

    // Consultas a base de datos con stored procedures
    @Value("${acme.datasource.jdbcUrl}")
    private String url;

    @Value("${acme.datasource.username}")
    private String userName;


    @Value("${acme.datasource.password}")
    private String password;

    @Override
    public List<Training> procedureFindAll() throws ClassNotFoundException
    {
        List<Training> results = new ArrayList<>();
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try
        {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            conn = DriverManager.getConnection(url, userName, password);
            //conn = DriverManager.getConnection(url);

            String SPsql = "EXEC SP_GetAllTraining ";

            ps = conn.prepareStatement(SPsql);
            ps.setEscapeProcessing(true);
            ps.setQueryTimeout(60000);
            rs = ps.executeQuery();
            while (rs.next())
            {
                Training training = new Training();

                training.setId(rs.getLong("id"));
                training.setCode(rs.getString("code"));
                training.setName(rs.getString("name"));
                training.setInstructor(rs.getString("instructor"));
                training.setArea(rs.getString("area"));
                training.setArea_id(rs.getLong("area_id"));
                training.setModifiedBy(rs.getInt("ModifiedBy"));
                results.add(training);
            }
        } catch (SQLException error)
        {
            System.out.println(error.getMessage());
        } finally
        {
            if (rs != null)
            {
                try
                {
                    rs.close();
                } catch (SQLException e)
                {
                    System.out.println(e.toString());
                }
            }
            if (ps != null)
            {
                try
                {
                    ps.close();
                } catch (SQLException e)
                {
                    System.out.println(e.toString());
                }
            }
            if (conn != null)
            {
                try
                {
                    conn.close();
                } catch (SQLException e)
                {
                    System.out.println(e.toString());
                }
            }
        }


        return results;
    }


    public Training procedureSaveInsert(Training training) throws ClassNotFoundException
    {
        int userId = 100;
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Training training1 = new Training();
        try
        {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            conn = DriverManager.getConnection(url, userName, password);

            String SPsql = "EXEC SP_InsertTraining ?,?,?,?,?";

            ps = conn.prepareStatement(SPsql);
            ps.setEscapeProcessing(true);
            ps.setQueryTimeout(60000);
            ps.setString(1, training.getCode());
            ps.setString(2, training.getName());
            ps.setString(3, training.getInstructor());
            ps.setLong(4, training.getArea_id());
            ps.setInt(5, userId);
            rs = ps.executeQuery();
        } catch (SQLException error)
        {
            System.out.println(error.getMessage());
        } finally
        {
            if (rs != null)
            {
                try
                {
                    rs.close();
                } catch (SQLException e)
                { /* ignored */}
            }
            if (ps != null)
            {
                try
                {
                    ps.close();
                } catch (SQLException e)
                { /* ignored */}
            }
            if (conn != null)
            {
                try
                {
                    conn.close();
                } catch (SQLException e)
                { /* ignored */}
            }
        }
        return training1;
    }

    public Training procedureSaveUpdate(Training training) throws ClassNotFoundException
    {
        int userId = 100;
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Training training2 = new Training();

        try
        {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            conn = DriverManager.getConnection(url, userName, password);

            String SPsql = "EXEC SP_UpdateTraining ?,?,?,?,?,?";

            ps = conn.prepareStatement(SPsql);
            ps.setEscapeProcessing(true);
            ps.setQueryTimeout(60000);
            ps.setLong(1, training.getId());
            ps.setString(2, training.getCode());
            ps.setString(3, training.getName());
            ps.setString(4, training.getInstructor());
            ps.setLong(5, training.getArea_id());
            ps.setInt(6, userId);
            rs = ps.executeQuery();
            while (rs.next())
            {
                training2.setId(rs.getLong("Id"));
                training2.setCode(rs.getString("code"));
                training2.setName(rs.getString("name"));
                training2.setInstructor(rs.getString("instructor"));
                training2.setArea_id(rs.getLong("area_id"));
                training2.setModifiedBy(rs.getInt("ModifiedBy"));
            }
        } catch (SQLException error)
        {
            System.out.println(error.getMessage());
        } finally
        {
            if (rs != null)
            {
                try
                {
                    rs.close();
                } catch (SQLException e)
                { /* ignored */}
            }
            if (ps != null)
            {
                try
                {
                    ps.close();
                } catch (SQLException e)
                { /* ignored */}
            }
            if (conn != null)
            {
                try
                {
                    conn.close();
                } catch (SQLException e)
                { /* ignored */}
            }
        }
        return training2;
    }

    public void procedureDelete(Long id) throws ClassNotFoundException
    {
        int userId = 100;
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try
        {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            conn = DriverManager.getConnection(url, userName, password);

            String SPsql = "EXEC SP_DeleteTraining ?";
            ps = conn.prepareStatement(SPsql);
            ps.setEscapeProcessing(true);
            ps.setQueryTimeout(60000);
            ps.setLong(1, id);
            rs = ps.executeQuery();
        } catch (SQLException error)
        {
            System.out.println(error.getMessage());
        } finally
        {
            if (rs != null)
            {
                try
                {
                    rs.close();
                } catch (SQLException e)
                { /* ignored */}
            }
            if (ps != null)
            {
                try
                {
                    ps.close();
                } catch (SQLException e)
                { /* ignored */}
            }
            if (conn != null)
            {
                try
                {
                    conn.close();
                } catch (SQLException e)
                { /* ignored */}
            }
        }
    }
}