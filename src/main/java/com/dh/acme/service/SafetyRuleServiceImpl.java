package com.dh.acme.service;

import com.dh.acme.domain.SafetyRule;
import com.dh.acme.repository.SafetyRuleRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public class SafetyRuleServiceImpl extends GenericServiceImpl<SafetyRule> implements SafetyRuleService{
    private SafetyRuleRepository repository;

    public SafetyRuleServiceImpl(SafetyRuleRepository repository) {
        this.repository = repository;
    }

    protected CrudRepository<SafetyRule, Long> getRepository(){
        return repository;
    }
}
