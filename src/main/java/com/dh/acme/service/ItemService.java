package com.dh.acme.service;

import com.dh.acme.domain.Item;

import java.io.InputStream;

public interface ItemService extends GenericService<Item> {

    void saveImage(Long id, InputStream file);

}
