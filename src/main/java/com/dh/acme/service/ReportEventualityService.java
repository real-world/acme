package com.dh.acme.service;

import com.dh.acme.domain.ReportEventuality;

import java.util.List;

public interface ReportEventualityService extends GenericService<ReportEventuality> {
    List<ReportEventuality> procedureFindAll() throws ClassNotFoundException;
}
