
package com.dh.acme.service;

import com.dh.acme.domain.DwTrainingGraphReport;

import java.util.List;

public interface DwTrainingGraphReportService extends GenericService<DwTrainingGraphReport>
{
    List<DwTrainingGraphReport> procedureFindAll() throws ClassNotFoundException;

}