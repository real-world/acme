package com.dh.acme.service;

import com.dh.acme.domain.DwReport;
import com.dh.acme.domain.ReportEventualityByEmployee;
import com.dh.acme.repository.ReportEventualityByEmployeeRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Service
public class ReportEventualityByEmployeeServiceImpl extends GenericServiceImpl<ReportEventualityByEmployee> implements ReportEventualityByEmployeeService{

    private ReportEventualityByEmployeeRepository reportrepository;

    public ReportEventualityByEmployeeServiceImpl(ReportEventualityByEmployeeRepository reportrepository) {
        this.reportrepository = reportrepository;
    }

    @Override
    protected CrudRepository<ReportEventualityByEmployee, Long> getRepository() {
        return reportrepository;
    }
    // Consultas a base de datos con stored procedures
    @Value("${dwacme.datasource.jdbcUrl}")
    private String url;

    @Value("${acme.datasource.username}")
    private String userName;

    @Value("${acme.datasource.password}")
    private String password;

    @Override
    public List<ReportEventualityByEmployee> procedureFindAll(Integer employeeId) throws ClassNotFoundException
    {
        List<ReportEventualityByEmployee> results = new ArrayList<>();
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try
        {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            conn = DriverManager.getConnection(url, userName, password);

            String SPsql = "EXEC SP_ReportTypeEventualityForEmployee ? ";

            ps = conn.prepareStatement(SPsql);
            ps.setEscapeProcessing(true);
            ps.setQueryTimeout(60000);
            ps.setInt(1, employeeId);
            rs = ps.executeQuery();
            while (rs.next())
            {
                ReportEventualityByEmployee report = new ReportEventualityByEmployee();

                report.setTypeEvent(rs.getString("typeEvent"));
                report.setNumberEventType(rs.getInt("numberEventType"));
                results.add(report);
            }
        } catch (SQLException error)
        {
            System.out.println(error.getMessage());
        } finally
        {
            if (rs != null)
            {
                try
                {
                    rs.close();
                } catch (SQLException e)
                {
                    System.out.println(e.toString());
                }
            }
            if (ps != null)
            {
                try
                {
                    ps.close();
                } catch (SQLException e)
                {
                    System.out.println(e.toString());
                }
            }
            if (conn != null)
            {
                try
                {
                    conn.close();
                } catch (SQLException e)
                {
                    System.out.println(e.toString());
                }
            }
        }
        return results;
    }


}
