
package com.dh.acme.service;

import com.dh.acme.domain.DwGraphReport;

import java.util.List;

public interface DwGraphReportService extends GenericService<DwGraphReport>
{
    List<DwGraphReport> procedureFindAll(String startDate, String endDate) throws ClassNotFoundException;

}