//--------Marvin Dickson Mendia Calizaya-------
package com.dh.acme.service;

import com.dh.acme.domain.Position;
import com.dh.acme.repository.PositionRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Service
public class PositionServiceImpl extends GenericServiceImpl<Position> implements PositionService
{
    private PositionRepository repository;

    public PositionServiceImpl(PositionRepository repository)
    {
        this.repository = repository;
    }

    @Override
    protected CrudRepository<Position, Long> getRepository()
    {
        return repository;
    }

    @Override
    public List<Position> getAllPositionNotAssigned() {
        return repository.getAllPositionNotAssigned();
    }

    // Consultas a base de datos con stored procedures
    @Value("${acme.datasource.jdbcUrl}")
    private String url;

    @Value("${acme.datasource.username}")
    private String userName;


    @Value("${acme.datasource.password}")
    private String password;

    @Override
    public List<Position> procedureFindAll() throws ClassNotFoundException
    {
        List<Position> results = new ArrayList<>();
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try
        {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            conn = DriverManager.getConnection(url, userName, password);
            //conn = DriverManager.getConnection(url);

            String SPsql = "EXEC SP_GetAllPosition ";

            ps = conn.prepareStatement(SPsql);
            ps.setEscapeProcessing(true);
            ps.setQueryTimeout(60000);
            rs = ps.executeQuery();
            while (rs.next())
            {
                Position position = new Position();

                position.setId(rs.getLong("id"));
                position.setName(rs.getString("name"));
                position.setRole(rs.getString("role"));
                position.setRole_id(rs.getLong("role_id"));
                position.setModifiedBy(rs.getInt("ModifiedBy"));
                results.add(position);
            }
        } catch (SQLException error)
        {
            System.out.println(error.getMessage());
        } finally
        {
            if (rs != null)
            {
                try
                {
                    rs.close();
                } catch (SQLException e)
                {
                    System.out.println(e.toString());
                }
            }
            if (ps != null)
            {
                try
                {
                    ps.close();
                } catch (SQLException e)
                {
                    System.out.println(e.toString());
                }
            }
            if (conn != null)
            {
                try
                {
                    conn.close();
                } catch (SQLException e)
                {
                    System.out.println(e.toString());
                }
            }
        }


        return results;
    }


    public Position procedureSaveInsert(Position position) throws ClassNotFoundException
    {
        int userId = 100;
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Position position1 = new Position();
        try
        {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            conn = DriverManager.getConnection(url, userName, password);

            String SPsql = "EXEC SP_InsertPosition ?,?,?";

            ps = conn.prepareStatement(SPsql);
            ps.setEscapeProcessing(true);
            ps.setQueryTimeout(60000);
            ps.setString(1, position.getName());
            ps.setLong(2, position.getRole_id());
            ps.setInt(3, userId);
            rs = ps.executeQuery();
        } catch (SQLException error)
        {
            System.out.println(error.getMessage());
        } finally
        {
            if (rs != null)
            {
                try
                {
                    rs.close();
                } catch (SQLException e)
                { /* ignored */}
            }
            if (ps != null)
            {
                try
                {
                    ps.close();
                } catch (SQLException e)
                { /* ignored */}
            }
            if (conn != null)
            {
                try
                {
                    conn.close();
                } catch (SQLException e)
                { /* ignored */}
            }
        }
        return position1;
    }

    public Position procedureSaveUpdate(Position position) throws ClassNotFoundException
    {
        int userId = 100;
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Position position2 = new Position();

        try
        {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            conn = DriverManager.getConnection(url, userName, password);

            String SPsql = "EXEC SP_UpdatePosition ?,?,?,?";

            ps = conn.prepareStatement(SPsql);
            ps.setEscapeProcessing(true);
            ps.setQueryTimeout(60000);
            ps.setLong(1, position.getId());
            ps.setString(2, position.getName());
            //System.out.println(position.getRole_id());
            ps.setLong(3, position.getRole_id());
            ps.setInt(4, userId);
            rs = ps.executeQuery();
            while (rs.next())
            {
                position2.setId(rs.getLong("Id"));
                position2.setName(rs.getString("name"));
                position2.setRole_id(rs.getLong("role_id"));
                position2.setModifiedBy(rs.getInt("ModifiedBy"));
            }
        } catch (SQLException error)
        {
            System.out.println(error.getMessage());
        } finally
        {
            if (rs != null)
            {
                try
                {
                    rs.close();
                } catch (SQLException e)
                { /* ignored */}
            }
            if (ps != null)
            {
                try
                {
                    ps.close();
                } catch (SQLException e)
                { /* ignored */}
            }
            if (conn != null)
            {
                try
                {
                    conn.close();
                } catch (SQLException e)
                { /* ignored */}
            }
        }
        return position2;
    }

    public void procedureDelete(Long id) throws ClassNotFoundException
    {
        int userId = 100;
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try
        {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            conn = DriverManager.getConnection(url, userName, password);

            String SPsql = "EXEC SP_DeletePosition ?";

            ps = conn.prepareStatement(SPsql);
            ps.setEscapeProcessing(true);
            ps.setQueryTimeout(60000);
            ps.setLong(1, id);
            rs = ps.executeQuery();
        } catch (SQLException error)
        {
            System.out.println(error.getMessage());
        } finally
        {
            if (rs != null)
            {
                try
                {
                    rs.close();
                } catch (SQLException e)
                { /* ignored */}
            }
            if (ps != null)
            {
                try
                {
                    ps.close();
                } catch (SQLException e)
                { /* ignored */}
            }
            if (conn != null)
            {
                try
                {
                    conn.close();
                } catch (SQLException e)
                { /* ignored */}
            }
        }
    }
}