package com.dh.acme.service;

import com.dh.acme.domain.Audit;

public interface AuditService extends GenericService<Audit>{
}
