package com.dh.acme.service;

import com.dh.acme.domain.Category;
import java.util.List;

public interface CategoryService extends GenericService<Category>{
    List<Category> find(String code);
}
