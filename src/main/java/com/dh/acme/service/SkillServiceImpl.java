//--------Marvin Dickson Mendia Calizaya-------
package com.dh.acme.service;

import com.dh.acme.domain.Skill;
import com.dh.acme.repository.SkillRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

@Service
public class SkillServiceImpl extends GenericServiceImpl<Skill> implements SkillService
{
    private SkillRepository repository;

    public SkillServiceImpl(SkillRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Skill> find(String code) {
        return StringUtils.isEmpty(code) ? findAll() : repository.findByCode(code).get();
    }

    @Override
    protected CrudRepository<Skill, Long> getRepository() {
        return repository;
    }
}