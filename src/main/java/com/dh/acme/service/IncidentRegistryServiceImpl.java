package com.dh.acme.service;

import com.dh.acme.domain.IncidentRegistry;
import com.dh.acme.repository.IncidentRegistryRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public class IncidentRegistryServiceImpl extends GenericServiceImpl<IncidentRegistry> implements IncidentRegistryService {
    private static Logger logger = LoggerFactory.getLogger(IncidentRegistryServiceImpl.class);
    private IncidentRegistryRepository incidentRegistryRepository;

    public IncidentRegistryServiceImpl(IncidentRegistryRepository IncidentRegistryRepository) {
        this.incidentRegistryRepository = IncidentRegistryRepository;
    }

    @Override
    protected CrudRepository<IncidentRegistry, Long> getRepository(){
        return incidentRegistryRepository;
    }
}
