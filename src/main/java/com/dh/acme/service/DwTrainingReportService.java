
package com.dh.acme.service;

import com.dh.acme.domain.DwTrainingReport;

import java.util.List;

public interface DwTrainingReportService extends GenericService<DwTrainingReport>
{
    List<DwTrainingReport> procedureFindAll() throws ClassNotFoundException;

}