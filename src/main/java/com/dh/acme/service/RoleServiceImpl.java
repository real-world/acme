//--------Marvin Dickson Mendia Calizaya-------
package com.dh.acme.service;

import com.dh.acme.domain.Role;
import com.dh.acme.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Service
public class RoleServiceImpl extends GenericServiceImpl<Role> implements RoleService
{
    private RoleRepository repository;

    public RoleServiceImpl(RoleRepository repository) {
        this.repository = repository;
    }

    @Override
    protected CrudRepository<Role, Long> getRepository() {
        return repository;
    }

    // Consultas a base de datos con stored procedures
    @Value("${acme.datasource.jdbcUrl}")
    private String url;

    @Value("${acme.datasource.username}")
    private String userName;


    @Value("${acme.datasource.password}")
    private String password;

    @Override
    public List<Role> procedureFindAll() throws ClassNotFoundException
    {
        List<Role> results = new ArrayList<>();
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try
        {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            conn = DriverManager.getConnection(url, userName, password);
            //conn = DriverManager.getConnection(url);

            String SPsql = "EXEC SP_GetAllRole";

            ps = conn.prepareStatement(SPsql);
            ps.setEscapeProcessing(true);
            ps.setQueryTimeout(60000);
            rs = ps.executeQuery();
            while (rs.next())
            {
                Role role = new Role();

                role.setId(rs.getLong("id"));
                role.setCode(rs.getString("code"));
                role.setDescription(rs.getString("description"));
                results.add(role);
            }
        } catch (SQLException error)
        {
            System.out.println(error.getMessage());
        } finally
        {
            if (rs != null)
            {
                try
                {
                    rs.close();
                } catch (SQLException e)
                {
                    System.out.println(e.toString());
                }
            }
            if (ps != null)
            {
                try
                {
                    ps.close();
                } catch (SQLException e)
                {
                    System.out.println(e.toString());
                }
            }
            if (conn != null)
            {
                try
                {
                    conn.close();
                } catch (SQLException e)
                {
                    System.out.println(e.toString());
                }
            }
        }


        return results;
    }


    public Role procedureSaveInsert(Role role) throws ClassNotFoundException
    {
        int userId = 100;
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Role role1 = new Role();

        try
        {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            conn = DriverManager.getConnection(url, userName, password);

            String SPsql = "EXEC SP_InsertRole ?,?,?";

            ps = conn.prepareStatement(SPsql);
            ps.setEscapeProcessing(true);
            ps.setQueryTimeout(60000);
            ps.setString(1, role.getCode());
            ps.setString(2, role.getDescription());
            ps.setInt(3, userId);
            rs = ps.executeQuery();
        } catch (SQLException error)
        {
            System.out.println(error.getMessage());
        } finally
        {
            if (rs != null)
            {
                try
                {
                    rs.close();
                } catch (SQLException e)
                { /* ignored */}
            }
            if (ps != null)
            {
                try
                {
                    ps.close();
                } catch (SQLException e)
                { /* ignored */}
            }
            if (conn != null)
            {
                try
                {
                    conn.close();
                } catch (SQLException e)
                { /* ignored */}
            }
        }
        return role1;
    }

    public Role procedureSaveUpdate(Role role) throws ClassNotFoundException
    {
        int userId = 100;
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Role role2 = new Role();

        try
        {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            conn = DriverManager.getConnection(url, userName, password);

            String SPsql = "EXEC SP_UpdateRole ?,?,?,?";

            ps = conn.prepareStatement(SPsql);
            ps.setEscapeProcessing(true);
            ps.setQueryTimeout(60000);
            ps.setLong(1, role.getId());
            ps.setString(2, role.getCode());
            ps.setString(3, role.getDescription());
            ps.setInt(4, userId);
            rs = ps.executeQuery();
            while (rs.next())
            {
                role2.setId(rs.getLong("Id"));
                role2.setCode(rs.getString("code"));
                role2.setDescription(rs.getString("name"));
                role2.setModifiedBy(rs.getInt("ModifiedBy"));
            }
        } catch (SQLException error)
        {
            System.out.println(error.getMessage());
        } finally
        {
            if (rs != null)
            {
                try
                {
                    rs.close();
                } catch (SQLException e)
                { /* ignored */}
            }
            if (ps != null)
            {
                try
                {
                    ps.close();
                } catch (SQLException e)
                { /* ignored */}
            }
            if (conn != null)
            {
                try
                {
                    conn.close();
                } catch (SQLException e)
                { /* ignored */}
            }
        }
        return role2;
    }

    public void procedureDelete(Long id) throws ClassNotFoundException
    {
        int userId = 100;
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try
        {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            conn = DriverManager.getConnection(url, userName, password);

            String SPsql = "EXEC SP_DeleteRole ?";
            ps = conn.prepareStatement(SPsql);
            ps.setEscapeProcessing(true);
            ps.setQueryTimeout(60000);
            ps.setLong(1, id);
            rs = ps.executeQuery();
        } catch (SQLException error)
        {
            System.out.println(error.getMessage());
        } finally
        {
            if (rs != null)
            {
                try
                {
                    rs.close();
                } catch (SQLException e)
                { /* ignored */}
            }
            if (ps != null)
            {
                try
                {
                    ps.close();
                } catch (SQLException e)
                { /* ignored */}
            }
            if (conn != null)
            {
                try
                {
                    conn.close();
                } catch (SQLException e)
                { /* ignored */}
            }
        }
    }

}