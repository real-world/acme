//--------Marvin Dickson Mendia Calizaya-------
package com.dh.acme.service;

import com.dh.acme.domain.EmployeeTraining;

public interface EmployeeTrainingService extends GenericService<EmployeeTraining>
{
}
