package com.dh.acme.service;


import com.dh.acme.domain.DwGraphReport;
import com.dh.acme.repository.DwReportGraphRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Service
public class DwGraphReportServiceImpl extends GenericServiceImpl<DwGraphReport> implements DwGraphReportService
{
    private DwReportGraphRepository repository;

    public DwGraphReportServiceImpl(DwReportGraphRepository dwrepository)
    {
        this.repository = dwrepository;
    }

    @Override
    protected CrudRepository<DwGraphReport, Long> getRepository()
    {
        return repository;
    }

    // Consultas a base de datos con stored procedures
    @Value("${dwacme.datasource.jdbcUrl}")
    private String url;

    @Value("${acme.datasource.username}")
    private String userName;


    @Value("${acme.datasource.password}")
    private String password;

    @Override
    public List<DwGraphReport> procedureFindAll(String starDate, String endDate) throws ClassNotFoundException
    {
        List<DwGraphReport> results = new ArrayList<>();
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try
        {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            conn = DriverManager.getConnection(url, userName, password);
            //conn = DriverManager.getConnection(url);

            String SPsql = "EXEC SP_ReportEventualityGraphicBetween ?,? ";

            ps = conn.prepareStatement(SPsql);
            ps.setEscapeProcessing(true);
            ps.setQueryTimeout(60000);
            ps.setDate(1, Date.valueOf(starDate));
            ps.setDate(2, Date.valueOf(endDate));

            rs = ps.executeQuery();

            System.out.println("llamo a sp wilder graph");
            while (rs.next())
            {
                DwGraphReport report = new DwGraphReport();

                report.setCountEnventuality(rs.getInt("CountEventuality"));
                report.setEmployeeName(rs.getString("EmployeeName"));
                report.setDateEvent(rs.getString("dateEvent"));
                results.add(report);
            }
        } catch (SQLException error)
        {
            System.out.println(error.getMessage());
        } finally
        {
            if (rs != null)
            {
                try
                {
                    rs.close();
                } catch (SQLException e)
                {
                    System.out.println(e.toString());
                }
            }
            if (ps != null)
            {
                try
                {
                    ps.close();
                } catch (SQLException e)
                {
                    System.out.println(e.toString());
                }
            }
            if (conn != null)
            {
                try
                {
                    conn.close();
                } catch (SQLException e)
                {
                    System.out.println(e.toString());
                }
            }
        }


        return results;
    }


    /*public Area procedureSaveInsert(Area area) throws ClassNotFoundException
    {
       // List<Area> results = new ArrayList<>();
        int userId=100;
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Area area1 = new Area();

        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            conn = DriverManager.getConnection(url, userName, password);

            String SPsql = "EXEC SP_InsertArea ?,?,?";

            ps = conn.prepareStatement(SPsql);
            ps.setEscapeProcessing(true);
            ps.setQueryTimeout(60000);
            ps.setString(1, area.getCode());
            ps.setString(2, area.getName());
            ps.setInt(3, userId);
            rs = ps.executeQuery();
            while (rs.next()) {

                area1.setCode(rs.getString("code"));
                area1.setName(rs.getString("name"));
                area1.setModifiedBy(rs.getInt("ModifiedBy"));
                //results.add(area);
            }
        }
        catch (SQLException error){
            System.out.println(error.getMessage());
        }
        finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) { *//* ignored *//*}
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) { *//* ignored *//*}
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) { *//* ignored *//*}
            }
        }
        return area1;
    }

    public Area procedureSaveUpdate(Area area) throws ClassNotFoundException
    {
        int userId=100;
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Area area2 = new Area();

        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            conn = DriverManager.getConnection(url, userName, password);

            String SPsql = "EXEC SP_UpdateArea ?,?,?,?";

            ps = conn.prepareStatement(SPsql);
            ps.setEscapeProcessing(true);
            ps.setQueryTimeout(60000);
            ps.setLong(1, area.getId());
            ps.setString(2, area.getCode());
            ps.setString(3, area.getName());
            ps.setInt(4, userId);
            rs = ps.executeQuery();
            while (rs.next()) {
                area2.setId(rs.getLong("Id"));
                area2.setCode(rs.getString("code"));
                area2.setName(rs.getString("name"));
                area2.setModifiedBy(rs.getInt("ModifiedBy"));
            }
        }
        catch (SQLException error){
            System.out.println(error.getMessage());
        }
        finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) { *//* ignored *//*}
            }
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) { *//* ignored *//*}
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) { *//* ignored *//*}
            }
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) { *//* ignored *//*}
            }
        }
        return area2;
    }
*/
}