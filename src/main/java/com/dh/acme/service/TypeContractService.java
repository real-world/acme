package com.dh.acme.service;

import com.dh.acme.domain.TypeContract;

import java.io.InputStream;

/**
 * Created by Marcelo on 21/05/2018.
 */
public interface TypeContractService extends GenericService<TypeContract> {
 //   void saveImage(Long id, InputStream inputStream);
}
