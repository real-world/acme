package com.dh.acme.service;

import com.dh.acme.domain.Project;

import java.util.List;

public interface ProjectService extends GenericService<Project> {
    List<Project> procedureFindAll()throws ClassNotFoundException;

    Project procedureSaveInsert(Project project)throws ClassNotFoundException;

    Project procedureSaveUpdate(Project project)throws ClassNotFoundException;
    void procedureDelete(long id)throws ClassNotFoundException;
}