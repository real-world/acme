package com.dh.acme.service;

import com.dh.acme.domain.AuditHistory;

import java.util.List;

public interface AuditHistoryService extends GenericService<AuditHistory> {
    List<AuditHistory> procedureFindAll() throws ClassNotFoundException;
}


