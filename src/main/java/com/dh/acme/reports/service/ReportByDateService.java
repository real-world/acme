package com.dh.acme.reports.service;

import com.dh.acme.reports.model.ReportByDate;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.StoredProcedureQuery;
import java.util.Date;
import java.util.List;


@Service
public class ReportByDateService {

    @PersistenceContext
    private EntityManager entityManager;

    public List<ReportByDate> getReportByDate(Date startDate, Date endDate) {
        StoredProcedureQuery query = entityManager.createNamedStoredProcedureQuery("SP_ReportEventualityBetween");

        query.execute();

        return query.getResultList();
    }
}