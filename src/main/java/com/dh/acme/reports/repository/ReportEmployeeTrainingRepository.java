package com.dh.acme.reports.repository;

import com.dh.acme.reports.model.ReportEmployeeTraining;

import java.util.Date;

public interface ReportEmployeeTrainingRepository {
    ReportEmployeeTraining procedureSelectByDate() throws ClassNotFoundException;
}
