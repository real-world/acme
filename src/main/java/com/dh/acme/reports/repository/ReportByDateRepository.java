package com.dh.acme.reports.repository;

import com.dh.acme.reports.model.ReportByDate;

import java.util.Date;
import java.util.List;

public interface ReportByDateRepository  {

    ReportByDate procedureSelectByDate(Date startDate, Date endDate) throws ClassNotFoundException;
}
