package com.dh.acme.reports.controller;

import com.dh.acme.reports.service.ReportByDateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Date;
import java.util.List;

public class ReportByDateController {

    @Autowired
    private ReportByDateService report;

    // ------------ Retrieve all reportByDate ------------
    @RequestMapping(value = "/reportByDate", method = RequestMethod.POST)
    public List getAllReportByDate(@Param("starDate") Date starDate, @Param("endDate") Date endDate) {
        return report.getReportByDate(starDate, endDate);
    }
}
