package com.dh.acme.reports.model;

import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.NamedStoredProcedureQuery;
import java.io.Serializable;
import java.util.Date;

@NamedStoredProcedureQueries({
        @NamedStoredProcedureQuery(
                name = "SP_ReportEventualityBetween",
                procedureName = "SP_ReportEventualityBetween",
                resultClasses = ReportByDate.class)
})

public class ReportByDate implements Serializable {
    Date dateEvent;
    String DescriptionEvent;
    String EmployeeName;
    String Project;
    String Training;

    public Date getDateEvent() {
        return dateEvent;
    }

    public void setDateEvent(Date dateEvent) {
        this.dateEvent = dateEvent;
    }

    public String getDescriptionEvent() {
        return DescriptionEvent;
    }

    public void setDescriptionEvent(String descriptionEvent) {
        DescriptionEvent = descriptionEvent;
    }

    public String getEmployeeName() {
        return EmployeeName;
    }

    public void setEmployeeName(String employeeName) {
        EmployeeName = employeeName;
    }

    public String getProject() {
        return Project;
    }

    public void setProject(String project) {
        Project = project;
    }

    public String getTraining() {
        return Training;
    }

    public void setTraining(String training) {
        Training = training;
    }
}
