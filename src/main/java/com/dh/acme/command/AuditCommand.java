package com.dh.acme.command;

import com.dh.acme.domain.Area;
import com.dh.acme.domain.Audit;
import com.dh.acme.domain.Employee;
import com.dh.acme.domain.ModelBase;

public class AuditCommand extends ModelBase {
//    private String objective;
//    private String scope;
//    private String auditCriteria;
//    private String auditTeam;
//
//    public String getObjective() {
//        return objective;
//    }
//
//    public void setObjective(String objective) {
//        this.objective = objective;
//    }
//
//    public String getScope() {
//        return scope;
//    }
//
//    public void setScope(String scope) {
//        this.scope = scope;
//    }
//
//    public String getAuditCriteria() {
//        return auditCriteria;
//    }
//
//    public void setAuditCriteria(String auditCriteria) {
//        this.auditCriteria = auditCriteria;
//    }
//
//    public String getAuditTeam() {
//        return auditTeam;
//    }
//
//    public void setAuditTeam(String auditTeam) {
//        this.auditTeam = auditTeam;
//    }
    private String auditName;
    private String auditCode;
    private String auditType;
    private String auditScope;
    private String auditObjective;
    private String auditCriteria;
    private String auditPeriodicity;
    private Employee employee;
    private Area area;


    public String getAuditName() {
        return auditName;
    }

    public void setAuditName(String auditName) {
        this.auditName = auditName;
    }

    public String getAuditCode() {
        return auditCode;
    }

    public void setAuditCode(String auditCode) {
        this.auditCode = auditCode;
    }

    public String getAuditType() {
        return auditType;
    }

    public void setAuditType(String auditType) {
        this.auditType = auditType;
    }

    public String getAuditScope() {
        return auditScope;
    }

    public void setAuditScope(String auditScope) {
        this.auditScope = auditScope;
    }

    public String getAuditObjective() {
        return auditObjective;
    }

    public void setAuditObjective(String auditObjective) {
        this.auditObjective = auditObjective;
    }

    public String getAuditCriteria() {
        return auditCriteria;
    }

    public void setAuditCriteria(String auditCriteria) {
        this.auditCriteria = auditCriteria;
    }

    public String getAuditPeriodicity() {
        return auditPeriodicity;
    }

    public void setAuditPeriodicity(String auditPeriodicity) {
        this.auditPeriodicity = auditPeriodicity;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Area getArea() {
        return area;
    }

    public void setArea(Area area) {
        this.area = area;
    }

    public AuditCommand(){
    }

    public AuditCommand(Audit audit){
        this.setId(audit.getId());
        this.setVersion(audit.getVersion());
        this.setCreatedOn(audit.getCreatedOn());
        this.setUpdatedOn(audit.getUpdatedOn());

//        this.objective = audit.getObjective();
//        this.scope = audit.getScope();
//        this.auditCriteria = audit.getAuditCriteria();
//        this.auditTeam = audit.getAuditTeam();
        this.auditName = audit.getAuditName();
        this.auditCode = audit.getAuditCode();
        this.auditType = audit.getAuditType();
        this.auditScope = audit.getAuditScope();
        this.auditObjective = audit.getAuditObjective();
        this.auditCriteria = audit.getAuditCriteria();
        this.auditPeriodicity = audit.getAuditPeriodicity();
//        this.employee = audit.getEmployee();
//        this.area = audit.getArea();
//colocar employee y area ojooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo

    }

    public Audit toAudit(){
        Audit audit = new Audit();
//        audit.setObjective(getObjective());
//        audit.setScope(getScope());
//        audit.setAuditCriteria(getAuditCriteria());
//        audit.setAuditTeam(getAuditTeam());
        audit.setAuditName(getAuditName());
        audit.setAuditCode(getAuditCode());
        audit.setAuditType(getAuditType());
        audit.setAuditScope(getAuditScope());
        audit.setAuditObjective(getAuditObjective());
        audit.setAuditCriteria(getAuditCriteria());
        audit.setAuditPeriodicity(getAuditPeriodicity());
        audit.setEmployee(getEmployee());
        audit.setArea(getArea());




        audit.setId(getId());
        audit.setVersion(getVersion());
        audit.setCreatedOn(getCreatedOn());
        audit.setUpdatedOn(getUpdatedOn());
        return audit;
    }


}
