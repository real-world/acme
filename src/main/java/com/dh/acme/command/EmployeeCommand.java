package com.dh.acme.command;

import com.dh.acme.domain.Contract;
import com.dh.acme.domain.Employee;
import com.dh.acme.domain.ModelBase;
import org.apache.tomcat.util.codec.binary.Base64;

public class EmployeeCommand extends ModelBase
{
    private String firstName;
    private String lastName;
    private String image;
    private String phone;
    private String email;
    private String dni;
    private String jobPosition;
    private String jobCode;
    private Boolean featured;
    private String jobDescription;
    private String address;

    public String getFirstName()
    {
        return firstName;
    }

    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    public String getLastName()
    {
        return lastName;
    }

    public void setLastName(String lastName)
    {
        this.lastName = lastName;

    }

    public EmployeeCommand()
    {

    }

    public EmployeeCommand(Employee employee)
    {
        setId(employee.getId());
        setAddress(employee.getAddress());
        setCreatedOn(employee.getCreatedOn());
        setUpdatedOn(employee.getUpdatedOn());
        firstName = employee.getFirstName();
        lastName = employee.getLastName();
        setPhone(employee.getPhone());
        setEmail(employee.getEmail());
        setDni(employee.getDni());

        for (Contract contract : employee.getContracts())
        {
            jobPosition = contract.getPosition().getName();
            jobCode = contract.getPosition().getName();
        }

        featured = true;
        setImageBase64(employee);
        setJobDescription(employee.getJobDescription());
        setJobPosition(employee.getJobPosition());
    }

    private void setImageBase64(Employee employee)
    {
        if (employee.getImage() != null)
        {
            byte[] bytes = new byte[employee.getImage().length];
            for (int i = 0; i < employee.getImage().length; i++)
            {
                bytes[i] = employee.getImage()[i];
            }
            String imageStr = Base64.encodeBase64String(bytes);
            this.setImage(imageStr);
        }
    }

    public String getImage()
    {
        return image;
    }

    public void setImage(String image)
    {
        this.image = image;
    }

    public String getJobPosition()
    {
        return jobPosition;
    }

    public void setJobPosition(String jobPosition)
    {
        this.jobPosition = jobPosition;
    }

    public String getJobCode()
    {
        return jobCode;
    }

    public void setJobCode(String jobCode)
    {
        this.jobCode = jobCode;
    }

    public Boolean getFeatured()
    {
        return featured;
    }

    public void setFeatured(Boolean featured)
    {
        this.featured = featured;
    }

    public String getJobDescription()
    {
        return jobDescription;
    }

    public void setJobDescription(String jobDescription)
    {
        this.jobDescription = jobDescription;
    }

    public Employee toEmployee()
    {
        Employee employee = new Employee();
        employee.setFirstName(getFirstName());
        employee.setLastName(getLastName());
        employee.setId(getId());
        employee.setAddress(getAddress());
        employee.setEmail(getEmail());
        employee.setCreatedOn(getCreatedOn());
        employee.setUpdatedOn(getUpdatedOn());
        employee.setPhone(getPhone());
        employee.setDni(getPhone());
        employee.setJobPosition(getJobPosition());
        employee.setJobDescription(getJobDescription());
        return employee;
    }

    public String getPhone()
    {
        return phone;
    }

    public void setPhone(String phone)
    {
        this.phone = phone;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getDni()
    {
        return dni;
    }

    public void setDni(String dni)
    {
        this.dni = dni;
    }

    public String getAddress()
    {
        return address;
    }

    public void setAddress(String address)
    {
        this.address = address;
    }
}
