package com.dh.acme.command;

import com.dh.acme.domain.Area;
import com.dh.acme.domain.Project;
import com.dh.acme.domain.ProjectArea;

import java.util.Date;

public class ProjectAreaCommand {

    private String estado;
    private Project project;
    private Area area;

    public ProjectAreaCommand() {
    }

    public ProjectAreaCommand(ProjectArea project_area) {
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public Area getArea() {
        return area;
    }

    public void setArea(Area area) {
        this.area = area;
    }

    public ProjectArea toProjectArea()
    {
        ProjectArea project_area = new ProjectArea();
        project_area.setEstado(getEstado());
        project_area.setProject(getProject());
        project_area.setArea(getArea());
        return project_area;
    }


}
