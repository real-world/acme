package com.dh.acme.command;

import com.dh.acme.domain.IncidentRegistry;
import com.dh.acme.domain.ModelBase;

public class IncidentRegistryCommand extends ModelBase{

    private Long id;
    private String title;
    private String description;

    public IncidentRegistryCommand(IncidentRegistry incidentRegistry) {
        this.setId(incidentRegistry.getId());
        this.setTitle(incidentRegistry.getTitle());
        this.setDescription(incidentRegistry.getDescription());
        this.setCreatedOn(incidentRegistry.getCreatedOn());
        this.setUpdatedOn(incidentRegistry.getUpdatedOn());
        this.setVersion(incidentRegistry.getVersion());
    }

    public IncidentRegistryCommand(){

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public IncidentRegistry toDomain(){
        IncidentRegistry incidentRegistry = new IncidentRegistry();
        incidentRegistry.setId(getId());
        incidentRegistry.setTitle(getTitle());
        incidentRegistry.setDescription(getDescription());
        incidentRegistry.setCreatedOn(getCreatedOn());
        incidentRegistry.setUpdatedOn(getUpdatedOn());
        incidentRegistry.setVersion(getVersion());
        return incidentRegistry;
    }
}
