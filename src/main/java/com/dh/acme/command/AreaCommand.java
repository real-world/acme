//--------Marvin Dickson Mendia Calizaya-------
package com.dh.acme.command;

import com.dh.acme.domain.Area;
import com.dh.acme.domain.ModelBase;

public class AreaCommand extends ModelBase
{
    private String code;
    private String name;
    private int modifiedBy;

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public int getModifiedBy()
    {
        return modifiedBy;
    }

    public void setModifiedBy(int modifiedBy)
    {
        this.modifiedBy = modifiedBy;
    }

    public AreaCommand()
    {
    }

    public AreaCommand(Area area)
    {
        setId(area.getId());
        setVersion(area.getVersion());
        setCreatedOn(area.getCreatedOn());
        setUpdatedOn(area.getUpdatedOn());
        setModifiedBy(area.getModifiedBy());
        code = area.getCode();
        name = area.getName();
        modifiedBy = area.getModifiedBy();

    }
    public Area toArea()
    {
        Area area = new Area();
        area.setCode(getCode());
        area.setName(getName());
        area.setId(getId());
        area.setVersion(getVersion());
        area.setCreatedOn(getCreatedOn());
        area.setUpdatedOn(getUpdatedOn());
        area.setModifiedBy(getModifiedBy());
        return area;
    }
}