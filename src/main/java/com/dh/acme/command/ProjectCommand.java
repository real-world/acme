package com.dh.acme.command;

import com.dh.acme.domain.Project;

import java.util.Date;

public class ProjectCommand {
    private long id;
    private String name;
    private String description;
    private Date date_start;
    private Date date_end;

    public ProjectCommand() { }

    public ProjectCommand(Project project) {
        setId(project.getId());
        setName(project.getName());
        setDescription(project.getDescription());
        setDate_start(project.getDate_start());
        setDate_end(project.getDate_end());
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDate_start() {
        return date_start;
    }

    public void setDate_start(Date date_start) {
        this.date_start = date_start;
    }

    public Date getDate_end() {
        return date_end;
    }

    public void setDate_end(Date date_end) {
        this.date_end = date_end;
    }

    public Project toProject()
    {
        Project project = new Project();
        project.setName(getName());
        project.setDescription(getDescription());
        project.setId(getId());
        project.setDate_start(getDate_start());
        project.setDate_end(getDate_end());
        return project;
    }

}
