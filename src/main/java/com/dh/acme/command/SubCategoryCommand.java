package com.dh.acme.command;

import com.dh.acme.domain.SubCategory;

public class SubCategoryCommand {

    private String name;
    private String description;
    private String code;
    private Long id;



    public SubCategoryCommand(SubCategory subCategory) {
        this.id = subCategory.getId();
        this.name = subCategory.getName();
        this.code = subCategory.getCode();
        this.description = subCategory.getDescription();
    }

    public SubCategoryCommand() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public SubCategory toSubCategory(){
        SubCategory subCategory = new SubCategory();
        subCategory.setCode(getCode());
        subCategory.setName(getName());
        subCategory.setDescription(getDescription());
        subCategory.setId(getId());
        subCategory.setSubcategoryID(1);

        return subCategory;
     }
}
