//--------Marvin Dickson Mendia Calizaya-------
package com.dh.acme.command;

import com.dh.acme.domain.ModelBase;
import com.dh.acme.domain.Position;

public class PositionCommand extends ModelBase
{
    private String name;
    private String role;
    private Long role_id;

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getRole()
    {
        return role;
    }

    public void setRole(String role)
    {
        this.role = role;
    }

    public Long getRole_id()
    {
        return role_id;
    }

    public void setRole_id(Long role_id)
    {
        this.role_id = role_id;
    }

    public PositionCommand()
    {
    }

    public PositionCommand(Position position)
    {
        this.setId(position.getId());
        this.setVersion(position.getVersion());
        this.setCreatedOn(position.getCreatedOn());
        this.setUpdatedOn(position.getUpdatedOn());
        this.name = position.getName();
        this.role = position.getRole();
        this.role_id = position.getRole_id();

    }
    public Position toPosition()
    {
        Position position = new Position();
        position.setName(getName());
        position.setId(getId());
        position.setVersion(getVersion());
        position.setCreatedOn(getCreatedOn());
        position.setUpdatedOn(getUpdatedOn());
        position.setRole(getRole());
        position.setRole_id(getRole_id());
        return position;
    }
}