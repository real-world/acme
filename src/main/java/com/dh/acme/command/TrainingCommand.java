//--------Marvin Dickson Mendia Calizaya-------
package com.dh.acme.command;

import com.dh.acme.domain.ModelBase;
import com.dh.acme.domain.Training;

public class TrainingCommand extends ModelBase
{
    private String name;
    private String code;
    private String instructor;
    private String area;
    private Long area_id;

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getInstructor()
    {
        return instructor;
    }

    public void setInstructor(String instructor)
    {
        this.instructor = instructor;
    }

    public String getArea()
    {
        return area;
    }

    public void setAreaCode(String area)
    {
        this.area = area;
    }

    public Long getArea_id()
    {
        return area_id;
    }

    public void setArea_id(Long area_id)
    {
        this.area_id = area_id;
    }

    public TrainingCommand()
    {
    }

    public TrainingCommand(Training training)
    {
        this.setId(training.getId());
        this.setVersion(training.getVersion());
        this.setCreatedOn(training.getCreatedOn());
        this.setUpdatedOn(training.getUpdatedOn());
        this.name = training.getName();
        this.code = training.getCode();
        this.instructor = training.getInstructor();
        this.area = training.getArea();
        this.area_id = training.getArea_id();
    }
    public Training toTraining()
    {
        Training training = new Training();
        training.setName(getName());
        training.setCode(getCode());
        training.setInstructor(getInstructor());
        training.setId(getId());
        training.setVersion(getVersion());
        training.setCreatedOn(getCreatedOn());
        training.setUpdatedOn(getUpdatedOn());
        training.setArea(getArea());
        training.setArea_id(getArea_id());
        return training;
    }

}