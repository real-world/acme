//--------Marvin Dickson Mendia Calizaya-------
package com.dh.acme.command;

import com.dh.acme.domain.ModelBase;
import com.dh.acme.domain.Role;

public class RoleCommand extends ModelBase
{
    private String code;
    private String description;
    private int modifiedBy;

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public int getModifiedBy()
    {
        return modifiedBy;
    }

    public void setModifiedBy(int modifiedBy)
    {
        this.modifiedBy = modifiedBy;
    }
    public RoleCommand()
    {
    }

    public RoleCommand(Role role)
    {
        this.setId(role.getId());
        this.setVersion(role.getVersion());
        this.setCreatedOn(role.getCreatedOn());
        this.setUpdatedOn(role.getUpdatedOn());
        this.description = role.getDescription();
        this.code = role.getCode();
        modifiedBy = role.getModifiedBy();
    }

    public Role toRole()
    {
        Role role = new Role();
        role.setId(getId());
        role.setVersion(getVersion());
        role.setCreatedOn(getCreatedOn());
        role.setUpdatedOn(getUpdatedOn());
        role.setCode(getCode());
        role.setDescription(getDescription());
        role.setModifiedBy(getModifiedBy());
        return role;
    }
}