package com.dh.acme.command;

import com.dh.acme.domain.DwReport;
import com.dh.acme.domain.ModelBase;

import java.sql.Date;

public class DwReportCommand extends ModelBase
{
    private Date dateEvent;
    private String descriptionEvent;
    private String employeName;
    private String project;
    private String training;


    public DwReportCommand()
    {
    }

    public DwReportCommand(DwReport report)
    {
        setId(report.getId());
        setVersion(report.getVersion());
        setDateEvent(report.getDateEvent());//setCreatedOn(report.getCreatedOn());
        setDescriptionEvent(report.getDescriptionEvent());
        setEmployeName(report.getEmployeName());
        setProject(report.getProject());
        setTraining(report.getTraining());

        // setUpdatedOn(report.getUpdatedOn());
        // setModifiedBy(report.getModifiedBy());
        dateEvent = report.getDateEvent();
        descriptionEvent = report.getDescriptionEvent();
        employeName = report.getEmployeName();
        project = report.getProject();
        training = report.getTraining();

    }

    public DwReport toDwReport()
    {
        DwReport report = new DwReport();
        report.setDateEvent(getDateEvent());
        report.setDescriptionEvent(getDescriptionEvent());
        report.setEmployeName(getEmployeName());
        report.setProject(getProject());
        report.setTraining(getTraining());

        return report;
    }


    public Date getDateEvent()
    {
        return dateEvent;
    }

    public void setDateEvent(Date dateEvent)
    {
        this.dateEvent = dateEvent;
    }

    public String getDescriptionEvent()
    {
        return descriptionEvent;
    }

    public void setDescriptionEvent(String descriptionEvent)
    {
        this.descriptionEvent = descriptionEvent;
    }

    public String getEmployeName()
    {
        return employeName;
    }

    public void setEmployeName(String employeName)
    {
        this.employeName = employeName;
    }

    public String getProject()
    {
        return project;
    }

    public void setProject(String project)
    {
        this.project = project;
    }

    public String getTraining()
    {
        return training;
    }

    public void setTraining(String training)
    {
        this.training = training;
    }
}