package com.dh.acme.command;

import com.dh.acme.domain.Audit;
import com.dh.acme.domain.ModelBase;
import com.dh.acme.domain.SafetyRule;

public class SafetyRuleCommand extends ModelBase {
    private String policyCode;
    private String policyName;
    private Integer complianceParameter;
    private Integer complianceMetric;
    private Boolean accomplishment;
    private Audit audit;

    public String getPolicyCode() {
        return policyCode;
    }

    public void setPolicyCode(String policyCode) {
        this.policyCode = policyCode;
    }

    public String getPolicyName() {
        return policyName;
    }

    public void setPolicyName(String policyName) {
        this.policyName = policyName;
    }

    public Integer getComplianceParameter() {
        return complianceParameter;
    }

    public void setComplianceParameter(Integer complianceParameter) {
        this.complianceParameter = complianceParameter;
    }

    public Integer getComplianceMetric() {
        return complianceMetric;
    }

    public void setComplianceMetric(Integer complianceMetric) {
        this.complianceMetric = complianceMetric;
    }

    public Boolean getAccomplishment() {
        return accomplishment;
    }

    public void setAccomplishment(Boolean accomplishment) {
        this.accomplishment = accomplishment;
    }

    public Audit getAudit() {
        return audit;
    }

    public void setAudit(Audit audit) {
        this.audit = audit;
    }

    public SafetyRuleCommand(){
    }

    public SafetyRuleCommand(SafetyRule safetyRule){
        this.setId(safetyRule.getId());
        this.setVersion(safetyRule.getVersion());
        this.setCreatedOn(safetyRule.getCreatedOn());
        this.setUpdatedOn(safetyRule.getUpdatedOn());
        this.policyCode = safetyRule.getPolicyCode();
        this.policyName = safetyRule.getPolicyName();
        this.complianceParameter = safetyRule.getComplianceParameter();
        this.complianceMetric = safetyRule.getComplianceMetric();
        this.accomplishment = safetyRule.getAccomplishment();


    }

    public SafetyRule toSafetyRule(){
        SafetyRule safetyRule = new SafetyRule();
        safetyRule.setPolicyCode(getPolicyCode());
        safetyRule.setPolicyName(getPolicyName());
        safetyRule.setComplianceParameter(getComplianceParameter());
        safetyRule.setComplianceMetric(getComplianceMetric());
//        safetyRule.setAccomplishment(getAccomplishment());
        safetyRule.setAccomplishment(accomplishmentCalc(getAccomplishment()));
        safetyRule.setAudit(audit);


        return safetyRule;
    }

    private Boolean accomplishmentCalc(Boolean accomplishment) {
        if (getComplianceMetric() >= getComplianceParameter()) {
            return true;
        }

        return false;
    }


}
