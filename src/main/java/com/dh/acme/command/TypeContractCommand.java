package com.dh.acme.command;

import com.dh.acme.domain.ModelBase;
import com.dh.acme.domain.TypeContract;

/**
 * Created by Marcelo on 21/05/2018.
 */
public class TypeContractCommand extends ModelBase {

    private String typeContract;
    private String responsable;
    private String description;
    private String contractCode;
    private String contractAmount;

    public TypeContractCommand(TypeContract typeContract) {
        setId(typeContract.getId());
        setCreatedOn(typeContract.getCreatedOn());
        setUpdatedOn(typeContract.getUpdatedOn());
        this.typeContract = typeContract.getTypeContract();
        this.responsable = typeContract.getResponsable();
        this.description = typeContract.getDescription();
    }

    public TypeContractCommand() {
    }

    public TypeContract toTypeContract() {

        TypeContract typeContract = new TypeContract();
        typeContract.setId(getId());
        typeContract.setCreatedOn(getCreatedOn());
        typeContract.setUpdatedOn(getUpdatedOn());
        typeContract.setTypeContract(getTypeContract());
        typeContract.setDescription(getDescription());
        typeContract.setResponsable(getResponsable());
        return typeContract;
    }

    public String getTypeContract() {
        return typeContract;
    }

    public void setTypeContract(String typeContract) {
        this.typeContract = typeContract;
    }

    public String getResponsable() {
        return responsable;
    }

    public void setResponsable(String responsable) {
        this.responsable = responsable;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getContractCode() {
        return contractCode;
    }

    public void setContractCode(String contractCode) {
        this.contractCode = contractCode;
    }

    public String getContractAmount() {
        return contractAmount;
    }


}
