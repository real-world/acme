//--------Marvin Dickson Mendia Calizaya-------
package com.dh.acme.command;

import com.dh.acme.domain.EmployeeTraining;
import com.dh.acme.domain.ModelBase;

public class EmployeeTrainingCommand extends ModelBase
{
    private Long trainingId;
    private String name;
    private String instructor;
    private  Long employeeId;
    private String firstName;
    private String lastName;

    public EmployeeTrainingCommand()
    {
    }

    public Long getTrainingId()
    {
        return trainingId;
    }

    public void setTrainingId(Long trainingId)
    {
        this.trainingId = trainingId;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getInstructor()
    {
        return instructor;
    }

    public void setInstructor(String instructor)
    {
        this.instructor = instructor;
    }

    public Long getEmployeeId()
    {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId)
    {
        this.employeeId = employeeId;
    }

    public String getFirstName()
    {
        return firstName;
    }

    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    public String getLastName()
    {
        return lastName;
    }

    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

    public EmployeeTrainingCommand(EmployeeTraining employeeTraining)
    {
        setId(employeeTraining.getId());
        setCreatedOn(employeeTraining.getCreatedOn());
        setUpdatedOn(employeeTraining.getUpdatedOn());
        setVersion(employeeTraining.getVersion());
        trainingId = employeeTraining.getTrainingId();
        name = employeeTraining.getName();
        instructor = employeeTraining.getInstructor();
        employeeId = employeeTraining.getEmployeeId();
        firstName = employeeTraining.getFirstName();
        lastName = employeeTraining.getLastName();
    }

    public EmployeeTraining toEmployeeTraining()
    {
        EmployeeTraining employeeTraining = new EmployeeTraining();
        employeeTraining.setTrainingId(getTrainingId());
        employeeTraining.setName(getName());
        employeeTraining.setInstructor(getInstructor());
        employeeTraining.setFirstName(getFirstName());
        employeeTraining.setLastName(getLastName());
        employeeTraining.setId(getId());
        employeeTraining.setCreatedOn(getCreatedOn());
        employeeTraining.setUpdatedOn(getUpdatedOn());
        employeeTraining.setVersion(getVersion());
        return employeeTraining;
    }
}