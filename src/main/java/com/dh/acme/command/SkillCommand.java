//--------Marvin Dickson Mendia Calizaya-------
package com.dh.acme.command;

import com.dh.acme.domain.ModelBase;
import com.dh.acme.domain.Position;
import com.dh.acme.domain.Skill;

public class SkillCommand extends ModelBase
{
    private String code;
    private String description;

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getDescription()
    {
        return description;
    }

    public SkillCommand()
    {
    }

    public SkillCommand(Skill skill)
    {
        this.setId(skill.getId());
        this.setVersion(skill.getVersion());
        this.setCreatedOn(skill.getCreatedOn());
        this.setUpdatedOn(skill.getUpdatedOn());
        this.description = skill.getDescription();
        this.code = skill.getCode();
    }

    public Skill toSkill()
    {
        Skill skill = new Skill();
        skill.setCode(getCode());
        skill.setDescription(getDescription());
        skill.setId(getId());
        skill.setVersion(getVersion());
        skill.setCreatedOn(getCreatedOn());
        skill.setUpdatedOn(getUpdatedOn());
        return skill;
    }
}