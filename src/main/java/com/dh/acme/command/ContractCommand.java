package com.dh.acme.command;

import com.dh.acme.domain.*;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created by Marcelo on 21/05/2018.
 */
public class ContractCommand extends ModelBase {
    private String contractCode;
    private String paymentType;
    private String contractAmount;
    private String initDate;
    private String endDate;
    private Long typeContractId;
    // private String employee;
    private Long positionId;
    private Long projectId;
    private Long employeeId;
    private Employee employee;
    private Position position;
    private Project project;
    private TypeContract typeContract;

    private String employeeFullName;
    private String projectName;
    private String typeContractName;

    public ContractCommand() {
    }

    public ContractCommand(Contract contract) {
        contractCode = contract.getContractCode();
        paymentType = contract.getPaymentType();
        contractAmount = contract.getContractAmount();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        initDate = dateFormat.format(contract.getInitDate());
        endDate = dateFormat.format(contract.getEndDate());
        positionId = contract.getPosition().getId();
//        projectId = contract.getProject().getId();
        employeeId = contract.getEmployee().getId();
        typeContractId = contract.getTypeContract().getId();
        projectName = contract.getProject().getName();
        employeeFullName = contract.getEmployee().getFirstName() + " " + contract.getEmployee().getLastName();
        typeContractName = contract.getTypeContract().getTypeContract();
        setId(contract.getId());
        setCreatedOn(contract.getCreatedOn());
        setUpdatedOn(contract.getUpdatedOn());
    }

    public Contract toContract() {
        Contract contract = new Contract();
        contract.setContractCode(getContractCode());
        contract.setPaymentType(getPaymentType());
        contract.setId(getId());
        contract.setContractAmount(getContractAmount());
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            contract.setInitDate(dateFormat.parse(getInitDate()));
            contract.setEndDate(dateFormat.parse(getEndDate()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        contract.setProject(project);
        contract.setEmployee(employee);
        contract.setCreatedOn(getCreatedOn());
        contract.setTypeContract(typeContract);
        contract.setPosition(position);
//        contract.setUpdatedOn(getUpdatedOn());
        return contract;
    }


    public String getContractCode() {
        return contractCode;
    }

    public void setContractCode(String contractCode) {
        this.contractCode = contractCode;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getContractAmount() {
        return contractAmount;
    }

    public void setContractAmount(String contractAmount) {
        this.contractAmount = contractAmount;
    }

    public Long getTypeContractId() {
        return typeContractId;
    }

    public void setTypeContractId(Long typeContractId) {
        this.typeContractId = typeContractId;
    }

    public String getInitDate() {
        return initDate;
    }

    public void setInitDate(String initDate) {
        this.initDate = initDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public Long getPositionId() {
        return positionId;
    }

    public void setPositionId(Long positionId) {
        this.positionId = positionId;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public Long getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Long employeeId) {
        this.employeeId = employeeId;
    }


    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public void setProject(Project project) {
        this.project = project;
    }

    public TypeContract getTypeContract() {
        return typeContract;
    }

    public void setTypeContract(TypeContract typeContract) {
        this.typeContract = typeContract;
    }

    public String getEmployeeFullName() {
        return employeeFullName;
    }

    public void setEmployeeFullName(String employeeFullName) {
        this.employeeFullName = employeeFullName;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getTypeContractName() {
        return typeContractName;
    }

    public void setTypeContractName(String typeContractName) {
        this.typeContractName = typeContractName;
    }
}
