package com.dh.acme.command;

import com.dh.acme.domain.Employee;
import com.dh.acme.domain.Eventuality;
import com.dh.acme.domain.ModelBase;
import org.springframework.data.annotation.Transient;

import java.util.Date;

public class EventualityCommand extends ModelBase{

    private Long id;
    private String type;
    private Date dateEvent;
    private String injuryType;
    private String injuryPart;
    private String description;
    private int typeEvent_id;

    @Transient
    private String employee;

    private String employeeId;
    private String emp;

    public EventualityCommand(Eventuality eventuality){
        this.setId(eventuality.getId());
        this.setType(eventuality.getTypeEvent());
        this.setDateEvent(eventuality.getDateEvent());
        this.setInjuryType(eventuality.getInjuryType());
        this.setInjuryPart(eventuality.getInjuryPart());
        this.setDescription(eventuality.getDescription());
        this.setCreatedOn(eventuality.getCreatedOn());
        this.setUpdatedOn(eventuality.getUpdatedOn());
        this.setVersion(eventuality.getVersion());
        //this.setIncidentRegistry(eventuality.getIncidentRegistry());
        // this.setEmployeeId(eventuality.getEmployee().getId());
        this.setEmployeeId (eventuality.getEmployee());
        //this.(eventuality.getEmployee().getId());
        this.setTypeEvent_id(eventuality.getTypeEvent_id());
    }

    public EventualityCommand(){
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDateEvent() {
        return dateEvent;
    }

    public void setDateEvent(Date dateEvent) {
        this.dateEvent = dateEvent;
    }

    public String getInjuryType() {
        return injuryType;
    }

    public void setInjuryType(String injuryType) {
        this.injuryType = injuryType;
    }

    public String getInjuryPart() {
        return injuryPart;
    }

    public void setInjuryPart(String injuryPart) {
        this.injuryPart = injuryPart;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmployee() {
        return employee;
    }

    public void setEmployee(String employee) {
        this.employee = employee;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String incidentRegistryId) {
        this.employeeId = incidentRegistryId;
    }

    public int getTypeEvent_id() {
        return typeEvent_id;
    }

    public void setTypeEvent_id(int typeEvent_id) {
        this.typeEvent_id = typeEvent_id;
    }

    public Eventuality toDomain(){
        Eventuality eventuality = new Eventuality();
        eventuality.setId(getId());
        eventuality.setTypeEvent(getType());
        eventuality.setDateEvent(getDateEvent());
        eventuality.setInjuryType(getInjuryType());
        eventuality.setInjuryPart(getInjuryPart());
        eventuality.setDescription(getDescription());
        eventuality.setVersion(getVersion());
        eventuality.setCreatedOn(getCreatedOn());
        eventuality.setUpdatedOn(getUpdatedOn());
        eventuality.setEmployee(getEmployee());
        //incidentRegistry = eventuality.getIncidentRegistry();
        //eventuality.setIncidentRegistry(incidentRegistry.setId(getIncidentRegistryId());
        //eventuality.setIncidentRegistry(i);

        return eventuality;
    }

    /*public Long getEmpId()
    {
        return empId;
    }

    public void setEmpId(Long empId)
    {
        this.empId = empId;
    }*/
}
