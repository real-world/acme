package com.dh.acme.command;

import com.dh.acme.domain.Category;
import com.dh.acme.domain.SubCategory;
import com.dh.acme.service.SubCategoryService;

public class CategoryCommand {

    private String name;
    private String description;
    private String code;
    private Long id;
    private SubCategoryService subCategoryService;
    private Long subCategoryId;
    private SubCategory subCategory;


    public CategoryCommand(Category category) {
        this.id = category.getId();
        this.name = category.getName();
        this.code = category.getCode();
        this.description = category.getDescription();
    }

    public CategoryCommand() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public Long getSubCategoryId() {
        return subCategoryId;
    }

    public void setSubCategoryId(Long subCategoryId) {
        this.subCategoryId = subCategoryId;
    }

    public SubCategoryService getSubCategoryService() {
        return subCategoryService;
    }

    public void setSubCategoryService(SubCategoryService subCategoryService) {
        this.subCategoryService = subCategoryService;
    }

    public SubCategory getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(SubCategory subCategory) {
        this.subCategory = subCategory;
    }

    public Category toCategory(){
        Category category = new Category();
        category.setCode(getCode());
        category.setName(getName());
        category.setDescription(getDescription());
        category.setSubCategory(getSubCategory());
        return category;
     }
}
