package com.dh.acme.repository;

import com.dh.acme.domain.IncidentRegistry;
import org.springframework.data.repository.CrudRepository;

public interface IncidentRegistryRepository extends CrudRepository<IncidentRegistry, Long> {
}
