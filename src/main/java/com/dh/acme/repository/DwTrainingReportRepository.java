//--------Marvin Dickson Mendia Calizaya-------
package com.dh.acme.repository;

import com.dh.acme.domain.DwTrainingReport;
import org.springframework.data.repository.CrudRepository;

public interface DwTrainingReportRepository extends CrudRepository<DwTrainingReport, Long>
{/*
    Optional<List<Area>> findByCode(String code);

    @Override
    List<Area> findAll();*/
}
