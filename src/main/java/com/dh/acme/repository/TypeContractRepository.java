package com.dh.acme.repository;

import com.dh.acme.domain.TypeContract;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Marcelo on 21/05/2018.
 */
public interface TypeContractRepository extends CrudRepository<TypeContract,Long> {
}
