package com.dh.acme.repository;

import com.dh.acme.domain.SubCategory;
import org.springframework.data.repository.CrudRepository;

public interface SubCategoryRepository extends CrudRepository<SubCategory, Long> {
}
