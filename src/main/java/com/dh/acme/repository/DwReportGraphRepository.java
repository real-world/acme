//--------Marvin Dickson Mendia Calizaya-------
package com.dh.acme.repository;

import com.dh.acme.domain.DwGraphReport;
import org.springframework.data.repository.CrudRepository;

public interface DwReportGraphRepository extends CrudRepository<DwGraphReport, Long>
{
}
