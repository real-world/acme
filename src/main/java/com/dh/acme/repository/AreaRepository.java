//--------Marvin Dickson Mendia Calizaya-------
package com.dh.acme.repository;

import com.dh.acme.domain.Area;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface AreaRepository extends CrudRepository<Area, Long>
{/*
    Optional<List<Area>> findByCode(String code);

    @Override
    List<Area> findAll();*/
}
