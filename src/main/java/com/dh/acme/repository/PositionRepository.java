package com.dh.acme.repository;

import com.dh.acme.domain.Position;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PositionRepository extends CrudRepository<Position, Long> {
    /*@Override
    List<Position> findAll();*/

    @Query("SELECT p " +
            "FROM Position p " +
            "WHERE p.id not in (" +
            "SELECT c.position.id " +
            "From Contract c " +
            "WHERE c.position.id is not null)")
    List<Position> getAllPositionNotAssigned();
}
