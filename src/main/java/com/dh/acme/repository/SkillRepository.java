//--------Marvin Dickson Mendia Calizaya-------
package com.dh.acme.repository;

import com.dh.acme.domain.Skill;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface SkillRepository extends CrudRepository<Skill, Long>
{
    Optional<List<Skill>> findByCode(String code);

    @Override
    List<Skill> findAll();
}
