package com.dh.acme.repository;

import com.dh.acme.domain.Employee;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface EmployeeRepository extends CrudRepository<Employee, Long> {

    @Query("SELECT e " +
            "FROM Employee e" +
            " WHERE LOWER(e.firstName) like %:search% " +
            "or LOWER(e.lastName) like %:search%")
    List<Employee> getByFirstNameAndLastName(@Param("search") String search, Pageable pageable);
}
