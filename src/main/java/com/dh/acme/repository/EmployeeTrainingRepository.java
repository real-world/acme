//--------Marvin Dickson Mendia Calizaya-------
package com.dh.acme.repository;

import com.dh.acme.domain.EmployeeTraining;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface EmployeeTrainingRepository extends CrudRepository<EmployeeTraining, Long>
{
    @Override
    List<EmployeeTraining> findAll();
}
