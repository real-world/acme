package com.dh.acme.repository;

import com.dh.acme.domain.SafetyRule;
import org.springframework.data.repository.CrudRepository;

public interface SafetyRuleRepository extends CrudRepository<SafetyRule, Long> {
}
