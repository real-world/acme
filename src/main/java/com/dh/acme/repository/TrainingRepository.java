//--------Marvin Dickson Mendia Calizaya-------
package com.dh.acme.repository;

import com.dh.acme.domain.Training;
import org.springframework.data.repository.CrudRepository;

public interface TrainingRepository extends CrudRepository<Training, Long>
{
    //Optional<List<Training>> findByCode(String code);

    /*@Override
    List<Training> findAll();*/
}