package com.dh.acme.repository;

import com.dh.acme.domain.Category;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface CategoryRepository extends CrudRepository<Category, Long> {
    Optional<List<Category>> findByCode(String code);

    @Override
    Optional<Category> findById(Long aLong);

    @Override
    List<Category> findAll();
}
