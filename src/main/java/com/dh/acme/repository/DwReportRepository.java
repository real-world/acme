//--------Marvin Dickson Mendia Calizaya-------
package com.dh.acme.repository;

import com.dh.acme.domain.DwReport;
import org.springframework.data.repository.CrudRepository;

public interface DwReportRepository extends CrudRepository<DwReport, Long>
{/*
    Optional<List<Area>> findByCode(String code);

    @Override
    List<Area> findAll();*/
}
