package com.dh.acme.repository;

import com.dh.acme.domain.AuditHistory;
import org.springframework.data.repository.CrudRepository;

public interface AuditHistoryRepository extends CrudRepository<AuditHistory, Long>{
}

