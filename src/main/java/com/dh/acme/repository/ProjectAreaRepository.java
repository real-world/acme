package com.dh.acme.repository;

import com.dh.acme.domain.ProjectArea;
import org.springframework.data.repository.CrudRepository;

public interface ProjectAreaRepository extends CrudRepository<ProjectArea, Long> {

}

