package com.dh.acme.repository;

import com.dh.acme.domain.ReportEventuality;
import org.springframework.data.repository.CrudRepository;

public interface ReportEventualityRepository extends CrudRepository<ReportEventuality, Long> {

}
