//--------Marvin Dickson Mendia Calizaya-------
package com.dh.acme.repository;

import com.dh.acme.domain.DwReportEmployeeTypeEvent;
import org.springframework.data.repository.CrudRepository;

public interface DwReportEmpleadoTypeEventRepository extends CrudRepository<DwReportEmployeeTypeEvent, Long> {
}
