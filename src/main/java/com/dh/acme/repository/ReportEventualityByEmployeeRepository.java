package com.dh.acme.repository;

import com.dh.acme.domain.ReportEventualityByEmployee;
import org.springframework.data.repository.CrudRepository;

public interface ReportEventualityByEmployeeRepository extends CrudRepository<ReportEventualityByEmployee, Long> {

}
