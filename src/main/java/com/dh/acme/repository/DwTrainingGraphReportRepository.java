//--------Marvin Dickson Mendia Calizaya-------
package com.dh.acme.repository;

import com.dh.acme.domain.DwTrainingGraphReport;
import org.springframework.data.repository.CrudRepository;

public interface DwTrainingGraphReportRepository extends CrudRepository<DwTrainingGraphReport, Long>
{
}
