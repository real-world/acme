package com.dh.acme.repository;

import com.dh.acme.domain.Eventuality;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface EventualityRepository extends CrudRepository<Eventuality, Long> {
    List<Eventuality> findAllById(int i);

    //List<Eventuality> findByEmployeeId(Long employeeId);
}
