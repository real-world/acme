package com.dh.acme.repository;

import com.dh.acme.domain.Audit;
import org.springframework.data.repository.CrudRepository;


public interface AuditRepository extends CrudRepository<Audit, Long> {
}
