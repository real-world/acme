//--------Marvin Dickson Mendia Calizaya-------
package com.dh.acme.controller;

import com.dh.acme.domain.DwReport;
import com.dh.acme.reports.request.RangeDate;
import com.dh.acme.service.DwReportService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;


@Path("/report")
@Produces(MediaType.APPLICATION_JSON)
@Controller
@CrossOrigin
public class DwReportController
{
    private DwReportService service;

    public DwReportController(DwReportService service)
    {
        this.service = service;
    }

    @POST
    public Response getReport(RangeDate rangeDate)
    {

        //System.out.printf(rangeDate.getStartDate());
        //System.out.printf(rangeDate.getStartDate());
        List<DwReport> reportList = new ArrayList<>();
        try
        {
            reportList = service.procedureFindAll(rangeDate.getStartDate(),rangeDate.getEndDate());
        } catch (Exception e)
        {
            System.out.println(e.toString());
        }
        Response.ResponseBuilder responseBuilder = Response.ok(reportList);
        return responseBuilder.build();
    }


}