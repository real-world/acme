//--------Marvin Dickson Mendia Calizaya-------
package com.dh.acme.controller;

import com.dh.acme.command.EmployeeTrainingCommand;
import com.dh.acme.domain.EmployeeTraining;
import com.dh.acme.service.EmployeeTrainingService;
import org.springframework.stereotype.Controller;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Path("/employeeTraining")
@Produces(MediaType.APPLICATION_JSON)
@Controller
public class EmployeeTrainingController
{
    private EmployeeTrainingService service;
    public EmployeeTrainingController(EmployeeTrainingService service)
    {
        this.service = service;
    }

    @GET
    public Response getEmployeeTraininies()
    {
        List<EmployeeTrainingCommand> employeeTrainingList = new ArrayList<>();
        service.findAll().forEach(employeeTraining -> {
            employeeTrainingList.add(new EmployeeTrainingCommand(employeeTraining));
        });
        Response.ResponseBuilder responseBuilder = Response.ok(employeeTrainingList);
        return responseBuilder.build();
    }

    @GET
    @Path("/{id}")
    public Response getPositionById(@PathParam("id") long id)
    {
        EmployeeTraining employeeTraining = service.findById(id);
        Response.ResponseBuilder responseBuilder = Response.ok(employeeTraining);
        return responseBuilder.build();
    }

    @OPTIONS
    public Response prefligth()
    {
        Response.ResponseBuilder responseBuilder = Response.ok();
        responseBuilder.allow("OPTIONS").build();
        return responseBuilder.build();
    }

    @POST
    public Response addEmployeeTraining(EmployeeTrainingCommand employeeTrainingCommand)
    {
        EmployeeTraining employeeTraining = service.save(employeeTrainingCommand.toEmployeeTraining());
        return getResponse(employeeTraining, false);
    }


    @PUT
    public EmployeeTrainingCommand updateEmployeeTraining(EmployeeTrainingCommand employeeTrainingCommand)
    {
        EmployeeTraining employeeTraining = service.save(employeeTrainingCommand.toEmployeeTraining());
        return new EmployeeTrainingCommand(employeeTraining);
    }

    private Response getResponse(EmployeeTraining employeeTraining, Boolean flag)
    {
        if (null == employeeTraining)
        {
            return Response.status(404).build();
        }
        Response.ResponseBuilder responseBuilder = null;
        if (!flag)
        {
            responseBuilder = Response.ok();
        } else
        {
            responseBuilder = Response.ok(new EmployeeTrainingCommand(employeeTraining));
        }

        return responseBuilder.build();
    }
    @DELETE
    @Path("/{id}")
    public void deleteEmployeeTraining(@PathParam("id") long id)
    {
        service.deleteById(id);
    }

}