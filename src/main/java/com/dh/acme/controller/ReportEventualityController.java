//package com.dh.acme.controller;
//
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.CrossOrigin;
//import com.dh.acme.domain.ReportEventuality;
//import com.dh.acme.service.ReportEventualityService;
//
//import javax.ws.rs.*;
//import javax.ws.rs.core.MediaType;
//import javax.ws.rs.core.Response;
//import java.util.ArrayList;
//import java.util.List;
//
//import javax.ws.rs.GET;
//import javax.ws.rs.Path;
//import javax.ws.rs.Produces;
//import javax.ws.rs.core.MediaType;
//
//@Path("/reportEventualities")
//@Produces(MediaType.APPLICATION_JSON)
//@Controller
//@CrossOrigin
//public class ReportEventualityController {
//
//    private ReportEventualityService service;
//
//    public ReportEventualityController(ReportEventualityService service)
//    {
//        this.service = service;
//    }
//
//    @GET
//    public Response getReportEventualities()
//    {
//        List<ReportEventuality> reportEventualityList = new ArrayList<>();
//        try {
//            reportEventualityList = service.procedureFindAll();
//        }
//        catch (Exception e){
//            System.out.println(e.toString());
//        }
//        Response.ResponseBuilder responseBuilder = Response.ok(reportEventualityList);
//        return responseBuilder.build();
//    }
//}
