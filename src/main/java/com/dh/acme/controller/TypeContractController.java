package com.dh.acme.controller;

import com.dh.acme.command.TypeContractCommand;
import com.dh.acme.domain.TypeContract;
import com.dh.acme.service.TypeContractService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marcelo on 21/05/2018.
 */

@Path("/typeContract")
@Produces(MediaType.APPLICATION_JSON)
@Controller
@CrossOrigin
public class TypeContractController {
    private TypeContractService typeContractService;

    public TypeContractController(TypeContractService typeContractService) {
        this.typeContractService = typeContractService;
    }

    @GET
    public Response getAllTypeContract() {
        List<TypeContractCommand> typeContractList = new ArrayList<>();
        List<TypeContract> typeContracts = typeContractService.findAll();
        typeContracts.forEach(typeContract -> {
            typeContractList.add(new TypeContractCommand(typeContract));
        });
        Response.ResponseBuilder responseBuilder = Response.ok(typeContractList);
        return responseBuilder.build();
    }


    @GET
    @Path("/{id}")
    public Response getTypeContractById(@PathParam("id") Long id) {
        TypeContract typeContract = typeContractService.findById(id);
        return getResponse(typeContract, true);
    }

    @OPTIONS
    public Response prefligth() {
        Response.ResponseBuilder responseBuilder = Response.ok();
        responseBuilder.allow("OPTIONS").build();
        return responseBuilder.build();
    }

    @POST
    public Response addTypeContract(TypeContractCommand typeContractCommand) {
        TypeContract typeContract = typeContractService.save(typeContractCommand.toTypeContract());
//        return getResponse(typeContract, false);
        Response.ResponseBuilder responseBuilder = Response.ok(new TypeContractCommand(typeContract));
        return responseBuilder.build();
    }

    @PUT
    public Response updateEmployee(TypeContractCommand typeContractCommand) {
        TypeContract typeContract = typeContractService.save(typeContractCommand.toTypeContract());
//        return getResponse(typeContract, false);
        Response.ResponseBuilder responseBuilder = Response.ok(new TypeContractCommand(typeContract));
        return responseBuilder.build();
    }

    private Response getResponse(TypeContract typeContract, Boolean flag) {
        if (null == typeContract) {
            return Response.status(404).build();
        }
        Response.ResponseBuilder responseBuilder = null;
        if (!flag) {
            responseBuilder = Response.ok();
        } else {
            responseBuilder = Response.ok(new TypeContractCommand(typeContract));
        }

        return responseBuilder.build();
    }

    @DELETE
    @Path("/{id}")
    public void deleteTypeContract(@PathParam("id") long id) {
        typeContractService.deleteById(id);
    }

}
