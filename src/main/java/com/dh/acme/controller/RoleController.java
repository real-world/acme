//--------Marvin Dickson Mendia Calizaya-------
package com.dh.acme.controller;

import com.dh.acme.command.RoleCommand;
import com.dh.acme.domain.Role;
import com.dh.acme.service.RoleService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;


@Path("/roles")
@Produces(MediaType.APPLICATION_JSON)
@Controller
@CrossOrigin
public class RoleController
{
    private RoleService service;
    public RoleController(RoleService service)
    {
        this.service = service;
    }

    @GET
    public Response getRoles()
    {
        /*List<RoleCommand> roleList = new ArrayList<>();
        service.findAll().forEach(role -> {
            roleList.add(new RoleCommand(role));
        });*/
        List<Role> roleList = new ArrayList<>();
        try
        {
            roleList = service.procedureFindAll();
        } catch (Exception e)
        {
            System.out.println(e.toString());
        }
        Response.ResponseBuilder responseBuilder = Response.ok(roleList);
        return responseBuilder.build();
    }

    @GET
    @Path("/{id}")
    public Response getRoleById(@PathParam("id") long id)
    {
        Role role = service.findById(id);
        Response.ResponseBuilder responseBuilder = Response.ok(role);
        return responseBuilder.build();
    }

    @OPTIONS
    public Response prefligth()
    {
        Response.ResponseBuilder responseBuilder = Response.ok();
        responseBuilder.allow("OPTIONS").build();
        return responseBuilder.build();
    }

    @POST
    public Response addRole(RoleCommand roleCommand) throws ClassNotFoundException
    {

        Role role = service.procedureSaveInsert(roleCommand.toRole());
        Response.ResponseBuilder responseBuilder = Response.ok(role);
        return responseBuilder.build();
    }

    /*@PUT
    public RoleCommand updateRole(RoleCommand roleCommand)
    {
        Role role = service.save(roleCommand.toRole());
        return new RoleCommand(role);
    }*/

    @PUT
    public Response updateRole(RoleCommand roleCommand) throws ClassNotFoundException
    {
        Role role = service.procedureSaveUpdate(roleCommand.toRole());
        Response.ResponseBuilder responseBuilder = Response.ok(role);
        return responseBuilder.build();
    }

    private Response getResponse(Role role, Boolean flag)
    {
        if (null == role)
        {
            return Response.status(404).build();
        }
        Response.ResponseBuilder responseBuilder = null;
        if (!flag)
        {
            responseBuilder = Response.ok();
        } else
        {
            responseBuilder = Response.ok(new RoleCommand(role));
        }
        return responseBuilder.build();
    }

    @DELETE
    @Path("/{id}")
    public void deleteRole(@PathParam("id") Long id) throws ClassNotFoundException
    {
        service.procedureDelete(id);
    }

}