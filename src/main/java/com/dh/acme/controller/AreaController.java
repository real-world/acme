//--------Marvin Dickson Mendia Calizaya-------
package com.dh.acme.controller;

import com.dh.acme.command.AreaCommand;
import com.dh.acme.domain.Area;
import com.dh.acme.service.AreaService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;


@Path("/areas")
@Produces(MediaType.APPLICATION_JSON)
@Controller
@CrossOrigin
public class AreaController
{
    private AreaService service;
    public AreaController(AreaService service)
    {
        this.service = service;
    }

    @GET
    public Response getAreas()
    {
        /*       List<AreaCommand> areaList = new ArrayList<>();
        service.findAll().forEach(area -> {
            areaList.add(new AreaCommand(area));
        });*/

        List<Area> areaList = new ArrayList<>();
        try {
            areaList = service.procedureFindAll();
        }
        catch (Exception e){
            System.out.println(e.toString());
        }
        Response.ResponseBuilder responseBuilder = Response.ok(areaList);
        return responseBuilder.build();
    }

    @GET
    @Path("/{id}")
    public Response getAreaById(@PathParam("id") long id)
    {
        Area area = service.findById(id);
        Response.ResponseBuilder responseBuilder = Response.ok(area);
        return responseBuilder.build();
    }

    @OPTIONS
    public Response prefligth()
    {
        Response.ResponseBuilder responseBuilder = Response.ok();
        responseBuilder.allow("OPTIONS").build();
        return responseBuilder.build();
    }

    @POST
    public Response addArea(AreaCommand areaCommand) throws ClassNotFoundException
    {

        Area area = service.procedureSaveInsert(areaCommand.toArea());
        Response.ResponseBuilder responseBuilder = Response.ok(area);
        return responseBuilder.build();
    }


/*    @PUT
    public AreaCommand updateArea(AreaCommand areaCommand)
    {
        Area area = service.save(areaCommand.toArea());
        System.out.println(area.getId()+" "+ area.getCreatedOn() + " "+ area.getUpdatedOn()+ " "+ area.getVersion()+ " " + area.getCode() + "ggggg");
        return new AreaCommand(area);
    }*/

    @PUT
    public Response updateArea(AreaCommand areaCommand) throws ClassNotFoundException
    {
        Area area = service.procedureSaveUpdate(areaCommand.toArea());
        Response.ResponseBuilder responseBuilder = Response.ok(area);
        return responseBuilder.build();
    }

    private Response getResponse(Area area, Boolean flag)
    {
        if (null == area)
        {
            return Response.status(404).build();
        }
        Response.ResponseBuilder responseBuilder = null;
        if (!flag)
        {
            responseBuilder = Response.ok();
        } else
        {
            responseBuilder = Response.ok(new AreaCommand(area));
        }

        return responseBuilder.build();
    }

    @DELETE
    @Path("/{id}")
    public void deleteArea(@PathParam("id") long id) throws ClassNotFoundException
    {
        service.procedureDelete(id);
    }
    /*public void deleteArea(@PathParam("id") long id)
    {
        service.deleteById(id);
    }*/
}