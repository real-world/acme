package com.dh.acme.controller;

import com.dh.acme.command.ProjectCommand;
import com.dh.acme.domain.Project;
import com.dh.acme.service.ProjectService;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.Response;

@Path("/project")
@Produces(MediaType.APPLICATION_JSON)
@Controller
@CrossOrigin
@Transactional
public class ProjectController {

    private ProjectService service;
    public ProjectController(ProjectService service) {
        this.service = service;
    }

    //    @GET
//    public Response getProjects() {
//        List<ProjectCommand> projectList = new ArrayList<>();
//        service.findAll().forEach(project -> {
//            ProjectCommand addProject=new ProjectCommand(project);
//            projectList.add(addProject);
//            });
//        Response.ResponseBuilder responseBuilder = Response.ok(projectList);
//        return responseBuilder.build();
//    }
    @GET
    public Response getProjects() {
        List<Project> projectList = new ArrayList<>();
        try {
            projectList = service.procedureFindAll();
        }
        catch (Exception e){
            System.out.println(e.toString());
        }
        Response.ResponseBuilder responseBuilder = Response.ok(projectList);
        return responseBuilder.build();
    }

    @GET
    @Path("/{id}")
    public Response getProjectById(@PathParam("id") @NotNull Long id)
    {
        Project project = service.findById(id);
        if(null == project){
            return Response.status(404).build();
        }
        ProjectCommand projectCommand = new ProjectCommand(project);
        Response.ResponseBuilder responseBuilder = Response.status(200).entity(projectCommand);
        return responseBuilder.build();
    }

    @POST
//    public Response saveProject(ProjectCommand projectCommand)
//    {
//        Project project = projectCommand.toProject();
//        Project projectPersisted = service.save(project);
//        Response.ResponseBuilder responseBuilder = Response.ok(new ProjectCommand(projectPersisted));
//        return responseBuilder.build();
//    }

    public Response addProject(ProjectCommand projectCommand) throws ClassNotFoundException
    {

        Project project = service.procedureSaveInsert(projectCommand.toProject());
        Response.ResponseBuilder responseBuilder = Response.ok(project);
        return responseBuilder.build();
    }


    @PUT
    public Response updateArea(ProjectCommand projectCommand) throws ClassNotFoundException
    {
        Project project = service.procedureSaveUpdate(projectCommand.toProject());
        Response.ResponseBuilder responseBuilder = Response.ok(project);
        return responseBuilder.build();
    }

    @DELETE
    @Path("/{id}")
    public void deleteProject(@PathParam("id") long id) throws ClassNotFoundException {
        service.procedureDelete(id);
    }

//    public Response deleteProject(@PathParam("id") String id){
//        service.deleteById(Long.valueOf(id));
//        Response.ResponseBuilder responseBuilder = Response.ok();
//        return responseBuilder.build();
//    }

}
