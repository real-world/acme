//--------Marvin Dickson Mendia Calizaya-------
package com.dh.acme.controller;

import com.dh.acme.command.SkillCommand;
import com.dh.acme.domain.Skill;
import com.dh.acme.service.SkillService;
import org.springframework.stereotype.Controller;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;


@Path("/skills")
@Produces(MediaType.APPLICATION_JSON)
@Controller
public class SkillController
{
    private SkillService service;
    public SkillController(SkillService service)
    {
        this.service = service;
    }

    @GET
    public Response getSkills()
    {
        List<SkillCommand> skillList = new ArrayList<>();
        service.findAll().forEach(skill -> {
            skillList.add(new SkillCommand(skill));
        });
        Response.ResponseBuilder responseBuilder = Response.ok(skillList);
        return responseBuilder.build();
    }

    @GET
    @Path("/{id}")
    public Response getSkillById(@PathParam("id") long id)
    {
        Skill skill = service.findById(id);
        Response.ResponseBuilder responseBuilder = Response.ok(skill);
        return responseBuilder.build();
    }

    @OPTIONS
    public Response prefligth()
    {
        Response.ResponseBuilder responseBuilder = Response.ok();
        responseBuilder.allow("OPTIONS").build();
        return responseBuilder.build();
    }

    @POST
    public Response addSkill(SkillCommand skillCommand)
    {
        Skill skill = service.save(skillCommand.toSkill());
        Response.ResponseBuilder responseBuilder = Response.ok(skill);
        return responseBuilder.build();
    }

    @PUT
    public SkillCommand updateSkill(SkillCommand skillCommand)
    {
        Skill skill = service.save(skillCommand.toSkill());
        return new SkillCommand(skill);
    }

    private Response getResponse(Skill skill, Boolean flag)
    {
        if (null == skill)
        {
            return Response.status(404).build();
        }
        Response.ResponseBuilder responseBuilder = null;
        if (!flag)
        {
            responseBuilder = Response.ok();
        } else
        {
            responseBuilder = Response.ok(new SkillCommand(skill));
        }

        return responseBuilder.build();
    }
    @DELETE
    @Path("/{id}")
    public void deleteSkill(@PathParam("id") long id)
    {
        service.deleteById(id);
    }
}