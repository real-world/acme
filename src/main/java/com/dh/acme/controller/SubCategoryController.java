package com.dh.acme.controller;


import com.dh.acme.command.SubCategoryCommand;
import com.dh.acme.domain.Category;
import com.dh.acme.domain.SubCategory;
import com.dh.acme.repository.CategoryRepository;
import com.dh.acme.repository.SubCategoryRepository;
import com.dh.acme.service.CategoryService;
import com.dh.acme.service.SubCategoryService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;


@Controller
@Path("/subcategories")
@Produces("application/json")
@CrossOrigin
public class SubCategoryController {

    private SubCategoryService subCategoryService;

    private SubCategoryRepository subCategoryRepository;

    private CategoryRepository categoryRepository;

    private CategoryService categoryService;

    public SubCategoryController(SubCategoryService subCategoryService) {
        this.subCategoryService = subCategoryService;
    }


    @GET
    public Response getSubCategories(@QueryParam("code") String code)
    {
        List<SubCategory> subCategories = new ArrayList<>();
        subCategories = subCategoryService.findAll();
//        subCategoryService.findAll().forEach(subcategory -> {
//            SubCategoryCommand subCategoryCommand = new SubCategoryCommand(subcategory);
//            subCategories.add(subCategoryCommand);
//        });
        if(null == subCategories){
            return Response.status(404).build();
        }
        Response.ResponseBuilder responseBuilder = Response.ok(subCategories);
        return responseBuilder.build();
    }


    @GET
    @Path("/{id}")
    public Response getSubCategoriesById(@PathParam("id") @NotNull Long id)
    {
        SubCategory subCategory = subCategoryService.findById(id);
        if(null == subCategory){
            return Response.status(404).build();
        }
        Response.ResponseBuilder responseBuilder = Response.status(200).entity(subCategory);
        return responseBuilder.build();
    }

    @POST
    public Response saveSubCategory(SubCategory subCategory)
    {
        //SubCategory subCategory = subcategoryCommand.toSubCategory();
        subCategory.setCategory(new Category());
        SubCategory subCategoryPersisted = subCategoryRepository.save(subCategory);
        Response.ResponseBuilder responseBuilder = Response.ok(subCategoryPersisted);
        return responseBuilder.build();
    }

    @PUT
    public Response updateSubcategory(SubCategory subCategory)
    {
        //Category category = categoryRepository.findById(1l);
        Category category = categoryService.findById(1l);
        subCategory.setCategory(category);
        SubCategory subCategoryPersisted = subCategoryRepository.save(subCategory);
        Response.ResponseBuilder responseBuilder = Response.ok(subCategoryPersisted);
        return responseBuilder.build();
    }

//    @DELETE
//    @Path("/{id}")
//    public Response deleteSubCategory(@PathParam("id") String id)
//    {
//        subCategoryService.deleteById(Long.valueOf(id));
//        Response.ResponseBuilder responseBuilder = Response.ok();
//        return responseBuilder.build();
//    }


    @DELETE
    @Path("/{id}")
    public void deleteSubCategory(@PathParam("id") long id)
    {
        subCategoryService.deleteById(id);
    }






}
