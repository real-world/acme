package com.dh.acme.controller;

import com.dh.acme.domain.DwReportEmployeeTypeEvent;
import com.dh.acme.service.DwGraphReportEmployeeTypeEventService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Path("/reportGraph")
@Produces(MediaType.APPLICATION_JSON)
@Controller
@CrossOrigin
public class DwGraphReportEmployeeTypeEventController {
    private DwGraphReportEmployeeTypeEventService service;

    public DwGraphReportEmployeeTypeEventController(DwGraphReportEmployeeTypeEventService service) {
        this.service = service;
    }

    @GET
    @Path("/{employee}/{typeEvent}/{project}/{dateIni}/{dateEnd}")
    public Response getReportEmployeInjury(@PathParam("employee") String employee,
                                           @PathParam("typeEvent") String typeEvent,
                                           @PathParam("project") Long project,
                                           @PathParam("dateIni") String dateIni,
                                           @PathParam("dateEnd") String dateEnd) {
        List<DwReportEmployeeTypeEvent> reportList = new ArrayList<>();
        try {
            reportList = service.procedureEmployeeTypeEvent(employee, project, typeEvent, dateIni, dateEnd);
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        Response.ResponseBuilder responseBuilder = Response.ok(reportList);
        return responseBuilder.build();
    }


}
