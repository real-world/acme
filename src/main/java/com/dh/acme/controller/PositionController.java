//--------Marvin Dickson Mendia Calizaya-------
package com.dh.acme.controller;

import com.dh.acme.command.PositionCommand;
import com.dh.acme.domain.Position;
import com.dh.acme.service.PositionService;
import com.dh.acme.service.RoleService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;


@Path("/positions")
@Produces(MediaType.APPLICATION_JSON)
@Controller
@CrossOrigin
public class PositionController
{
    private PositionService service;
    private RoleService roleService;

    public PositionController(PositionService service, RoleService roleService)
    {
        this.service = service;
        this.roleService = roleService;
    }

    @GET
    public Response getPositions()
    {
        /*List<PositionCommand> positionList = new ArrayList<>();
        service.findAll().forEach(position -> {
            positionList.add(new PositionCommand(position));
        });*/
        List<PositionCommand> positionList = new ArrayList<>();
        try
        {
            //positionList = service.procedureFindAll();
            service.procedureFindAll().forEach(position -> {
                positionList.add(new PositionCommand(position));
            });
        } catch (Exception e)
        {
            System.out.println(e.toString());
        }
        Response.ResponseBuilder responseBuilder = Response.ok(positionList);
        return responseBuilder.build();
    }

    @GET
    @Path("/{id}")
    public Response getPositionById(@PathParam("id") long id)
    {
        Position position = service.findById(id);
        Response.ResponseBuilder responseBuilder = Response.ok(new PositionCommand(position));

        return responseBuilder.build();
        /*Position position = service.findById(id);
        Response.ResponseBuilder responseBuilder = Response.ok(position);
        return responseBuilder.build();*/
    }

    @OPTIONS
    public Response prefligth()
    {
        Response.ResponseBuilder responseBuilder = Response.ok();
        //addCorsHeader(responseBuilder);
        responseBuilder.allow("OPTIONS").build();
        return responseBuilder.build();
    }

    @POST
    public Response addPosition(PositionCommand positionCommand) throws ClassNotFoundException
    {

        Position position = service.procedureSaveInsert(positionCommand.toPosition());
        Response.ResponseBuilder responseBuilder = Response.ok(position);
        return getResponse(position, false);
    }

/*    @PUT
    public PositionCommand updatePosition(PositionCommand positionCommand)
    {
        Position position = service.save(positionCommand.toPosition());
        return new PositionCommand(position);
    }*/

    @PUT
    public Response updatePosition(PositionCommand positionCommand) throws ClassNotFoundException
    {
        Position position = service.procedureSaveUpdate(positionCommand.toPosition());
        Response.ResponseBuilder responseBuilder = Response.ok(position);
        //return responseBuilder.build();
        return getResponse(position, false);
    }

    private Response getResponse(Position position, Boolean flag)
    {
        if (null == position)
        {
            return Response.status(404).build();
        }
        Response.ResponseBuilder responseBuilder = null;
        if (!flag)
        {
            responseBuilder = Response.ok();
        } else
        {
            responseBuilder = Response.ok(new PositionCommand(position));
        }

        return responseBuilder.build();
    }
    @DELETE
    @Path("/{id}")
    public void deletePosition(@PathParam("id") long id) throws ClassNotFoundException
    {
        service.procedureDelete(id);
    }
}