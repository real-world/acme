//--------Marvin Dickson Mendia Calizaya-------
package com.dh.acme.controller;

import com.dh.acme.domain.DwReport;
import com.dh.acme.domain.DwTrainingReport;
import com.dh.acme.service.DwReportService;
import com.dh.acme.service.DwTrainingReportService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;


@Path("/reportTraining")
@Produces(MediaType.APPLICATION_JSON)
@Controller
@CrossOrigin
public class DwTrainingReportController
{
    private DwTrainingReportService service;

    public DwTrainingReportController(DwTrainingReportService service)
    {
        this.service = service;
    }

    @GET
    public Response getReport()
    {
        //       List<AreaCommand> areaList = new ArrayList<>();
//        service.findAll().forEach(area -> {
//            areaList.add(new AreaCommand(area));
//        });
        List<DwTrainingReport> reportList = new ArrayList<>();
        try
        {
            reportList = service.procedureFindAll();
        } catch (Exception e)
        {
            System.out.println(e.toString());
        }
        Response.ResponseBuilder responseBuilder = Response.ok(reportList);
        return responseBuilder.build();
    }


}