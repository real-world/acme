//--------Marvin Dickson Mendia Calizaya-------
package com.dh.acme.controller;


import com.dh.acme.domain.DwTrainingGraphReport;
import com.dh.acme.service.DwTrainingGraphReportService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;


@Path("/employeeTrainingGraph")
@Produces(MediaType.APPLICATION_JSON)
@Controller
@CrossOrigin
public class DwTrainingGraphReportController
{
    private DwTrainingGraphReportService service;

    public DwTrainingGraphReportController(DwTrainingGraphReportService service)
    {
        this.service = service;
    }

    @GET
    public Response getReport()
    {
        List<DwTrainingGraphReport> reportList = new ArrayList<>();
        try
        {
            reportList = service.procedureFindAll();
        } catch (Exception e)
        {
            System.out.println(e.toString());
        }
        Response.ResponseBuilder responseBuilder = Response.ok(reportList);
        return responseBuilder.build();
    }


}