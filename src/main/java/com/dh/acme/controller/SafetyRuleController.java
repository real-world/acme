package com.dh.acme.controller;


import com.dh.acme.command.AuditCommand;
import com.dh.acme.command.SafetyRuleCommand;
import com.dh.acme.domain.SafetyRule;
import com.dh.acme.service.SafetyRuleService;
import org.springframework.stereotype.Controller;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Path("/safetyRules")
@Produces(MediaType.APPLICATION_JSON)
@Controller
public class SafetyRuleController {
    private SafetyRuleService service;
    public SafetyRuleController(SafetyRuleService service){
        this.service = service;
    }

    @GET
    public Response getSafetyRules(){
        List<SafetyRuleCommand> safetyRuleList = new ArrayList<>();
        service.findAll().forEach(safetyRule -> {
                    safetyRuleList.add(new SafetyRuleCommand(safetyRule));
                }
        );
        Response.ResponseBuilder responseBuilder = Response.ok(safetyRuleList);
        return responseBuilder.build();
    }

    @GET
    @Path("/{id}")
    public Response getAuditById(@PathParam("id") long id)
    {
        SafetyRule safetyRule = service.findById(id);

        Response.ResponseBuilder responseBuilder = Response.ok(safetyRule);
        return responseBuilder.build();
    }

    @OPTIONS
    public Response prefligth()
    {
        Response.ResponseBuilder responseBuilder = Response.ok();

        responseBuilder.allow("OPTIONS").build();
        return responseBuilder.build();
    }

    @POST
    public Response addSafetyRule(SafetyRuleCommand safetyRuleCommand)
    {
        SafetyRule safetyRule = service.save(safetyRuleCommand.toSafetyRule());
        return getResponse(safetyRule, false);
    }

    @PUT
    public SafetyRuleCommand updateSafetyRule(SafetyRuleCommand safetyRuleCommand)
    {
        SafetyRule safetyRule = service.save(safetyRuleCommand.toSafetyRule());
        return new SafetyRuleCommand(safetyRule);
    }

    private Response getResponse(SafetyRule safetyRule, Boolean flag)
    {
        if (null == safetyRule)
        {
            return Response.status(404).build();
        }
        Response.ResponseBuilder responseBuilder = null;
        if (!flag)
        {
            responseBuilder = Response.ok();
        } else
        {
            responseBuilder = Response.ok(new SafetyRuleCommand(safetyRule));
        }

        return responseBuilder.build();
    }
    @DELETE
    @Path("/{id}")
    public void deleteAudit(@PathParam("id") long id)
    {
        service.deleteById(id);
    }


}
