package com.dh.acme.controller;

import com.dh.acme.domain.EmployeeId;
import com.dh.acme.domain.ReportEventualityByEmployee;
import com.dh.acme.service.ReportEventualityByEmployeeService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Path("/reportaccidentbyemployee")
@Produces(MediaType.APPLICATION_JSON)
@Controller
@CrossOrigin
public class ReportEventualityByEmployeeController {

    private ReportEventualityByEmployeeService service;

    public ReportEventualityByEmployeeController(ReportEventualityByEmployeeService service)
    {
        this.service = service;
    }

    @POST
    public Response getReport(EmployeeId employeeId)
    {
        List<ReportEventualityByEmployee> ReportEventualityByEmployeeList = new ArrayList<>();
        try
        {
            ReportEventualityByEmployeeList = service.procedureFindAll(employeeId.getEmployeeId());
        } catch (Exception e)
        {
            System.out.println(e.toString());
        }
        Response.ResponseBuilder responseBuilder = Response.ok(ReportEventualityByEmployeeList);
        return responseBuilder.build();
    }
}
