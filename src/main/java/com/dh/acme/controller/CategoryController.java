package com.dh.acme.controller;

import com.dh.acme.command.CategoryCommand;
import com.dh.acme.domain.Category;
import com.dh.acme.domain.SubCategory;
import com.dh.acme.service.CategoryService;
import com.dh.acme.service.SubCategoryService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;


@Controller
@Path("/categories")
@Produces("application/json")
@CrossOrigin
public class CategoryController {

    private CategoryService categoryService;
    private SubCategoryService subCategoryService;

    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }


    @GET
    public Response getCategories(@QueryParam("code") String code)
    {
//        List<CategoryCommand> categories = new ArrayList<>();
//        categoryService.findAll().forEach(category -> {
//            CategoryCommand categoryCommand = new CategoryCommand(category);
//            categories.add(categoryCommand);
//        });
//        if(null == categories){
//            return Response.status(404).build();
//        }
//        Response.ResponseBuilder responseBuilder = Response.ok(categories);
//        return responseBuilder.build();


        List<CategoryCommand> categoryCommandList = new ArrayList<>();
        categoryService.findAll().forEach(category -> {
            categoryCommandList.add(new CategoryCommand(category));
        });
        Response.ResponseBuilder responseBuilder = Response.ok(categoryCommandList);
        return responseBuilder.build();
    }


    @GET
    @Path("/{id}")
    public Response getCategoriesById(@PathParam("id") @NotNull Long id)
    {
        Category category = categoryService.findById(id);
        if(null == category){
            return Response.status(404).build();
        }
        Response.ResponseBuilder responseBuilder = Response.status(200).entity(category);
        return responseBuilder.build();
    }

    @POST
    public Response saveCategory(CategoryCommand categoryCommand)
    {
        Category category = categoryCommand.toCategory();
        Category categoryPersisted = categoryService.save(category);
        Response.ResponseBuilder responseBuilder = Response.ok(new CategoryCommand(categoryPersisted));
        return responseBuilder.build();
    }

    @PUT
    public Response updateCategory(Category category)
    {
        Category categoryPersisted = categoryService.save(category);
        Response.ResponseBuilder responseBuilder = Response.ok(new CategoryCommand(categoryPersisted));
        return responseBuilder.build();
    }

//    @DELETE
//    @Path("/{id}")
//    public Response deleteCategory(@PathParam("id") String id)
//    {
//        categoryService.deleteById(Long.valueOf(id));
//        Response.ResponseBuilder responseBuilder = Response.ok();
//        return responseBuilder.build();
//    }

    @DELETE
    @Path("/{id}")
    public void deleteCategory(@PathParam("id") long id)
    {   Category category= categoryService.findById(id);
        SubCategory subCategory=category.getSubCategory();
        if(null!=subCategory){
            subCategoryService.deleteById(subCategory.getId());
            categoryService.deleteById(id);
        }
        categoryService.deleteById(id);
    }





}
