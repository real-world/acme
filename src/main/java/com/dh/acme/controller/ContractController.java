package com.dh.acme.controller;

import com.dh.acme.command.ContractCommand;
import com.dh.acme.domain.Contract;
import com.dh.acme.service.*;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marcelo on 21/05/2018.
 */

@Path("/contract")
@Produces(MediaType.APPLICATION_JSON)
@Controller
@CrossOrigin
public class ContractController {

    private ContractService contractService;
    private PositionService positionService;
    private ProjectService projectService;
    private EmployeeService employeeService;
    private TypeContractService typeContractService;

    public ContractController(ContractService contractService,
                              PositionService positionService,
                              ProjectService projectService,
                              EmployeeService employeeService,
                              TypeContractService typeContractService) {
        this.contractService = contractService;
        this.employeeService = employeeService;
        this.positionService = positionService;
        this.projectService = projectService;
        this.typeContractService = typeContractService;
    }

    @POST
    public Response addContract(ContractCommand contractCommand) {
        contractCommand.setProject(projectService.findById(contractCommand.getProjectId()));
        contractCommand.setEmployee(employeeService.findById(contractCommand.getEmployeeId()));
        contractCommand.setTypeContract(typeContractService.findById(contractCommand.getTypeContractId()));
        contractCommand.setPosition(positionService.findById(contractCommand.getPositionId()));
        Contract contract = contractService.save(contractCommand.toContract());
        return getResponse(contract, false);
    }

    @PUT
    //@Path(value = "update")
    public Response updateContract(ContractCommand contractCommand) {
        Contract contract = contractService.save(contractCommand.toContract());
        return getResponse(contract, false);
    }

    private Response getResponse(Contract contract, Boolean flag) {
        if (null == contract) {
            return Response.status(404).build();
        }
        Response.ResponseBuilder responseBuilder = null;
        if (!flag) {
            responseBuilder = Response.ok();
        } else {
            responseBuilder = Response.ok(new ContractCommand(contract));
        }
        return responseBuilder.build();
    }

//    @OPTIONS
//    public Response prefligth() {
//        Response.ResponseBuilder responseBuilder = Response.ok();
//        responseBuilder.allow("OPTIONS").build();
//        return responseBuilder.build();
//    }

    @GET
    @Path("/{id}")
    public Response getContractById(@PathParam("id") Long id) {
        Contract contract = contractService.findById(id);
        return getResponse(contract, true);
    }


    @GET
    public Response getContract() {
        List<ContractCommand> contractList = new ArrayList<>();
        contractService.findAll().forEach(contracted -> {
            contractList.add(new ContractCommand(contracted));
        });
        Response.ResponseBuilder responseBuilder = Response.ok(contractList);
        return responseBuilder.build();
    }

    @DELETE
    @Path("/{id}")
    public void deleteEmployee(@PathParam("id") long id) {
        contractService.deleteById(id);
    }


}
