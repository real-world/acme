//--------Marvin Dickson Mendia Calizaya-------
package com.dh.acme.controller;

import com.dh.acme.domain.DwGraphReport;
import com.dh.acme.reports.request.RangeDate;
import com.dh.acme.service.DwGraphReportService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;


@Path("/reportGraph")
@Produces(MediaType.APPLICATION_JSON)
@Controller
@CrossOrigin
public class DwGraphReportController
{
    private DwGraphReportService service;

    public DwGraphReportController(DwGraphReportService service)
    {
        this.service = service;
    }

    @POST
    public Response getReport(RangeDate rangeDate)
    {
        List<DwGraphReport> reportList = new ArrayList<>();
        try
        {
            reportList = service.procedureFindAll(rangeDate.getStartDate(), rangeDate.getEndDate());
        } catch (Exception e)
        {
            System.out.println(e.toString());
        }
        Response.ResponseBuilder responseBuilder = Response.ok(reportList);
        return responseBuilder.build();
    }


}