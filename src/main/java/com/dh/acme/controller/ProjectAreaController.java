package com.dh.acme.controller;

import com.dh.acme.command.ProjectAreaCommand;
import com.dh.acme.domain.ProjectArea;
import com.dh.acme.service.ProjectAreaService;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.ws.rs.*;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


@Path("/projectArea")
@Produces(MediaType.APPLICATION_JSON)
@Controller
@CrossOrigin
@Transactional
public class ProjectAreaController {

    private ProjectAreaService service;

    public ProjectAreaController(ProjectAreaService service) {
        this.service = service;
    }

//    @GET
//    public Response getProjectArea()
//    {
//        List<ProjectArea> projectAreatList = new ArrayList<>();
//        try {
//            projectAreatList = service.procedureFindAll();
//        }
//        catch (Exception e){
//            System.out.println(e.toString());
//        }
//        Response.ResponseBuilder responseBuilder = Response.ok(projectAreatList);
//        return responseBuilder.build();
//    }

    @GET
    @Path("/{id}")
    public Response getProjectAreaById(@PathParam("id") Long id) throws ClassNotFoundException
    {
        ProjectArea projectArea = service.findById(id);
        return getResponse(projectArea, true);
    }


    @POST
    public Response saveProjectArea(ProjectAreaCommand projectAreaCommand)throws ClassNotFoundException
    {
        ProjectArea projectArea1 = projectAreaCommand.toProjectArea();
        ProjectArea projectArea = service.procedureSaveInsert(projectArea1);
        Response.ResponseBuilder responseBuilder = Response.ok(projectArea);
        return responseBuilder.build();

    }
    private Response getResponse(ProjectArea projectArea, Boolean flag)
    {
        if (null == projectArea)
        {
            return Response.status(404).build();
        }
        Response.ResponseBuilder responseBuilder = null;
        if (!flag)
        {
            responseBuilder = Response.ok();
        } else
        {
            responseBuilder = Response.ok(new ProjectAreaCommand(projectArea));
        }
        return responseBuilder.build();
    }
}
