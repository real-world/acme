package com.dh.acme.controller;

import com.dh.acme.command.EmployeeCommand;
import com.dh.acme.domain.Employee;
import com.dh.acme.service.EmployeeService;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

@Path("/employees")
@Produces(MediaType.APPLICATION_JSON)
@Controller
@CrossOrigin
@Transactional
public class EmployeeController
{
    private EmployeeService service;

    public EmployeeController(EmployeeService service)
    {
        this.service = service;
    }

    @GET
    public Response getEmployees() throws ClassNotFoundException
    {
        List<EmployeeCommand> employeeList = new ArrayList<>();
        service.procedureFindAllEmployees().forEach(employee -> {
            employeeList.add(new EmployeeCommand(employee));
        });
        Response.ResponseBuilder responseBuilder = Response.ok(employeeList);
        return responseBuilder.build();
    }


    @GET
    @Path("/{id}")
    public Response getEmployeeById(@PathParam("id") Long id) throws ClassNotFoundException
    {
        Employee employee = service.findById(id);
        return getResponse(employee, true);
    }

    @OPTIONS
    public Response prefligth()
    {
        Response.ResponseBuilder responseBuilder = Response.ok();
        responseBuilder.allow("OPTIONS").build();
        return responseBuilder.build();
    }

    @POST
    public Response addEmployee(EmployeeCommand employeeCommand) throws ClassNotFoundException
    {
        Employee employee = service.procedureSaveInsertEmployee(employeeCommand.toEmployee());
        return getResponse(employee, false);
    }

    @PUT
    public Response updateEmployee(EmployeeCommand employeeCommand) throws ClassNotFoundException
    {
        Employee employee = service.procedureSaveUpdateEmployee(employeeCommand.toEmployee());
        return getResponse(employee, false);
    }

    private Response getResponse(Employee employee, Boolean flag)
    {
        if (null == employee)
        {
            return Response.status(404).build();
        }
        Response.ResponseBuilder responseBuilder = null;
        if (!flag)
        {
            responseBuilder = Response.ok();
        } else
        {
            responseBuilder = Response.ok(new EmployeeCommand(employee));
        }
        return responseBuilder.build();
    }

    @DELETE
    @Path("/{id}")
    public void deleteEmployee(@PathParam("id") long id) throws ClassNotFoundException
    {
        service.procedureDeleteEmployee(id);
    }

    @Path("/{id}/image")
    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    public Response uploadFile(@PathParam("id") String id,
                               @FormDataParam("file") InputStream file,
                               @FormDataParam("file") FormDataContentDisposition fileDisposition)
    {
        service.saveImage(Long.valueOf(id), file);
        return Response.ok("Data uploaded successfully !!").build();
    }

    @GET
    @Path("/search/{search}")
    public Response getEmployeesLike(@PathParam("search") String search)
    {
        List<EmployeeCommand> employeeList = new ArrayList<>();
        service.getEmployeeLike(search).forEach(employee -> {
            employeeList.add(new EmployeeCommand(employee));
        });
        Response.ResponseBuilder responseBuilder = Response.ok(employeeList);
        return responseBuilder.build();
    }

}
