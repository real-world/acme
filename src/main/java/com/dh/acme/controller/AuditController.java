package com.dh.acme.controller;


import com.dh.acme.command.AuditCommand;
import com.dh.acme.domain.Audit;
import com.dh.acme.service.AuditService;
import org.springframework.stereotype.Controller;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Path("/audits")
@Produces(MediaType.APPLICATION_JSON)
@Controller
public class AuditController {
    private AuditService service;
    public AuditController(AuditService service){
        this.service = service;
    }

    @GET
    public Response getAudits(){
        List<AuditCommand> auditList = new ArrayList<>();
        service.findAll().forEach(audit -> {
            auditList.add(new AuditCommand(audit));
                }
        );
        Response.ResponseBuilder responseBuilder = Response.ok(auditList);
        return responseBuilder.build();
    }

    @GET
    @Path("/{id}")
    public Response getAuditById(@PathParam("id") long id)
    {
        Audit audit = service.findById(id);

        Response.ResponseBuilder responseBuilder = Response.ok(audit);
        return responseBuilder.build();
    }

    @OPTIONS
    public Response prefligth()
    {
        Response.ResponseBuilder responseBuilder = Response.ok();

        responseBuilder.allow("OPTIONS").build();
        return responseBuilder.build();
    }

    @POST
    public Response addAudit(AuditCommand auditCommand)
    {
        Audit audit = service.save(auditCommand.toAudit());
        return getResponse(audit, false);
    }

    @PUT
    public AuditCommand updateAudit(AuditCommand auditCommand)
    {
        Audit audit = service.save(auditCommand.toAudit());
        return new AuditCommand(audit);
    }

    private Response getResponse(Audit audit, Boolean flag)
    {
        if (null == audit)
        {
            return Response.status(404).build();
        }
        Response.ResponseBuilder responseBuilder = null;
        if (!flag)
        {
            responseBuilder = Response.ok();
        } else
        {
            responseBuilder = Response.ok(new AuditCommand(audit));
        }

        return responseBuilder.build();
    }
    @DELETE
    @Path("/{id}")
    public void deleteAudit(@PathParam("id") long id)
    {
        service.deleteById(id);
    }


}
