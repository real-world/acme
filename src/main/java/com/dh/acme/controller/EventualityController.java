package com.dh.acme.controller;

import com.dh.acme.command.EventualityCommand;
import com.dh.acme.domain.Employee;
import com.dh.acme.domain.Eventuality;
import com.dh.acme.service.EmployeeService;
import com.dh.acme.service.EventualityService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Controller
@Path("/eventualities")
@Produces("application/json")
@CrossOrigin
public class EventualityController {

    @org.springframework.data.annotation.Transient
    private EmployeeService employeeService;

    @org.springframework.data.annotation.Transient
    private Employee test;

    @org.springframework.data.annotation.Transient
    private Employee test2;

    @org.springframework.data.annotation.Transient
    private Employee test3;

    private EventualityService eventualityService;

    public EventualityController(EmployeeService employeeService, EventualityService eventualityService) {
        this.employeeService = employeeService;
        this.eventualityService = eventualityService;
    }
    @GET
    public Response getEventualities()  throws ClassNotFoundException
    {
        List<EventualityCommand> eventualityList = new ArrayList<>();
        try
        {
            eventualityService.procedureFindAll().forEach(eventuality -> {
                eventualityList.add(new EventualityCommand(eventuality));
            });
        /*List<Eventuality> eventualityList = new ArrayList<>();
        try {
            eventualityList = eventualityService.procedureFindAll();
        }*/
        }
        catch (Exception e){
            System.out.println(e.toString());
        }
        Response.ResponseBuilder responseBuilder = Response.ok(eventualityList);
        return responseBuilder.build();
    }

   /* @GET
    public Response getEventualities(){
        List<EventualityCommand> eventualities = new ArrayList<>();
        eventualityService.findAll().forEach(eventuality -> {
            EventualityCommand eventualityCommand = new EventualityCommand(eventuality);
            eventualities.add(eventualityCommand);
        });
        Response.ResponseBuilder responseBuilder = Response.ok(eventualities);

        return responseBuilder.build();
    }*/

    @GET
    @Path("/{employeeId}/eventualitiesByEmployee")
    public Response getEventualitiesByEmployee(@PathParam("employeeId") @NotNull Long employeeId) {
        test3 = employeeService.findById(employeeId);
        List<EventualityCommand> eventualities = new ArrayList<>();
        eventualityService.findByEmployee(test3).forEach(eventuality -> {
            EventualityCommand eventualityCommand = new EventualityCommand(eventuality);
            eventualities.add(eventualityCommand);
        });
        Response.ResponseBuilder responseBuilder = Response.ok(eventualities);

        return responseBuilder.build();

    }

    @GET
    @Path("/{id}")
    public Response getEventualitiesById(@PathParam("id") @NotNull Long id) {
        Eventuality eventuality = eventualityService.findById(id);
        Response.ResponseBuilder responseBuilder = Response.ok(new EventualityCommand(eventuality));

        return responseBuilder.build();
    }

    /* @POST
     @Path("{employeeId}/add")
     public Response saveEventuality(@PathParam("employeeId") Long employeeId, EventualityCommand eventualityCommand) {
         test = employeeService.findById(employeeId);
         eventualityCommand.setEmployee(test);
         Eventuality eventuality = eventualityCommand.toDomain();
         Eventuality eventualityPersisted = eventualityService.save(eventuality);
         Response.ResponseBuilder responseBuilder = Response.ok(new EventualityCommand(eventualityPersisted));
         return responseBuilder.build();
     }*/
    @POST
    public Response saveEventuality(EventualityCommand eventualityCommand)throws ClassNotFoundException
    {
        System.out.println("************controller********************");
        System.out.println(eventualityCommand);
        Eventuality eventuality = eventualityService.procedureSaveInsert(eventualityCommand.toDomain());
        System.out.println(eventuality);
        return getResponse(eventuality, false);
    }
    /* @PUT
     public Response updateEventuality(EventualityCommand eventualityCommand) {
         //test2 = employeeService.findById(eventualityCommand.getEmployeeId());

        // eventualityCommand.setEmployee(test2);
         Eventuality eventuality = eventualityCommand.toDomain();
         Eventuality eventualityPersisted = eventualityService.save(eventuality);
         Response.ResponseBuilder responseBuilder = Response.ok(new EventualityCommand(eventualityPersisted));
         return responseBuilder.build();
     }*/
    @PUT
    public Response updateEventuality(EventualityCommand eventualityCommand) throws ClassNotFoundException
    {
        Eventuality eventuality = eventualityService.procedureSaveUpdate(eventualityCommand.toDomain());
        Response.ResponseBuilder responseBuilder = Response.ok(eventuality);
        return responseBuilder.build();
    }
    @DELETE
    @Path("/{id}")
//    public Response deleteEventuality(@PathParam("id") String id){
//        eventualityService.deleteById(Long.valueOf(id));
//        Response.ResponseBuilder responseBuilder = Response.ok();
//
//        return responseBuilder.build();
//    }

//    public Response deleteEventuality(@PathParam("id") long id) throws ClassNotFoundException {
//        eventualityService.procedureDelete(id);
//        Response.ResponseBuilder responseBuilder = Response.ok();
//        return responseBuilder.build();
//    }

    public void deleteEventuality(@PathParam("id") long id) throws ClassNotFoundException {
        eventualityService.procedureDelete(id);
    }

    /*private void addCorsHeader(Response.ResponseBuilder responseBuilder){
        responseBuilder.header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Credentials", "true")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
                .header("Access-Control-Allow-Headers",
                        "Access-Control-Allow-Credentials, Access-Control-Allow-Headers, Origin, Accept, Authorization, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
    }*/

    private Response getResponse(Eventuality eventuality, Boolean flag) {
        if (null == eventuality) {
            return Response.status(404).build();
        }
        Response.ResponseBuilder responseBuilder = null;
        if (!flag) {
            responseBuilder = Response.ok();
        } else {
            responseBuilder = Response.ok(new EventualityCommand(eventuality));
        }
        return responseBuilder.build();
    }
}
