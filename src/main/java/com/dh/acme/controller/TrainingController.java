//--------Marvin Dickson Mendia Calizaya-------
package com.dh.acme.controller;

import com.dh.acme.command.TrainingCommand;
import com.dh.acme.domain.Training;
import com.dh.acme.service.AreaService;
import com.dh.acme.service.TrainingService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;


@Path("/traininies")
@Produces(MediaType.APPLICATION_JSON)
@Controller
@CrossOrigin
public class TrainingController
{
    private TrainingService service;
    private AreaService areaService;
    public TrainingController(TrainingService service,  AreaService areaService )
    {
        this.service = service;
        this.areaService = areaService;
    }

    @GET
    public Response getTraininies()
    {
        /*List<TrainingCommand> trainingList = new ArrayList<>();
        service.findAll().forEach(training -> {
            trainingList.add(new TrainingCommand(training));
        });*/
        List<TrainingCommand> trainingList = new ArrayList<>();
        try
        {
            //trainingList = service.procedureFindAll();
            service.procedureFindAll().forEach(training -> {
                trainingList.add(new TrainingCommand(training));
            });
        } catch (Exception e)
        {
            System.out.println(e.toString());
        }
        Response.ResponseBuilder responseBuilder = Response.ok(trainingList);
        return responseBuilder.build();
    }

    @GET
    @Path("/{id}")
    public Response getPositionById(@PathParam("id") @NotNull Long id)
    {
        Training training = service.findById(id);
        Response.ResponseBuilder responseBuilder = Response.ok(new TrainingCommand(training));

        return responseBuilder.build();
        /*Training training = service.findById(id);
        Response.ResponseBuilder responseBuilder = Response.ok(training);
        return responseBuilder.build();*/
    }

    @OPTIONS
    public Response prefligth()
    {
        Response.ResponseBuilder responseBuilder = Response.ok();
        responseBuilder.allow("OPTIONS").build();
        return responseBuilder.build();
    }

    @POST
    public Response addTraining(TrainingCommand trainingCommand) throws ClassNotFoundException
    {
        /*Training training = trainingCommand.toTraining();
        training.setArea(areaService.findById(trainingCommand.getAreaId()));
        service.save(training);
        return getResponse(training, false);*/

        Training training = service.procedureSaveInsert(trainingCommand.toTraining());
        Response.ResponseBuilder responseBuilder = Response.ok(training);
        return getResponse(training, false);
        //return responseBuilder.build();
    }


    /*@PUT
    public TrainingCommand updateTraining(TrainingCommand trainingCommand)
    {
        Training training = service.save(trainingCommand.toTraining());
        return new TrainingCommand(training);
    }*/

    @PUT
    public Response updateTraining(TrainingCommand trainingCommand) throws ClassNotFoundException
    {
        Training training = service.procedureSaveUpdate(trainingCommand.toTraining());
        Response.ResponseBuilder responseBuilder = Response.ok(training);
        return responseBuilder.build();
    }

    private Response getResponse(Training training, Boolean flag)
    {
        if (null == training)
        {
            return Response.status(404).build();
        }
        Response.ResponseBuilder responseBuilder = null;
        if (!flag)
        {
            responseBuilder = Response.ok();
        } else
        {
            responseBuilder = Response.ok(new TrainingCommand(training));
        }

        return responseBuilder.build();
    }

    @DELETE
    @Path("/{id}")
    public void deleteTraining(@PathParam("id") Long id) throws ClassNotFoundException
    {
        service.procedureDelete(id);
    }

    /*@DELETE
    @Path("/{id}")
    public void deleteTraining(@PathParam("id") long id)
    {
        service.deleteById(id);
    }*/
}