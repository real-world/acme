package com.dh.acme.controller;

import com.dh.acme.command.IncidentRegistryCommand;
import com.dh.acme.domain.IncidentRegistry;
import com.dh.acme.service.IncidentRegistryService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

@Controller
@Path("/incidents")
@Produces("application/json")
@CrossOrigin
public class IncidentRegistryController {

    private IncidentRegistryService service;

    public IncidentRegistryController(IncidentRegistryService service) {
        this.service = service;
    }

    @GET
    public Response getIncidents(){
        List<IncidentRegistryCommand> incidents = new ArrayList<>();
        service.findAll().forEach(incidentRegistry -> {
            IncidentRegistryCommand incidentRegistryCommand = new IncidentRegistryCommand(incidentRegistry);
            incidents.add(incidentRegistryCommand);
        });
        Response.ResponseBuilder responseBuilder = Response.ok(incidents);
        //addCorsHeader(responseBuilder);
        return responseBuilder.build();
    }

    @GET
    @Path("/{id}")
    public Response getIncidentById(@PathParam("id") @NotNull  Long id){
        IncidentRegistry incidentRegistry = service.findById(id);
        Response.ResponseBuilder responseBuilder = Response.ok(new IncidentRegistryCommand(incidentRegistry));
        //addCorsHeader(responseBuilder);
        return responseBuilder.build();
    }

    @POST
    public Response saveIncident (IncidentRegistryCommand incidentRegistryCommand){
        IncidentRegistry incidentRegistry = incidentRegistryCommand.toDomain();
        IncidentRegistry incidentRegistryPersisted = service.save(incidentRegistry);
        Response.ResponseBuilder responseBuilder = Response.ok(new IncidentRegistryCommand(incidentRegistryPersisted));
        return responseBuilder.build();
    }

    @PUT
    public Response updateIncident(IncidentRegistry incidentRegistry){
        IncidentRegistry incidentRegistryPersisted = service.save(incidentRegistry);
        Response.ResponseBuilder responseBuilder = Response.ok(new IncidentRegistryCommand(incidentRegistryPersisted));
        return responseBuilder.build();
    }

    @DELETE
    @Path("/{id}")
    public Response deleteIncident(@PathParam("id") String id){
        service.deleteById(Long.valueOf(id));
        Response.ResponseBuilder responseBuilder = Response.ok();
        //addCorsHeader(responseBuilder);
        return responseBuilder.build();
    }
    /*
    private void addCorsHeader(Response.ResponseBuilder responseBuilder){
        responseBuilder.header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Credentials", "true")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT")
                .header("Access-Control-Allow-Headers",
                        "Access-Control-Allow-Credentials, Access-Control-Allow-Headers, Origin, Accept, Authorization, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
    }*/
}
