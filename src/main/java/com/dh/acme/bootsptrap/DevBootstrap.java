package com.dh.acme.bootsptrap;

import com.dh.acme.domain.*;
import com.dh.acme.repository.*;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class DevBootstrap implements ApplicationListener<ContextRefreshedEvent> {

    private CategoryRepository categoryRepository;
    private SubCategoryRepository subCategoryRepository;
    private ItemRepository itemRepository;
    private EmployeeRepository employeeRepository;
    private PositionRepository positionRepository;
    private ContractRepository contractRepository;
    private TrainingRepository trainingRepository;
    private EventualityRepository eventualityRepository;
    private AreaRepository areaRepository;
    private RoleRepository roleRepository;
    private SkillRepository skillRepository;
    private ProjectRepository projectRepository;
    private IncidentRegistryRepository incidentRegistryRepository;
    private AuditRepository auditRepository;
    private SafetyRuleRepository safetyRuleRepository;
    private TypeContractRepository typeContractRepository;
    private EmployeeTrainingRepository employeeTrainingRepository;

    public DevBootstrap(CategoryRepository categoryRepository, SubCategoryRepository subCategoryRepository,
                        ItemRepository itemRepository, EmployeeRepository employeeRepository, PositionRepository positionRepository,
                        ContractRepository contractRepository, TrainingRepository trainingRepository, EventualityRepository eventualityRepository,
                        ProjectRepository projectRepository, IncidentRegistryRepository incidentRegistryRepository,
                        AuditRepository auditRepository, SafetyRuleRepository safetyRuleRepository, AreaRepository areaRepository,
                        RoleRepository roleRepository, SkillRepository skillRepository, TypeContractRepository typeContractRepository,
                        EmployeeTrainingRepository employeeTrainingRepository) {
        this.categoryRepository = categoryRepository;
        this.subCategoryRepository = subCategoryRepository;
        this.itemRepository = itemRepository;
        this.employeeRepository = employeeRepository;
        this.positionRepository = positionRepository;
        this.contractRepository = contractRepository;
        this.trainingRepository = trainingRepository;
        this.eventualityRepository = eventualityRepository;
        this.projectRepository = projectRepository;
        this.incidentRegistryRepository = incidentRegistryRepository;
        this.auditRepository = auditRepository;
        this.safetyRuleRepository = safetyRuleRepository;
        this.areaRepository = areaRepository;
        this.roleRepository = roleRepository;
        this.skillRepository = skillRepository;
        this.typeContractRepository = typeContractRepository;
        this.employeeTrainingRepository = employeeTrainingRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        initData();
    }

    private void initData() {
        // EPP category
        Category eppCategory = new Category();
        eppCategory.setCode("ADD");
        eppCategory.setName("segudidad");
        eppCategory.setDescription("categoria de equipos de segudiad");

        categoryRepository.save(eppCategory);

        // EPP category
        Category resourceCategory = new Category();
        resourceCategory.setCode("CON");
        resourceCategory.setName("construccion");
        resourceCategory.setDescription("categoria de equipos de contruccion");
        categoryRepository.save(resourceCategory);

        // safety subcategory
        SubCategory safetySubCategory = new SubCategory();
        safetySubCategory.setCategory(eppCategory);
        safetySubCategory.setCode("SAF");
        safetySubCategory.setName("SAFETY");
        safetySubCategory.setDescription("subcategoria de equipos de seguidad");

        subCategoryRepository.save(safetySubCategory);

        // raw material subcategory
        SubCategory rawMaterialSubCategory = new SubCategory();
        rawMaterialSubCategory.setCategory(resourceCategory);
        rawMaterialSubCategory.setCode("RM");
        rawMaterialSubCategory.setName("RAW MATERIAL");
        rawMaterialSubCategory.setDescription("subcategoria de equipos de construccion");

        subCategoryRepository.save(rawMaterialSubCategory);

        // Helmet Item
        Item helmet = new Item();
        helmet.setCode("HEL");
        helmet.setName("Alambre galvanizado Nro. 16/14 (en rollo)");
        helmet.setSubCategory(safetySubCategory);

        itemRepository.save(helmet);

        // ink Item
        Item ink = new Item();
        ink.setCode("INK");
        ink.setName("Bloque tipo MODULBLOCK Ab, 2 huecos pasantes medida: 15x19x39 cm ");
        ink.setSubCategory(rawMaterialSubCategory);
        itemRepository.save(ink);


        // John Employee
        Employee john = new Employee();
        john.setFirstName("John");
        john.setLastName("Doe");
        john.setPhone("1234567890");
        john.setEmail("john.doe@acme.com");
        john.setDni("12345");
        john.setAddress("6th street");
        john.setJobDescription("Good Driver");
        john.setJobPosition("Driver");

        // Pedro Employee
        Employee pedro = new Employee();
        pedro.setFirstName("Pedro");
        pedro.setLastName("Rodriguez");
        pedro.setPhone("22567890");
        pedro.setEmail("pedro.rodriguez@acme.com");
        pedro.setDni("76545");
        pedro.setAddress("Augusta street");
        pedro.setJobDescription("Electricity");
        pedro.setJobPosition("Electricity Manager");

        // Juan Employee
        Employee juan = new Employee();
        juan.setFirstName("Juan");
        juan.setLastName("Choque");
        juan.setPhone("68485250");
        juan.setEmail("juan.choque@acme.com");
        juan.setDni("99999");
        juan.setAddress("Heart street");
        juan.setJobDescription("Construction");
        juan.setJobPosition("Construction Manager");
        employeeRepository.save(juan);


        //electricity Area
        Area area1 = new Area();
        area1.setCode("ELE01");
        area1.setName("ELECTRICITY");
        areaRepository.save(area1);

        //CIVIL CONSTRUCTION Area
        Area area2 = new Area();
        area2.setCode("HAM01");
        area2.setName("CIVIL CONSTRUCTION");
        areaRepository.save(area2);


        //INSTALL ELECTRIC SUPERVISOR
        Role role1 = new Role();
        role1.setCode("SIE01");
        role1.setDescription("INSTALL ELECTRIC SUPERVISOR");

        //reinforced concrete SUPERVISOR
        Role role2 = new Role();
        role2.setCode("SRC01");
        role2.setDescription("REINFORCED CONCRETE SUPERVISOR");
        roleRepository.save(role1);
        roleRepository.save(role2);

        Role role3 = new Role();
        role3.setCode("SRC02");
        role3.setDescription("SUPERVISOR ENERGY ELECTRIC");
        roleRepository.save(role3);
        Role role4 = new Role();
        role4.setCode("SR0011");
        role4.setDescription("OPERATOR PALA MECANIC");
        roleRepository.save(role4);
        Role role5 = new Role();
        role5.setCode("OPM050");
        role5.setDescription("OPERATOR COMPACTOR MACHINE");
        roleRepository.save(role5);

        //Position
        Position position = new Position();
        position.setName("OPERATIVE");
        //position.setRole(role1);

        Position position2 = new Position();
        position2.setName("PENDIENTE");
        //position2.setRole(role2);
        positionRepository.save(position2);
        Position position3 = new Position();
        position3.setName("OPERATIVE");
        //position3.setRole(role3);
        positionRepository.save(position3);
        Position position4 = new Position();
        position4.setName("OPERATIVO MACHINE");
        //position4.setRole(role4);
        positionRepository.save(position4);

        //home electricity skill
        Skill skill = new Skill();
        skill.setCode("HES01");
        skill.setDescription("HOME ELECTRICITY");
        //position.getSkills().add(skill);

        //Training
        Training training = new Training();
        training.setCode("ELECTNORM");
        training.setName("ELECTRIC NORMATIVE");
        training.setInstructor("RODRIGO ZEBALLOS");
        //training.setArea(area1);
        //position.getTraininies().add(training);

        positionRepository.save(position);
        skillRepository.save(skill);
        trainingRepository.save(training);

        //Training
        Training training1 = new Training();
        training1.setCode("ELENOR2018");
        training1.setName("ELECTRIC NORMATIVE 2018");
        training1.setInstructor("MARIO CACERES");
        //training1.setArea(area1);
        trainingRepository.save(training1);

        // Type Contract
        TypeContract destajo = new TypeContract();
        destajo.setTypeContract("Destajo");
        destajo.setDescription("Trabajos por labor realizada");
        destajo.setResponsable("Personal");

        TypeContract service = new TypeContract();
        service.setTypeContract("servicio");
        service.setDescription("Trabajos por servicio");
        service.setResponsable("empresa");
        typeContractRepository.save(destajo);
        typeContractRepository.save(service);

        // create project for contract
        Project vivienda = new Project();
        vivienda.setName("Vivienda  chalet");
        vivienda.setDescription("vivienda tipo chalet");
//        vivienda.setArea("Techado");
        projectRepository.save(vivienda);


        //project
        List<Contract> contractsC = new ArrayList<>();
        //contracts.add(contractPersisted);

        List<Audit> auditsA = new ArrayList<>();
        //audits.add(auditPersisted);

        //contracts.add(contractPersisted);
        Project project1 = new Project();
        project1.setName("project1");
        project1.setDescription("test");
        //-project1.setArea("ObraGruesa");
//        project.setContracts(contracts);

        // contract
        Contract contract1 = new Contract();
        contract1.setContractAmount("7000 bs");
        contract1.setContractCode("CACO001");
        contract1.setPaymentType("Efectivo");
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            contract1.setInitDate(dateFormat.parse("2010-01-02"));
            contract1.setEndDate(dateFormat.parse("2010-08-10"));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        employeeRepository.save(john);
        typeContractRepository.save(destajo);
        contract1.setPosition(position);
        contract1.setEmployee(john);
        contract1.setTypeContract(destajo);
        john.getContracts().add(contract1);
        destajo.getContracts().add(contract1);
//        vivienda.getContracts().add(contract1);
        contractsC.add(contract1);
//        project1.setContracts(contractsC);
        contract1.setProject(project1);
        projectRepository.save(project1);
//        contractRepository.save(contract);
        //  projectRepository.save(vivienda);

//        Contract contractPersisted = contractRepository.save(contract);

        //Incidents
        IncidentRegistry accidente = new IncidentRegistry();
        accidente.setDescription("Contusion en la cabeza al caer en piso mojado");
        accidente.setTitle("No llevaba casco");

        incidentRegistryRepository.save(accidente);

        /*//Eventuality
        Eventuality event1 = new Eventuality();
        event1.setTypeEvent("Accident");
        event1.setDateEvent(new Date(118, 4, 3));
        event1.setInjuryType("Burn");
        event1.setInjuryPart("Legs");
        event1.setDescription("Description test");
        //caida.setEmployee(john);
        event1.setEmployee("john");

        eventualityRepository.save(event1);*/

        // audit
        Audit audit1 = new Audit();
        audit1.setAuditName("Audit Name");
        audit1.setAuditCode("AUD-01");
        audit1.setAuditType("INTERNAL");
        audit1.setAuditScope("Evaluate Scope");
        audit1.setAuditObjective("Find Objective");
        audit1.setAuditCriteria("According to Manual Criteria");
        audit1.setAuditPeriodicity("ANNUAL");
        audit1.setEmployee(john);
        audit1.setArea(area1);
        auditRepository.save(audit1);

        //Safety Rule
        SafetyRule safetyRule1 = new SafetyRule();
        safetyRule1.setPolicyCode("POLICY-01");
        safetyRule1.setPolicyName("PREVENTION");
        safetyRule1.setComplianceParameter(60);
        safetyRule1.setComplianceMetric(70);
        safetyRule1.setAccomplishment(true);
        safetyRule1.setAudit(audit1);
        safetyRuleRepository.save(safetyRule1);

        SafetyRule safetyRule2 = new SafetyRule();
        safetyRule2.setPolicyCode("POLICY-02");
        safetyRule2.setPolicyName("TRAINING");
        safetyRule2.setComplianceParameter(90);
        safetyRule2.setComplianceMetric(90);
        safetyRule2.setAccomplishment(false);
        safetyRule2.setAudit(audit1);
        safetyRuleRepository.save(safetyRule2);

        //project
        Project project = new Project();
        project.setName("Edificio San Gabriel ");
        project.setDescription("Cimentación, columnas, trabes, losas, etc");
        //-project.setArea("Obra Gruesa");


        //ProjectAreas
        ProjectArea project_area1= new ProjectArea();
        project_area1.setEstado("activo");
       // project_area1.setFecha_inicio(new Date());
       // project_area1.setFecha_fin(null);

        ProjectArea project_area2= new ProjectArea();
        project_area2.setEstado("inactivo");
       // project_area2.setFecha_inicio(new Date());
       // project_area2.setFecha_fin(null);

        //adding projectAreas to project
     //   project.addProject_Area(project_area1);
       // project.addProject_Area(project_area2);

        project_area1.setProject(project);
        project_area2.setProject(project);
        projectRepository.save(project);
        //EmployeeTraining
        EmployeeTraining employeeTraining = new EmployeeTraining();
        employeeTraining.setTrainingId(training.getId());
        employeeTraining.setName(training.getName());
        employeeTraining.setInstructor(training.getInstructor());
        employeeTraining.setEmployeeId(john.getId());
        employeeTraining.setFirstName(john.getFirstName());
        employeeTraining.setLastName(john.getLastName());

        EmployeeTraining employeeTraining1 = new EmployeeTraining();
        employeeTraining1.setTrainingId(training1.getId());
        employeeTraining1.setName(training1.getName());
        employeeTraining1.setInstructor(training1.getInstructor());
        employeeTraining1.setEmployeeId(john.getId());
        employeeTraining1.setFirstName(john.getFirstName());
        employeeTraining1.setLastName(john.getLastName());

        EmployeeTraining employeeTraining2 = new EmployeeTraining();
        employeeTraining2.setTrainingId(training.getId());
        employeeTraining2.setName(training.getName());
        employeeTraining2.setInstructor(training.getInstructor());
        employeeTraining2.setEmployeeId(pedro.getId());
        employeeTraining2.setFirstName(pedro.getFirstName());
        employeeTraining2.setLastName(pedro.getLastName());
        employeeTrainingRepository.save(employeeTraining);
        employeeTrainingRepository.save(employeeTraining1);
        employeeTrainingRepository.save(employeeTraining2);
    }
}
