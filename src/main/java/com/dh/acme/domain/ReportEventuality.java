package com.dh.acme.domain;

import javax.persistence.Entity;

@Entity
public class ReportEventuality extends ModelBase{
    private String employeeName;
    private String typeEvent;

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getTypeEvent() {
        return typeEvent;
    }

    public void setTypeEvent(String typeEvent) {
        this.typeEvent = typeEvent;
    }
}
