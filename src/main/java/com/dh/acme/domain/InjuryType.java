package com.dh.acme.domain;

import javax.persistence.Entity;

@Entity
public class InjuryType extends ModelBase {
    private String injuryType;

    public String getInjuryType() {
        return injuryType;
    }

    public void setInjuryType(String injuryType) {
        this.injuryType = injuryType;
    }
}