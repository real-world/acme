package com.dh.acme.domain;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "project")
public class Project extends ModelBase {

    private String name;
    private String description;
    private Date date_start;
    private Date date_end;

    @OneToMany(cascade = CascadeType.ALL,orphanRemoval = true,mappedBy="project",fetch = FetchType.EAGER)
    public Set<ProjectArea> projectAreas =  new HashSet<ProjectArea>();

//    @OneToMany(mappedBy = "project", cascade = CascadeType.ALL, orphanRemoval = true)
//    private List<ProjectArea> project_areas = new ArrayList<ProjectArea>();

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDate_start() {
        return date_start;
    }

    public void setDate_start(Date date_start) {
        this.date_start = date_start;
    }

    public Date getDate_end() {
        return date_end;
    }

    public void setDate_end(Date date_end) {
        this.date_end = date_end;
    }


//    public List<ProjectArea> getProject_areas() { return project_areas; }
//
//    public void setProject_areas(List<ProjectArea> project_areas) {
//        this.project_areas = project_areas;
//    }
//
//    public void addProject_Area(ProjectArea project_area){
//        this.project_areas.add(project_area);
//    }
}