package com.dh.acme.domain;

import javax.persistence.Entity;


/**
 * Created by Marcelo on 07/07/2018.
 */
@Entity
public class DwReportParamEmployeTypeEvent extends ModelBase {
    private String nameEmployee;
    private String nameTypeEvent;
    private String nameProyect;

    public String getNameEmployee() {
        return nameEmployee;
    }

    public void setNameEmployee(String nameEmployee) {
        this.nameEmployee = nameEmployee;
    }

    public String getNameTypeEvent() {
        return nameTypeEvent;
    }

    public void setNameTypeEvent(String nameTypeInjury) {
        this.nameTypeEvent = nameTypeInjury;
    }

    public String getNameProyect() {
        return nameProyect;
    }

    public void setNameProyect(String nameProyect) {
        this.nameProyect = nameProyect;
    }
}
