//--------Marvin Dickson Mendia Calizaya-------
package com.dh.acme.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "skill")
public class Skill extends ModelBase
{
    private String code;
    private String description;

    @ManyToMany(cascade = {CascadeType.ALL}, fetch = FetchType.LAZY)
    @JoinTable(name = "assignmentPositionSkill",
            joinColumns = @JoinColumn(name = "skill_id"),
            inverseJoinColumns = @JoinColumn(name = "position_id"))
    private List<Position> positions = new ArrayList<>();

    //@JsonBackReference
    @JsonIgnore
    public List<Position> getPositions()
    {
        return positions;
    }
    //@JsonBackReference
    @JsonIgnore
    public void setPositions(List<Position> positions)
    {
        this.positions = positions;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }
}