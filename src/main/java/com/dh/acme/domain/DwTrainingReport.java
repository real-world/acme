//--------Marvin Dickson Mendia Calizaya-------
package com.dh.acme.domain;

import javax.persistence.Entity;

@Entity
//@Table(name = "area")
public class DwTrainingReport extends ModelBase
{

    private String typeEvent;
    private String employeeName;
    private String training;


    public String getTypeEvent() {
        return typeEvent;
    }

    public void setTypeEvent(String typeEvent) {
        this.typeEvent = typeEvent;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getTraining() {
        return training;
    }

    public void setTraining(String training) {
        this.training = training;
    }
}