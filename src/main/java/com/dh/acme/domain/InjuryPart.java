package com.dh.acme.domain;

import javax.persistence.Entity;

@Entity
public class InjuryPart extends ModelBase {
    private String Part;

    public String getPart() {
        return Part;
    }

    public void setPart(String part) {
        Part = part;
    }
}
