//--------Marvin Dickson Mendia Calizaya-------
package com.dh.acme.domain;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "area")
public class Area extends ModelBase
{
    private String code;
    private String name;
    private int modifiedBy;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "area", orphanRemoval = true)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<Training> traininies = new ArrayList<>();
    //private Training training;

    @OneToMany(mappedBy = "area")
    private List<ProjectArea> project_areas = new ArrayList<ProjectArea>();

    @OneToMany(cascade = CascadeType.ALL,orphanRemoval = true,mappedBy="area",fetch = FetchType.EAGER)
    public Set<ProjectArea> projectAreas =  new HashSet<ProjectArea>();

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public List<Training> getTraininies()
    {
        return traininies;
    }

    public void setTraininies(List<Training> traininies)
    {
        this.traininies = traininies;
    }

    public int getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(int modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    /*    public Training getTraining()
    {
        return training;
    }

    public void setTraining(Training training)
    {
        this.training = training;
    }*/
}