package com.dh.acme.domain;

import javax.persistence.Entity;

@Entity
public class TypeEvent extends ModelBase {

    private String typeEvent;

    public String getTypeEvent() {
        return typeEvent;
    }

    public void setTypeEvent(String typeEvent) {
        this.typeEvent = typeEvent;
    }
}