//--------Marvin Dickson Mendia Calizaya-------
package com.dh.acme.domain;

import javax.persistence.Entity;

@Entity
//@Table(name = "area")
public class DwTrainingGraphReport extends ModelBase {

    private Integer countEnventuality;
    private String EmployeeName;

    public Integer getCountEnventuality() {
        return countEnventuality;
    }

    public void setCountEnventuality(Integer countEnventuality) {
        this.countEnventuality = countEnventuality;
    }

    public String getEmployeeName() {
        return EmployeeName;
    }

    public void setEmployeeName(String employeeName) {
        EmployeeName = employeeName;
    }
}