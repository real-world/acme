package com.dh.acme.domain;

import javax.persistence.Entity;

@Entity
public class ReportEventualityByEmployee extends ModelBase {
    private String typeEvent;
    private Integer numberEventType;

    public String getTypeEvent() {
        return typeEvent;
    }

    public void setTypeEvent(String typeEvent) {
        this.typeEvent = typeEvent;
    }

    public Integer getNumberEventType() {
        return numberEventType;
    }

    public void setNumberEventType(Integer numberEventType) {
        this.numberEventType = numberEventType;
    }
}
