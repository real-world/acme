package com.dh.acme.domain;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "contract")
//@JsonIgnoreProperties({"position","employee"})
public class Contract extends ModelBase {


    private String contractCode;
    private String paymentType;
    private String contractAmount;

    @OneToOne
    private Position position;

    @ManyToOne(optional = false)
    @JoinColumn(name = "type_contract_id", referencedColumnName = "id")
    private TypeContract typeContract ;

    private Date initDate;
    private Date endDate;

    @ManyToOne
    @Fetch(FetchMode.JOIN)
    @JoinColumn(name = "employee_id", referencedColumnName = "id")
    private Employee employee;


    @ManyToOne
    @Fetch(FetchMode.JOIN)
    @JoinColumn(name = "project_id", referencedColumnName = "id")
    private Project project;

//    @ManyToMany(cascade = {CascadeType.MERGE, CascadeType.REMOVE}, fetch = FetchType.EAGER)
//    @JoinTable(name = "contract_project",
//            joinColumns = @JoinColumn(name = "contract_id"),
//            inverseJoinColumns = @JoinColumn(name = "project_id"))
//    private List<Contract> project = new ArrayList<>();



    public TypeContract getTypeContract() {
        return typeContract;
    }

    public void setTypeContract(TypeContract typeContract) {
        this.typeContract = typeContract;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Date getInitDate() {
        return initDate;
    }

    public void setInitDate(Date initDate) {
        this.initDate = initDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getContractCode() {
        return contractCode;
    }

    public void setContractCode(String contractCode) {
        this.contractCode = contractCode;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getContractAmount() {
        return contractAmount;
    }

    public void setContractAmount(String contractAmount) {
        this.contractAmount = contractAmount;
    }

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }



}
