package com.dh.acme.domain;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class IncidentRegistry extends ModelBase{

    @NotNull
    @Size(max = 100)
    //@Column(unique = true)
    private String title;

    //@NotNull
    @Size(max = 250)
    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
