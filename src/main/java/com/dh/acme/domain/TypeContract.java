package com.dh.acme.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Marcelo on 18/05/2018.
 */
@Entity
public class TypeContract extends ModelBase{
    private String typeContract;
    private String responsable;
    private String description;

    @JsonIgnoreProperties("contracts")
    @OneToMany(  mappedBy = "typeContract", fetch = FetchType.EAGER,cascade = CascadeType.ALL,orphanRemoval = true)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<Contract>  contracts= new ArrayList<>();

    public String getTypeContract() {
        return typeContract;
    }

    public void setTypeContract(String typeContract) {
        this.typeContract = typeContract;
    }

    public String getResponsable() {
        return responsable;
    }

    public void setResponsable(String responsable) {
        this.responsable = responsable;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Contract> getContracts() {
        return contracts;
    }

    public void setContracts(List<Contract> contracts) {
        this.contracts = contracts;
    }


}
