//--------Marvin Dickson Mendia Calizaya-------
package com.dh.acme.domain;

import javax.persistence.Entity;
import java.sql.Date;

@Entity
//@Table(name = "area")
public class DwReport extends ModelBase {
    private Date dateEvent;
    private String descriptionEvent;
    private String employeName;
    private String project;
    private String training;

    public Date getDateEvent() {
        return dateEvent;
    }

    public void setDateEvent(Date dateEvent) {
        this.dateEvent = dateEvent;
    }

    public String getDescriptionEvent() {
        return descriptionEvent;
    }

    public void setDescriptionEvent(String descriptionEvent) {
        this.descriptionEvent = descriptionEvent;
    }

    public String getEmployeName() {
        return employeName;
    }

    public void setEmployeName(String employeName) {
        this.employeName = employeName;
    }

    public String getProject() {
        return project;
    }

    public void setProject(String project) {
        this.project = project;
    }

    public String getTraining() {
        return training;
    }

    public void setTraining(String training) {
        this.training = training;
    }
}