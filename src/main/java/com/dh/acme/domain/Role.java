//--------Marvin Dickson Mendia Calizaya-------
package com.dh.acme.domain;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "role")
public class Role extends ModelBase
{
    private String code;
    private String description;
    private int modifiedBy;
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "role", orphanRemoval = true)
    @Fetch(value = FetchMode.SUBSELECT)
    private List<Position> positions = new ArrayList<>();

    public List<Position> getPositions()
    {
        return positions;
    }

    public void setPositions(List<Position> positions)
    {
        this.positions = positions;
    }

    public String getCode()
    {
        return code;
    }

    public void setCode(String code)
    {
        this.code = code;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public int getModifiedBy()
    {
        return modifiedBy;
    }

    public void setModifiedBy(int modifiedBy)
    {
        this.modifiedBy = modifiedBy;
    }
}