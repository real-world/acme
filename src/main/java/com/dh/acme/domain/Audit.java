package com.dh.acme.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
public class Audit extends ModelBase{
//    private String objective;
//    private String scope;
//    private String auditCriteria;
//    private String auditTeam;
//    private Date dateStart;
//    private Date dateFinish;

    private String auditName;
    private String auditCode;
    private String auditType;
    private String auditScope;
    private String auditObjective;
    private String auditCriteria;
    private String auditPeriodicity;

    @ManyToOne
    @JoinColumn(name = "project_area_id_fk")
    private ProjectArea project_area;

//    @JsonBackReference
//    @OneToMany(fetch = FetchType.LAZY, mappedBy = "audit", cascade = CascadeType.REMOVE)
//    private List<AuditResult> auditResults = new ArrayList<>();

   // @JsonBackReference
   @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private Employee employee;

    //@JsonBackReference
    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.REMOVE)
    private Area area;



//    public String getObjective() {
//        return objective;
//    }
//
//
//
//
//    public void setObjective(String objective) {
//        this.objective = objective;
//    }
//
//    public String getScope() {
//        return scope;
//    }
//
//    public void setScope(String scope) {
//        this.scope = scope;
//    }
//
//    public String getAuditCriteria() {
//        return auditCriteria;
//    }
//
//    public void setAuditCriteria(String auditCriteria) {
//        this.auditCriteria = auditCriteria;
//    }
//
//    public String getAuditTeam() {
//        return auditTeam;
//    }
//
//    public void setAuditTeam(String auditTeam) {
//        this.auditTeam = auditTeam;
//    }
//
//    public Date getDateStart() {
//        return dateStart;
//    }
//
//    public void setDateStart(Date dateStart) {
//        this.dateStart = dateStart;
//    }
//
//    public Date getDateFinish() {
//        return dateFinish;
//    }
//
//    public void setDateFinish(Date dateFinish) {
//        this.dateFinish = dateFinish;
//    }
//
//    public List<AuditResult> getAuditResults() {
//        return auditResults;
//    }
//
//    public void setAuditResults(List<AuditResult> auditResults) {
//        this.auditResults = auditResults;
//    }


    public String getAuditName() {
        return auditName;
    }

    public void setAuditName(String auditName) {
        this.auditName = auditName;
    }

    public String getAuditCode() {
        return auditCode;
    }

    public void setAuditCode(String auditCode) {
        this.auditCode = auditCode;
    }

    public String getAuditType() {
        return auditType;
    }

    public void setAuditType(String auditType) {
        this.auditType = auditType;
    }

    public String getAuditScope() {
        return auditScope;
    }

    public void setAuditScope(String auditScope) {
        this.auditScope = auditScope;
    }

    public String getAuditObjective() {
        return auditObjective;
    }

    public void setAuditObjective(String auditObjective) {
        this.auditObjective = auditObjective;
    }

    public String getAuditCriteria() {
        return auditCriteria;
    }

    public void setAuditCriteria(String auditCriteria) {
        this.auditCriteria = auditCriteria;
    }

    public String getAuditPeriodicity() {
        return auditPeriodicity;
    }

    public void setAuditPeriodicity(String auditPeriodicity) {
        this.auditPeriodicity = auditPeriodicity;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Area getArea() {
        return area;
    }

    public void setArea(Area area) {
        this.area = area;
    }
}
