package com.dh.acme.domain;

import javax.persistence.Entity;

@Entity
public class DwReportEmployeeTypeEvent extends ModelBase {

    private String employeeName;
    private String typeEvent;
    private String proyect;
    private Long cant;
    private Long numberAccident;
    private Long numberIncident;

	

    public Long getCant() {
        return cant;
    }

    public void setCant(Long cant) {
        this.cant = cant;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getTypeEvent() {
        return typeEvent;
    }

    public void setTypeEvent(String typeEvent) {
        this.typeEvent = typeEvent;
    }

    public String getProyect() {
        return proyect;
    }

    public void setProyect(String proyect) {
        this.proyect = proyect;
    }

    public Long getNumberAccident() {
        return numberAccident;
    }

    public void setNumberAccident(Long numberAccident) {
        this.numberAccident = numberAccident;
    }

    public Long getNumberIncident() {
        return numberIncident;
    }

    public void setNumberIncident(Long numberIncident) {
        this.numberIncident = numberIncident;
    }

}