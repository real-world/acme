package com.dh.acme.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.util.Date;

@Entity
public class Eventuality extends ModelBase{

    private Date dateEvent;
    private String description;

    @ManyToOne(targetEntity = TypeEvent.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "typeEvent_id", referencedColumnName="id", insertable=false, updatable=false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private String typeEvent;
    private int typeEvent_id;

    @ManyToOne(targetEntity = InjuryType.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "injuryType_id", referencedColumnName="id", insertable=false, updatable=false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private String injuryType;
    private int injuryType_id;

    @ManyToOne(targetEntity = InjuryPart.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "injuryPart_id", referencedColumnName="id", insertable=false, updatable=false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private String injuryPart;
    private int injuryPart_id;

    @ManyToOne(targetEntity = Employee.class, fetch = FetchType.LAZY)
    @JoinColumn(name = "employee_id", referencedColumnName="id", insertable=false, updatable=false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private String employee;
    private int employee_id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "projectArea_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private ProjectArea projectArea;

    public Date getDateEvent() {
        return dateEvent;
    }

    public void setDateEvent(Date dateEvent) {
        this.dateEvent = dateEvent;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTypeEvent() {
        return typeEvent;
    }

    public void setTypeEvent(String typeEvent) {
        this.typeEvent = typeEvent;
    }

    public String getInjuryType() {
        return injuryType;
    }

    public void setInjuryType(String injuryType) {
        this.injuryType = injuryType;
    }

    public String getInjuryPart() {
        return injuryPart;
    }

    public void setInjuryPart(String injuryPart) {
        this.injuryPart = injuryPart;
    }

    public String getEmployee() {
        return employee;
    }

    public void setEmployee(String employee) {
        this.employee = employee;
    }

    public ProjectArea getProjectArea() {
        return projectArea;
    }

    public void setProjectArea(ProjectArea projectArea) {
        this.projectArea = projectArea;
    }

    public int getTypeEvent_id() {
        return typeEvent_id;
    }

    public void setTypeEvent_id(int typeEvent_id) {
        this.typeEvent_id = typeEvent_id;
    }

    public int getInjuryType_id() {
        return injuryType_id;
    }

    public void setInjuryType_id(int injuryType_id) {
        this.injuryType_id = injuryType_id;
    }

    public int getInjuryPart_id() {
        return injuryPart_id;
    }

    public void setInjuryPart_id(int injuryPart_id) {
        this.injuryPart_id = injuryPart_id;
    }

    public int getEmployee_id() {
        return employee_id;
    }

    public void setEmployee_id(int employee_id) {
        this.employee_id = employee_id;
    }
}
