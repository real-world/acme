package com.dh.acme.domain;

import javax.persistence.*;

@Entity
@Table(name = "subcategory")
public class SubCategory extends ModelBase{

    private String name;
    private String code;
    @OneToOne(optional = false)
    private Category category;
    private  String description;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getSubcategoryID() {
        return category.getId();
    }

    public void setSubcategoryID(long id) {
        this.getCategory().setId(id);
    }
}
