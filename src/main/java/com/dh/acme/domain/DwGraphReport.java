//--------Marvin Dickson Mendia Calizaya-------
package com.dh.acme.domain;

import javax.persistence.Entity;
import java.sql.Date;

@Entity
//@Table(name = "area")
public class DwGraphReport extends ModelBase {

    private Integer countEnventuality;
    private String EmployeeName;
    private String DateEvent;

    public Integer getCountEnventuality() {
        return countEnventuality;
    }

    public void setCountEnventuality(Integer countEnventuality) {
        this.countEnventuality = countEnventuality;
    }

    public String getEmployeeName() {
        return EmployeeName;
    }

    public void setEmployeeName(String employeeName) {
        EmployeeName = employeeName;
    }

    public String getDateEvent() {
        return DateEvent;
    }

    public void setDateEvent(String dateEvent) {
        DateEvent = dateEvent;
    }
}